// JavaScript Document

/* MODAL CONFIGURATION */

// 0: if user without signed DUT clicks AWS link.
var aws_no_dut = { 
	width: 500,
	height: 300,
	scroll: false,
	title: 'AWS Connection: HCP Data Use Terms Required',
	content: $('#aws-no-dut').html(),
	enter: false,
	buttons: {
		close: {
			label: 'Okay',
			isDefault: true
		}
	}
};

// 1: if user with signed DUT clicks on AWS link. Goal: create AWS credentials. 
var aws_setup = { 
	width: 500,
	height: 300,
	scroll: false,
	title: 'AWS Connection: Set Up Credentials',
	content: $('#aws-setup').html(),
	enter: false,
	classNames: 'aws',
	buttons: {
		ok: {
			label: 'Create my AWS Credentials',
			isDefault: true,
			close: false,
			action: function(obj) { 
				// opens "processing modal"
				setupAWS();
			} 
		},
		cancel: { 
			label: 'Cancel',
			close: true, 
			link: true
		} 
	}
}; 

// 2: progress modal opened by aws_setup. 
var aws_setup_processing = { 
	width: 450,
	height: 250,
	scroll: false,
	title: 'AWS Connection: Set Up Credentials',
	content: $('#aws-setup-processing').html(),
	enter: false,
	classNames: 'aws',
	buttons: {
		cancel: { 
			label: 'Cancel',
			close: false, // disable the default modal close behavior. Force user to confirm.  
			link: true,
			action: function(obj) { 
				// confirm that the user wants to cancel this operation. If so, close the modal. 
				if (confirm("Are you sure you want to cancel this operation?")) { 
					// mckay, do we need to explicitly cancel the AWS request? 
					xmodal.closeAll(); 
				}
			}
		} 
	}
};

// 3: success modal opened by AWS credential creation call
var aws_setup_success = { 
	width: 500,
	height: 450,
	scroll: false,
	title: 'AWS Connection: Success',
	content: $('#aws-setup-success').html(),
	enter: false,
	classNames: 'aws aws-success',
	buttons: {
		close: { 
			label: 'Close',
			isDefault: true,
			close: true
		} 
	}
}; 

// 4: failure modal opened by AWS credential creation call. 
var aws_setup_failure = { 
	width: 500,
	height: 300,
	scroll: false,
	title: 'AWS Connection Manager: Failure',
	content: $('#aws-setup-failure').html(),
	enter: false,
	classNames: 'aws aws-failure',
	buttons: {
		ok: {
			label: 'Send Error Report',
			isDefault: true,
			close: true,
			action: function(obj) { 
				// mckay: send an email to support@humanconnectome.org 
			} 
		},
		close: { 
			label: 'Close',
			link: true
		} 
	}
};

// 5: user-initiated modal. displays stored user credentials, and has a link to create new. 
var aws_recreate = { 
	width: 500,
	height: 450,
	scroll: false,
	title: 'AWS Connection Manager',
	content: $('#aws-confirm').html(),
	enter: false,
	classNames: 'aws aws-success',
	buttons: {
		ok: {
			label: 'Recreate my AWS Credentials',
			isDefault: true,
			close: false,
			action: function(obj) { 
				// opens "Recreate Credentials modal"
				resetAWS();
			} 
		},
		cancel: { 
			label: 'Cancel',
			close: true, 
			link: true
		} 
	}
}; 

// 6: progress modal opened by aws_confirm. 
var aws_recreate_processing = { 
	width: 450,
	height: 250,
	scroll: false,
	title: 'AWS Connection: Processing',
	content: $('#aws-recreate-processing').html(),
	enter: false,
	classNames: 'aws',
	buttons: {
		cancel: { 
			label: 'Cancel',
			close: false, // disable the default modal close behavior. Force user to confirm.  
			link: true,
			action: function(obj) { 
				// confirm that the user wants to cancel this operation. If so, close the modal. 
				if (confirm("Are you sure you want to cancel this operation?")) { 
					// mckay, do we need to explicitly cancel the AWS request? 
					xmodal.closeAll(); 
				}
			}
		} 
	}
};

// 7: success modal opened by AWS credential creation call
var aws_recreate_success = { 
	width: 500,
	height: 450,
	scroll: false,
	title: 'AWS Connection: Success',
	content: $('#aws-recreate-success').html(),
	enter: false,
	classNames: 'aws aws-success',
	buttons: {
		close: { 
			label: 'Close',
			isDefault: true,
			close: true
		} 
	}
}; 

// 8: failure modal opened by AWS credential creation call. 
var aws_recreate_failure = { 
	width: 500,
	height: 300,
	scroll: false,
	title: 'AWS Connection Manager: Failure',
	content: $('#aws-recreate-failure').html(),
	enter: false,
	classNames: 'aws aws-failure',
	buttons: {
		ok: {
			label: 'Send Error Report',
			isDefault: true,
			close: true,
			action: function(obj) { 
				// mckay: send an email to support@humanconnectome.org 
			} 
		},
		close: { 
			label: 'Close',
			link: true
		} 
	}
};

// 9: user-initiated modal. displays stored user credentials relative to a specific project, and has a link to create new. 
var aws_confirm_proj = { 
	width: 500,
	height: 450,
	scroll: false,
	title: 'AWS Connection Manager',
	content: $('#aws-confirm-proj').html(),
	enter: false,
	classNames: 'aws aws-success',
	buttons: {
		ok: {
			label: 'Recreate my AWS Credentials',
			isDefault: true,
			close: false,
			action: function(obj) { 
				// opens "Recreate Credentials modal"
				setupAWS();
			} 
		},
		cancel: { 
			label: 'Cancel',
			close: true, 
			link: true
		} 
	}
}; 


var awsBucketChangeNotification = function(showRegenerate){ 
		xmodal.message({title: "Important Message",
				content: "<h3>IMPORTANT:  The AWS bucket for HCP data has temporarily changed.</h3><b>The new bucket is " + 
					"<em>s3://hcp-openaccess-temp/</em><br><br>" + 
					((showRegenerate) ? "<em>This change requires AWS keys to be regenerated.</em><br><br>" : "") +
					"</b>REASON:  Recent changes in the AWS Public Dataset Program has required migration of HCP data to a new AWS account." +
					"  While we migrate the original s3://hcp-openaccess/ bucket, the data must be accessed through a temporary bucket location, s3://hcp-openaccess-temp." +
					"  It is expected that the migration may take 4 - 6 weeks, after which time the data will once again be accessible via the original bucket.",
				height: 300,
				width: 700 });	
}

/* BUTTON CLICK CONFIGURATION */
$(document).ready(function(){ 
	$('.aws-launcher-nodut').on('click',function(){ 
		xmodal.open(aws_no_dut);
	});
	
	$('.aws-launcher-enable').on('click',function(){ 
		xmodal.open(aws_setup);
		//awsBucketChangeNotification(false);
	});
	
	$('.aws-launcher-recreate').on('click',function(){ 
		xmodal.open(aws_recreate);
		//awsBucketChangeNotification(true);
	});
});

/* AMAZON REQUESTS AND MODAL CHAINING LOGIC */

// initial credential setup

var setupAWSsuccess = function(o){ 
	// mckay: get a series of values as a result from a successful call, then plug them into the modal using the beforeShow method. I'm inserting dummy values here. 
	aws_setup_success.beforeShow = function(obj){ 
		var accessKey = 'accessKey1';
		var secretKey = 'originalSecretKey';
		obj.$modal.find('input.aws-accesskey').val(accessKey);
		obj.$modal.find('input.aws-secretkey').val(secretKey);
	};
	xmodal.closeAll();
	xmodal.open(aws_setup_success);
};

var setupAWSfailure = function(o){ 
	// mckay: get error messages as a result from a failed call, then plug them into the modal using the beforeShow method. I'm inserting dummy values here. 
	aws_setup_failure.beforeShow = function(obj){ 
		var errorMsg = '';  
		obj.$modal.find('.aws-errormessage').text(errorMsg);
	};
	xmodal.closeAll();
	xmodal.open(aws_setup_failure);
};

var setupAWScallback = { 
	success: setupAWSsuccess,
	failure: setupAWSfailure,
	cache:false, // Turn off caching for IE
	scope:this
};

var setupAWS = function(){
	// insert jQuery GET request to Amazon. Use setupAWScallback as the callback handler. 
	xmodal.open(aws_setup_processing);
};

// credential recreation

var resetAWSsuccess = function(o){ 
	// mckay: get a series of values as a result from a successful call, then plug them into the modal using the beforeShow method. I'm inserting dummy values here. 
	aws_recreate_success.beforeShow = function(obj){ 
		var accessKey = 'accessKey1';
		var secretKey = 'recreatedSecretKey';
		obj.$modal.find('input.aws-accesskey').val(accessKey);
		obj.$modal.find('input.aws-secretkey').val(secretKey);
	};
	xmodal.closeAll();
	xmodal.open(aws_recreate_success);
};

var resetAWSfailure = function(o){ 
	// mckay: get error messages as a result from a failed call, then plug them into the modal using the beforeShow method. I'm inserting dummy values here. 
	aws_recreate_failure.beforeShow = function(obj){ 
		var errorMsg = '';  
		obj.$modal.find('.aws-errormessage').text(errorMsg);
	};
	xmodal.closeAll();
	xmodal.open(aws_recreate_failure);
};

var resetAWScallback = { 
	success: resetAWSsuccess,
	failure: resetAWSfailure,
	cache:false, // Turn off caching for IE
	scope:this
};

var resetAWS = function(){
	// insert jQuery GET request to Amazon. Use setupAWScallback as the callback handler. 
};
