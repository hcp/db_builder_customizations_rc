// set projectId as a global variable
var projectId = XNAT.data.context.projectID;

var upCounter = 0; //Each update added gets a new number. The next update to be created gets number equal to upCounter.
var upsActive = 0; //This is the number of updates that are active for this subject key.
/*
 * Administrative tracking of dataset updates 
 */

/* JSON object sort function. From http://stackoverflow.com/questions/979256/sorting-an-array-of-javascript-objects

var homes = [{

   "h_id": "3",
   "city": "Dallas",
   "state": "TX",
   "zip": "75201",
   "price": "162500"

}, {

   "h_id": "4",
   "city": "Bevery Hills",
   "state": "CA",
   "zip": "90210",
   "price": "319250"

}, {

   "h_id": "5",
   "city": "New York",
   "state": "NY",
   "zip": "00010",
   "price": "962500"

}];

// Sort object homes by price high to low
homes.sort(sort_by('price', true, parseInt));

// Sort by city, case-insensitive, A-Z
homes.sort(sort_by('city', false, function(a){return a.toUpperCase()}));
*/
var sort_by = function(field, reverse, primer){

   var key = primer ? 
       function(x) {return primer(x[field])} : 
       function(x) {return x[field]};

   reverse = [-1, 1][+!!reverse];

   return function (a, b) {
       return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
     } 
}

var displayDate = function(timestamp) { 
	var dateString = new Date(parseInt(timestamp)).toDateString(); 
	if (dateString == "Invalid Date") { 
		return dateString; 
	} else { 
		return dateString.slice(3); 
	}
} 

var displayUpdate = function(upObj,showProject) { 
	var href;
	if ((upObj.link != "NULL") && (upObj.link.indexOf("//") <0 )) { 
		href="//" + upObj.link; 
	} else { 
		href=upObj.link;
	}
	
	var upSet = '<tr class="up-container " id="up-container'+upCounter+'" name="up-container'+upCounter+'">'+
				'<td>'+displayDate(upObj.date)+'</td>';
	if (showProject) { 
		upSet += '<td>'+upObj.project+'</td>'; 
	} 
	upSet +=	'<td>'+upObj.update+'</td>'+
				'<td>';
	if (upObj.link != "NULL") { 
		upSet += '<a href="'+href+'" target="_blank">More details</a>';
	}
	upSet += 	'</td>';
	if(canEdit!="true"){
		upSet += '</tr>';
	}
	else{
		upSet += '<td class="deleteUpTD"><button class="btn btn-sm" onclick="deleteUp(\''+upCounter+'\', \''+upObj.date+'\', \''+upObj.update+'\', \''+upObj.link+'\');"><span class="icon icon-trash-red"></span></button></td></tr>';
	}
	return upSet; 
} 
 
// Add Update Function
var getUpdatesSuccess=function(o){
    var updatesJSON = YAHOO.lang.JSON.parse(o.responseText);
	
	var updateCount = updatesJSON.length; 
	updatesJSON.sort(sort_by('date',false,parseInt)); // sort timestamps in descending order

	if (updateCount) { 

		for(var i=0;i<updateCount;i++){
			// simple check for valid hyperlink
			
			var upSet = displayUpdate(updatesJSON[i],false); 
			
			upCounter=upCounter+1;
			upsActive= upsActive+1;
			/* add table row to existing updates table */
			$('#updates table tbody').append(upSet);
		}
	} else { 		
		var noUpdate = '<tr class="up-container" id="no-updates">'; 
		if(canEdit!="true"){
			noUpdate = noUpdate + '<td colspan="3">No Updates Found</td></tr>';
		} else { 
			noUpdate = noUpdate + '<td colspan="4">No Updates Found</td></tr>'; 
		} 
		$('#updates table tbody').append(noUpdate);
	}
};

// version that gets all updates for all projects and displays on the splash page
var getAllUpdatesSuccess=function(o){
    var updatesJSON = YAHOO.lang.JSON.parse(o.responseText);
	
	var updateCount = updatesJSON.length; 
	updatesJSON.sort(sort_by('date',false,parseInt)); // sort timestamps in descending order

	if (updateCount) { 
		for(var i=0;i<updatesJSON.length;i++){
			
			var upSet = displayUpdate(updatesJSON[i],true);
			
			upCounter=upCounter+1;
			upsActive= upsActive+1;
			/* add table row to existing updates table */
			$('#allUpdates table tbody').append(upSet);
		}
	} else { 		
		var noUpdate = '<tr class="up-container" id="no-updates">'; 
		if(canEdit!="true"){
			noUpdate = noUpdate + '<td colspan="4">No Updates Found</td></tr>';
		} else { 
			noUpdate = noUpdate + '<td colspan="5">No Updates Found</td></tr>'; 
		} 
		$('#updates table tbody').append(noUpdate);
	}
};

var getUpdatesFailure=function(o){
	var errorTr = "<tr><td colspan='3'>No updates found.</td></tr>";
	$('.updates table tbody').append(errorTr); 
};

var getAllUpdatesCallback = { 
	success:getAllUpdatesSuccess,
	failure:getUpdatesFailure,
	cache:false, // Turn off caching for IE
	scope:this
};

var getUpdatesCallback = {
	success:getUpdatesSuccess,
	failure:getUpdatesFailure,
	cache:false, // Turn off caching for IE
	scope:this
};

var upSuccess=function(o){
	xModalLoadingClose();
	xModalMessage('Update Added', 'The update has been added. It is now listed under "Update History" at the bottom of this page.');
};
var upFailure=function(o){
	xModalLoadingClose();
	if(o.status==401){
		xModalMessage('Session Expired', sessExpMsg);
		window.location=serverRoot+"/app/template/Login.vm";
	}else{
		xModalMessage('Adding New Update Error', 'Error ' + o.status + ': Change failed.');
	}
};

var updateCallback = {
	success:upSuccess,
	failure:upFailure,
	cache:false, // Turn off caching for IE
	scope:this
};

var upChangeSuccess=function(o){
};
var upChangeFailure=function(o){
};
var upChange = {
    success:upChangeSuccess,
    failure:upChangeFailure,
    cache:false, // Turn off caching for IE
    scope:this
};

deleteUp = function(upNumber, date, update, link){
    $('#up-container'+upNumber).remove();
    upsActive= upsActive-1;
    YAHOO.util.Connect.asyncRequest('DELETE',serverRoot + "/data/services/update?project="+projectId+"&date="+date+"&update="+update+"&link="+link,upChange,null,this);
};

addUpdate = function(timestamp,update,link){
	// convert date string to timestamp
	
	var url = (link) ? link : "NULL"; 
	YAHOO.util.Connect.asyncRequest('PUT',serverRoot + "/data/services/update?project="+projectId+"&date="+timestamp+"&update="+update+"&link="+url,updateCallback,null,this);
	
	var updateObj = { "date":timestamp, "project":projectId, "update":update, "link":url } 

	var upSet = displayUpdate(updateObj,false);
	
	upCounter=upCounter+1;
	upsActive= upsActive+1;
	/* if it exists, remove the "no updates" table row */
	$('#updates table #no-updates').slideUp(); 
	
	/* add table row to existing updates table */
	$('#updates table').append(upSet);
}

openUpdateModal = function(){
	var updateOptions = {} ;
	 updateOptions.action = function(){
		var date=document.getElementById("dateUp").value;
		var timestamp = Date.parse(date) + (12 * 60 * 60 * 1000); // adds 12 hours (43200000 milliseconds) to the date entered. 
		if ((timestamp != "Invalid Date") && (!isNaN(timestamp))) { 
			var update=document.getElementById("updateUp").value;
			var url=document.getElementById("linkUp").value;
			xModalLoadingOpen({title:'Please wait...'});
			addUpdate(timestamp,update,url);
		} else { 
			xModalMessage("Invalid Date Entry","You must enter a valid date in order to post an update. Try YYYY-MM-DD or MM/DD/YYYY."); 
			return false; 
		} 
	};
	 updateOptions.beforeShow = function(modal){
		var waitForInput = setInterval(function(){
			if (modal.is(':visible')) {
				clearInterval(waitForInput);
				modal.find('#dateUp').focus();
			}
		}, 100);
	};
	updateOptions.cancel= 'show';
	xModalMessage('Add Update', 'Date: <input type="text" name="dateUp" id="dateUp"><br>Update: <input type="text" name="updateUp" id="updateUp"><br>Link: <input type="text" name="linkUp" id="linkUp">', 'Add',  updateOptions);
};

/* Other utilities */

// display dataset permalink
openPermalinkModal = function(){
	xModalMessage('Dataset Permalink', 'Select and copy this URL to link to this dataset. <br><input style="width: 100%" type="text" name="link" value="'+location.hostname + '/data/projects/' + projectId +'" readonly>');
};

// Toolbar Cleanup - used on report pages and dashboard
function toolbarCleanup() { 
	var maxHeight = 0; 
	
	$('.toolset').each(function(){
		if ( $(this).height() > maxHeight ) { maxHeight = $(this).height(); } // get the max height of each toolset
	});
	$('.toolset').each(function(){
		$(this).height(maxHeight); // set the height of each equal to the max height
	});
}