// Add Article Function
// Requires a valid PubMed ID

var pubCounter = 0; //Each publication added gets a new number. The next publication to be created gets number equal to pubCounter.
var pubsActive = 0; //This is the number of publications that are active for this subject key.

var getPublicationsSuccess=function(o){
	var publicationsJSON = YAHOO.lang.JSON.parse(o.responseText);
	
	var pubCount = publicationsJSON.length; 
	
	if (pubCount) { 

		for(var i=0;i<pubCount;i++){
			var pubSet = '' +
					'<tr class="pub-container pmid'+publicationsJSON[i].pmid+'" id="pub-container'+pubCounter+'" name="pub-container'+pubCounter+'">'+
					'<td>'+decodeURIComponent(escape(publicationsJSON[i].authors))+'</td>'+
					'<td>'+decodeURIComponent(escape(publicationsJSON[i].articleTitle))+'</td>'+
					'<td>'+decodeURIComponent(escape(publicationsJSON[i].publication))+'</td>'+
					'<td nowrap>'+publicationsJSON[i].date+'</td>'+
					'<td><a href="http://www.ncbi.nlm.nih.gov/pubmed/'+publicationsJSON[i].pmid+'" target="_blank">'+publicationsJSON[i].pmid+'</a></td>'
			if((typeof(publicationsJSON[i].doi) != "undefined") && (publicationsJSON[i].doi!="")){
				pubSet = pubSet +'<td><a href="http://dx.doi.org/'+publicationsJSON[i].doi+'" target="_blank">'+publicationsJSON[i].doi+'</a></td>';
			}
			else{
				pubSet = pubSet +'<td></td>';
			}
			if(canEdit!="true"){
				pubSet = pubSet + '<td class="removePub hidden"></td></tr>';
			}
			else{
				pubSet = pubSet + '<td class="deletePubTD"><button class="btn btn-sm" onclick="deletePub('+pubCounter+', '+publicationsJSON[i].pmid+');"><span class="icon icon-trash-red"></span></a></td></tr>';
			}
			pubCounter=pubCounter+1;
			pubsActive= pubsActive+1;
			/* add table row to existing publications table */
			$("#publications table tbody").append(pubSet);
		}
		$("#pubCountP").html("Publications Listed: "+pubsActive);
		
	} else { 
		var noPubs = '<tr class="pub-container" id="no-publications">'; 
		if(canEdit!="true"){
			noPubs = noPubs + '<td colspan="6">No Publications Found</td></tr>';
		} else { 
			noPubs = noPubs + '<td colspan="7">No Publications Found</td></tr>';
		} 
		$("#publications table tbody").append(noPubs);
		$("#pubCountP").html("No Publications Listed");
	} 

};

var getPublicationsFailure=function(o){
};

var getPublicationsCallback = {
	success:getPublicationsSuccess,
	failure:getPublicationsFailure,
	cache:false, // Turn off caching for IE
	scope:this
};

deletePub = function(pubNumber, pmidString){
	$('#pub-container'+pubNumber).remove();
	pubsActive= pubsActive-1;
	$( "#pubCountP").html("Publications Listed: "+pubsActive);
	YAHOO.util.Connect.asyncRequest('PUT',serverRoot + "/data/services/removePublication?pmid="+pmidString+"&project="+projectId+"&XNAT_CSRF="+csrfToken,publicationChange,null,this);
};

var getPMIDSuccess=function(o){
	xModalLoadingClose();
	var xmlDoc = jq.parseXML(o.responseText);
	var error = $(xmlDoc).find("eFetchResult").find("ERROR").text();

	var articleCount = $(xmlDoc).find("PubmedArticleSet").children("PubmedArticle").size();


	if(error!='' || articleCount<1){
		xModalMessage('PubMed Error', 'This PMID was not found.');
	}
	else {
		var pmidString = $(xmlDoc).find("MedlineCitation").children("pmid").text();
		var articles = $(xmlDoc).find("ArticleIdList").children("ArticleId");
		var doiString = "";
		for (var i = 0; i < articles.length; i++) {
			if(articles[i].getAttribute('IdType')=="doi"){
				doiString = articles[i].textContent;
			}
		}


		var pmidClass = ".pmid"+pmidString;
		if($(pmidClass).length>0){
			xModalMessage('Duplicate Article Error', 'The PMID '+pmidString+' has already been added.');
		}
		else{ //if there was no error obtaining the article
			var authors = $(xmlDoc).find("AuthorList").children("Author");
			var authorsString = "";
			if(authors.length>0){
				authorsString  = authorsString + $(authors[0]).find("ForeName").text() + ' ' + $(authors[0]).find("LastName").text();
				if(authors.length>2){
					authorsString  = authorsString + ", et al.";
				}
				else if(authors.length==2){
					authorsString  = authorsString + " and " + $(authors[1]).find("ForeName").text() + ' ' + $(authors[1]).find("LastName").text();
				}
			}
			else{
				authorsString = "[No authors listed]";
			}
//                for(var i = 0; i < authors.length; i++){
//                    if(i!=0 && !((i==authors.length-1 && authors.length==2))){
//                        authorsString = authorsString + ', ';
//                    }
//                    if(i==authors.length-1 && authors.length!=1){
//                        authorsString = authorsString + 'and ';
//                    }
//                    authorsString  = authorsString + $(authors[i]).find("ForeName").text() + ' ' + $(authors[i]).find("LastName").text();
//                }
			var articleTitleString = $(xmlDoc).find("ArticleTitle").text();
			var titleString = $(xmlDoc).find("Title").text();
			var monthDay = ''
			var month = $(xmlDoc).find("PubDate").find("Month").text();
			var day = $(xmlDoc).find("PubDate").find("Day").text();
			if(month!=''){
				if(day!=''){

					monthDay = month + ' ' + day + ', ';
				}
				else{
					monthDay = month + ', ';
				}
			}

			var dateString = monthDay + $(xmlDoc).find("PubDate").find("Year").text();

			pubSet = '' +
					'<tr class="pub-container pmid'+pmidString+'" id="pub-container'+pubCounter+'" name="pub-container'+pubCounter+'">'+
					'<td>'+authorsString+'</td>'+
					'<td>'+articleTitleString+'</td>'+
					'<td>'+titleString+'</td>'+
					'<td nowrap>'+dateString+'</td>'+
					'<td><a href="http://www.ncbi.nlm.nih.gov/pubmed/'+pmidString+'" target="_blank">'+pmidString+'</a></td>'+
					'<td><a href="http://dx.doi.org/'+doiString+'" target="_blank">'+doiString+'</a></td>';
			if(canEdit!="true"){
				pubSet = pubSet + '</tr>';
			}
			else{
				pubSet = pubSet + '<td class="deletePubTD"><button class="btn btn-sm" onclick="deletePub('+pubCounter+', '+pmidString+');"><span class="icon icon-trash-red"></span></button></td></tr>';
			}

			pubCounter=pubCounter+1;
			pubsActive= pubsActive+1;
			$( "#pubCountP").html("Publications Listed: "+pubsActive);
			
			/* if it exists, remove the "no publications" table row */
			$('#publications table #no-publications').slideUp(); 

			/* append new publication to the existing publication table */
			$('#publications table').append(pubSet);
			xModalLoadingClose();
			console.log("YAHOO.util.Connect.asyncRequest('PUT',"+ serverRoot + "/data/services/addPublication?articleTitle="+unescape(encodeURIComponent(articleTitleString))+"&authors="+unescape(encodeURIComponent(authorsString))+"&publication="+unescape(encodeURIComponent(titleString))+"&date="+dateString+"&pmid="+pmidString+"&doi="+doiString+"&project="+projectId+"&XNAT_CSRF="+csrfToken+",publicationChange,null,this);"); 
			YAHOO.util.Connect.asyncRequest('PUT',serverRoot + "/data/services/addPublication?articleTitle="+unescape(encodeURIComponent(articleTitleString))+"&authors="+unescape(encodeURIComponent(authorsString))+"&publication="+unescape(encodeURIComponent(titleString))+"&date="+dateString+"&pmid="+pmidString+"&doi="+doiString+"&project="+projectId+"&XNAT_CSRF="+csrfToken,publicationChange,null,this);
			xModalMessage('Publication Added', 'The publication "'+articleTitleString+'" has been added. It is now listed under "Publications" at the bottom of this page.');
		}
	}
};

var publicationSuccess=function(o){
	updateCompleteness();
};
var publicationFailure=function(o){
};
var publicationChange = {
	success:publicationSuccess,
	failure:publicationFailure,
	cache:false, // Turn off caching for IE
	scope:this
};


var getPMIDFailure=function(o){
	xModalLoadingClose();
	if(o.status==401){
		xModalMessage('Session Expired', sessExpMsg);
		window.location=serverRoot+"/app/template/Login.vm";
	}else{
		xModalMessage('Subject Key PMID Error', 'Error ' + o.status + ': PMID failure.');
	}
};

var getPMID = {
	success:getPMIDSuccess,
	failure:getPMIDFailure,
	cache:false, // Turn off caching for IE
	scope:this
};

addByPmid = function(pmidToAdd){
	YAHOO.util.Connect.asyncRequest('GET',serverRoot + "/data/services/pubMed/"+pmidToAdd,getPMID,null,this);
}

openPubModal = function(){
	var pubOptions = {} ;
	pubOptions.action = function(){
		xModalLoadingOpen({title:'Please wait...'});
		var pubMedID=document.getElementById("pubMedID").value;
		addByPmid(pubMedID);
	};
	pubOptions.afterShow = function(modal){
        modal.__modal.find('#pubMedID').focus();
	};
	pubOptions.cancel= 'show';
	xModalMessage('Link to Article', 'Enter Article PubMed ID (required): <input type="text" name="pubMedID" id="pubMedID">', 'Look Up', pubOptions);
};



