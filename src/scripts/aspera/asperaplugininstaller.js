
var currentProto;

// pick up the protocol and use that to download the latest connect files
// however if we are running off the local file system use http

var handleConnectReady = function() {
       // Called if Aspera Connect is installed and meets version requirements.
  jq('#starting_download').show();
  doDownload();	// callback to function on page to init the download
};

var handleInstallError = function() {
  // Called if an install error occurs. Display some text.
  showMessage('page_body', 'Aspera plugin installation error', 'An error occurred installing the Aspera plugin. Please contact HCP support: support@humanconnectome.org');
};

var handleInstall = function() {
  jq('#no_aspera_plugin').show();
   // Called if an install is required.
   CDB.aspera.connectInstaller.startEmbeddedInstall({
       installError : handleInstallError,
       stylesheet : currentProto + "//d2fvxkmjao6pcr.cloudfront.net/custom.css",
       installDismiss : function() {
    	//Called if install is dismissed by the user, used in connect 3.0 onwards
        showMessage("page_body", "Notification", "Please install the latest version of the Aspera Connect plugin and restart your web browser.");
    	}    
   });
};

var initAsperaConnect = function () {
	// namespace this guy so he doesn't collide with the one in htmlClose.vm
    try{
        XNAT.app.aspera = {};
        XNAT.app.aspera.AW4 = new AW4.Connect();
        XNAT.app.aspera.statusEventListener = function (eventType, data) {
            switch(data) {
              case AW4.Connect.STATUS.INITIALIZING: {
                  console.log("Aspera Download Status:  Launching");
                  break;
              };
              case AW4.Connect.STATUS.FAILED: {
                  console.log("Aspera Download Status: Not running");
                  break;
              };
              case AW4.Connect.STATUS.OUTDATED: {
                  console.log("Aspera Download Status:  Not up to date");
                  break;
              };
              case AW4.Connect.STATUS.RUNNING: {
                  console.log("Aspera Download Status:  Running");
                  break;
              }
              default:
              ;
            }
        };
        XNAT.app.aspera.AW4.addEventListener(AW4.Connect.EVENT.STATUS, XNAT.app.aspera.statusEventListener);
        XNAT.app.aspera.AW4.initSession();
        setTimeout(function() {
            if (XNAT.app.aspera.AW4.getStatus()==AW4.Connect.STATUS.RUNNING) {
                handleConnectReady();
            } else {
                // Allow more time before bringing up install dialog.
                setTimeout(function() {
                    if (XNAT.app.aspera.AW4.getStatus()==AW4.Connect.STATUS.RUNNING) {
                        handleConnectReady();
                    } else {
                        handleInstall();
                    } 
                }, 1500);
            } 
        }, 500);
      } catch(e){
	console.log(e);
        //catch and just suppress error
      }
};

$(document).on("asperaScriptsLoaded",{},function() {
	//console.log("Caught asperaScriptsLoaded");
	CDB.aspera.initializeInstaller();
	initAsperaConnect();
});

