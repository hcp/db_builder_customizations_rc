// MRH-2018/05/25:  Oddly, the getScripts() call didn't work correctly when loading locally without adding these writes to the console.
console.log("loading local connectversions.js script");
if (typeof AW === "undefined") var AW = {};
AW.connectVersions = {
    "title": "Aspera Connect - Latest Version",
    "id": "http://asperasoft.com/download/sw/connect/auto/connectversions.js",
    "updated": "2012-10-30T10:16:00+07:00",
    "entries": [
        {
            "title": "Aspera Connect for Windows",
            "platform": {
                "os": "win32",
                "version": "6.0"
            },
            "navigator": {
                "platform": "Win32-Vista"
            },
            "version": "3.8.0.158555",
            "id": "urn:uuid:589F9EE5-0489-4F73-9982-A612FAC70C4E",
            "updated": "2012-10-30T10:16:00+07:00",
            "links": [
                {
                    "title": "Windows Vista Auto Installer",
                    "type": "application/octet-stream",
                    "href": "bin/IBMAsperaConnect-ML-3.8.0.158555.msi",
                    "hreflang": "en",
                    "rel": "enclosure"
                },
                {
                    "title": "Windows Installer",
                    "type": "application/octet-stream",
                    "href": "bin/IBMAsperaConnect-ML-3.8.0.158555.msi",
                    "hreflang": "en",
                    "rel": "enclosure-manual"
                },
                {
                    "title": "Aspera Connect HTML Documentation for Windows",
                    "type": "text/html",
                    "href": "docs/win/en/html/index.html",
                    "hreflang": "en",
                    "rel": "documentation"
                },
                {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/fr-fr/pdf/Connect_User_3.8.0_Windows_fr-fr.pdf",
                      "hreflang": "fr-fr",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/en/pdf/Connect_User_3.8.0_Windows.pdf",
                      "hreflang": "en",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/es-es/pdf/Connect_User_3.8.0_Windows_es-es.pdf",
                      "hreflang": "es-es",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/zh-cn/pdf/Connect_User_3.8.0_Windows_zh-cn.pdf",
                      "hreflang": "zh-cn",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/ja-jp/pdf/Connect_User_3.8.0_Windows_ja-jp.pdf",
                      "hreflang": "ja-jp",
                      "rel": "documentation"
                  },
  
                {
                    "title": "Aspera Connect Release Notes for Windows",
                    "type": "text/html",
                    "href": "http://www.asperasoft.com/en/release_notes/default_1/release_notes_55",
                    "hreflang": "en",
                    "rel": "release-notes"
                },
                {
                    "title": "Aspera Connect Removal Tool",
                    "type": "application/octet-stream",
                    "href": "bin/AsperaConnectCleanup.exe",
                    "hreflang": "en",
                    "rel": "cleanup"
                }
            ]
        },
        {
            "title": "Aspera Connect for Windows 64-bit",
            "platform": {
                "os": "win64",
                "version": "6.0"
            },
            "navigator": {
                "platform": "Win64-Vista"
            },
            "version": "3.8.0.158555",
            "id": "urn:uuid:A3820D20-083E-11E2-892E-0800200C9A66",
            "updated": "2012-10-30T10:16:00+07:00",
            "links": [
                {
                    "title": "Windows Vista Auto Installer",
                    "type": "application/octet-stream",
                    "href": "bin/IBMAsperaConnect-ML-3.8.0.158555.msi",
                    "hreflang": "en",
                    "rel": "enclosure"
                },
                {
                    "title": "Windows Installer",
                    "type": "application/octet-stream",
                    "href": "bin/IBMAsperaConnect-ML-3.8.0.158555.msi",
                    "hreflang": "en",
                    "rel": "enclosure"
                },
                {
                    "title": "Aspera Connect HTML Documentation for Windows",
                    "type": "text/html",
                    "href": "docs/win/en/html/index.html",
                    "hreflang": "en",
                    "rel": "documentation"
                },
                {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/fr-fr/pdf/Connect_User_3.8.0_Windows_fr-fr.pdf",
                      "hreflang": "fr-fr",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/en/pdf/Connect_User_3.8.0_Windows.pdf",
                      "hreflang": "en",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/es-es/pdf/Connect_User_3.8.0_Windows_es-es.pdf",
                      "hreflang": "es-es",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/zh-cn/pdf/Connect_User_3.8.0_Windows_zh-cn.pdf",
                      "hreflang": "zh-cn",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/ja-jp/pdf/Connect_User_3.8.0_Windows_ja-jp.pdf",
                      "hreflang": "ja-jp",
                      "rel": "documentation"
                  },
  
                {
                    "title": "Aspera Connect Release Notes for Windows",
                    "type": "text/html",
                    "href": "http://www.asperasoft.com/en/release_notes/default_1/release_notes_55",
                    "hreflang": "en",
                    "rel": "release-notes"
                },
                {
                    "title": "Aspera Connect Removal Tool",
                    "type": "application/octet-stream",
                    "href": "bin/AsperaConnectCleanup64.exe",
                    "hreflang": "en",
                    "rel": "cleanup"
                }
            ]
        },
        {
            "title": "Aspera Connect for Windows XP",
            "platform": {
                "os": "win32",
                "version": "5.1"
            },
            "navigator": {
                "platform": "Win32"
            },
            "version": "3.8.0.158555",
            "id": "urn:uuid:589F9EE5-0489-4F73-9982-A612FAC70C4E",
            "updated": "2012-10-30T10:16:00+07:00",
            "links": [
                {
                    "title": "Windows Installer",
                    "type": "application/octet-stream",
                    "href": "bin/IBMAsperaConnect-ML-3.8.0.158555.msi",
                    "hreflang": "en",
                    "rel": "enclosure"
                },
                {
                    "title": "Aspera Connect HTML Documentation for Windows",
                    "type": "text/html",
                    "href": "docs/win/en/html/index.html",
                    "hreflang": "en",
                    "rel": "documentation"
                },
                {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/fr-fr/pdf/Connect_User_3.8.0_Windows_fr-fr.pdf",
                      "hreflang": "fr-fr",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/en/pdf/Connect_User_3.8.0_Windows.pdf",
                      "hreflang": "en",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/es-es/pdf/Connect_User_3.8.0_Windows_es-es.pdf",
                      "hreflang": "es-es",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/zh-cn/pdf/Connect_User_3.8.0_Windows_zh-cn.pdf",
                      "hreflang": "zh-cn",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/ja-jp/pdf/Connect_User_3.8.0_Windows_ja-jp.pdf",
                      "hreflang": "ja-jp",
                      "rel": "documentation"
                  },
  
                {
                    "title": "Aspera Connect Release Notes for Windows",
                    "type": "text/html",
                    "href": "http://www.asperasoft.com/en/release_notes/default_1/release_notes_55",
                    "hreflang": "en",
                    "rel": "release-notes"
                },
                {
                    "title": "Aspera Connect Removal Tool",
                    "type": "application/octet-stream",
                    "href": "bin/AsperaConnectCleanup.exe",
                    "hreflang": "en",
                    "rel": "cleanup"
                }
            ]
        },
        {
            "title": "Aspera Connect for Windows XP 64-bit",
            "platform": {
                "os": "win64",
                "version": "5.1"
            },
            "navigator": {
                "platform": "Win64"
            },
            "version": "3.8.0.158555",
            "id": "urn:uuid:55425020-083E-11E2-892E-0800200C9A66",
            "updated": "2012-10-30T10:16:00+07:00",
            "links": [
                {
                    "title": "Windows Installer",
                    "type": "application/octet-stream",
                    "href": "bin/IBMAsperaConnect-ML-3.8.0.158555.msi",
                    "hreflang": "en",
                    "rel": "enclosure"
                },
                {
                    "title": "Aspera Connect HTML Documentation for Windows",
                    "type": "text/html",
                    "href": "docs/win/en/html/index.html",
                    "hreflang": "en",
                    "rel": "documentation"
                },
                {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/fr-fr/pdf/Connect_User_3.8.0_Windows_fr-fr.pdf",
                      "hreflang": "fr-fr",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/en/pdf/Connect_User_3.8.0_Windows.pdf",
                      "hreflang": "en",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/es-es/pdf/Connect_User_3.8.0_Windows_es-es.pdf",
                      "hreflang": "es-es",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/zh-cn/pdf/Connect_User_3.8.0_Windows_zh-cn.pdf",
                      "hreflang": "zh-cn",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Windows",
                      "type": "application/pdf",
                      "href": "docs/win/ja-jp/pdf/Connect_User_3.8.0_Windows_ja-jp.pdf",
                      "hreflang": "ja-jp",
                      "rel": "documentation"
                  },
  
                {
                    "title": "Aspera Connect Release Notes for Windows",
                    "type": "text/html",
                    "href": "http://www.asperasoft.com/en/release_notes/default_1/release_notes_55",
                    "hreflang": "en",
                    "rel": "release-notes"
                },
                {
                    "title": "Aspera Connect Removal Tool",
                    "type": "application/octet-stream",
                    "href": "bin/AsperaConnectCleanup64.exe",
                    "hreflang": "en",
                    "rel": "cleanup"
                }
            ]
        },
        {
            "title": "Aspera Connect for Mac Intel",
            "platform": {
                "os": "Mac OS X",
                "arch": "Intel",
                "version": "10.7"
            },
            "navigator": {
                "platform": "MacIntel"
            },
            "version": "3.8.0.158555",
            "id": "urn:uuid:D8629AD2-6898-4811-A46F-2AF386531BFF",
            "updated": "2012-10-30T10:16:00+07:00",
            "links": [
                {
                    "title": "Mac Intel Installer",
                    "type": "application/octet-stream",
                    "href": "bin/IBMAsperaConnectInstaller-3.8.0.158555.dmg",
                    "hreflang": "en",
                    "rel": "enclosure"
                },
                {
                    "title": "Aspera Connect for Mac HTML Documentation",
                    "type": "text/html",
                    "href": "",
                    "hreflang": "en",
                    "rel": "documentation"
                },
                {
                      "title": "Aspera Connect PDF Documentation for Mac OS",
                      "type": "application/pdf",
                      "href": "docs/osx/es-es/pdf/Connect_User_3.8.0_OSX_es-es.pdf",
                      "hreflang": "es-es",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Mac OS",
                      "type": "application/pdf",
                      "href": "docs/osx/zh-cn/pdf/Connect_User_3.8.0_OSX_zh-cn.pdf",
                      "hreflang": "zh-cn",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Mac OS",
                      "type": "application/pdf",
                      "href": "docs/osx/ja-jp/pdf/Connect_User_3.8.0_OSX_ja-jp.pdf",
                      "hreflang": "ja-jp",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Mac OS",
                      "type": "application/pdf",
                      "href": "docs/osx/en/pdf/Connect_User_3.8.0_OSX.pdf",
                      "hreflang": "en",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Mac OS",
                      "type": "application/pdf",
                      "href": "docs/osx/fr-fr/pdf/Connect_User_3.8.0_OSX_fr-fr.pdf",
                      "hreflang": "fr-fr",
                      "rel": "documentation"
                  },
  
                {
                    "title": "Aspera Connect for Mac Release Notes",
                    "type": "text/html",
                    "href": "http://www.asperasoft.com/en/release_notes/default_1/release_notes_54",
                    "hreflang": "en",
                    "rel": "release-notes"
                }
            ]
        },

       {
            "title": "Aspera Connect for Linux 64",
            "platform": {
                "os": "Linux"
            },
            "navigator": {
                "platform": "Linux x86_64"
            },
            "version": "3.8.0.158533",
            "id": "urn:uuid:97F94DF0-22B1-11E2-81C1-0800200C9A66",
            "updated": "2012-10-30T10:16:00+07:00",
            "links": [
                {
                    "title": "Linux 64-bit Installer",
                    "type": "application/octet-stream",
                    "href": "bin/ibm-aspera-connect-3.8.0.158533-linux-g2.12-64.tar.gz",
                    "hreflang": "en",
                    "rel": "enclosure"
                },
                {
                    "title": "Aspera Connect for Linux HTML Documentation",
                    "type": "text/html",
                    "href": "",
                    "hreflang": "en",
                    "rel": "documentation"
                },
                {
                      "title": "Aspera Connect PDF Documentation for Linux",
                      "type": "application/pdf",
                      "href": "docs/linux/fr-fr/pdf/Connect_User_3.8.0_Linux_fr-fr.pdf",
                      "hreflang": "fr-fr",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Linux",
                      "type": "application/pdf",
                      "href": "docs/linux/es-es/pdf/Connect_User_3.8.0_Linux_es-es.pdf",
                      "hreflang": "es-es",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Linux",
                      "type": "application/pdf",
                      "href": "docs/linux/en/pdf/Connect_User_3.8.0_Linux.pdf",
                      "hreflang": "en",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Linux",
                      "type": "application/pdf",
                      "href": "docs/linux/ja-jp/pdf/Connect_User_3.8.0_Linux_ja-jp.pdf",
                      "hreflang": "ja-jp",
                      "rel": "documentation"
                  },
                  {
                      "title": "Aspera Connect PDF Documentation for Linux",
                      "type": "application/pdf",
                      "href": "docs/linux/zh-cn/pdf/Connect_User_3.8.0_Linux_zh-cn.pdf",
                      "hreflang": "zh-cn",
                      "rel": "documentation"
                  },
  
                {
                    "title": "Aspera Connect for Linux Release Notes",
                    "type": "text/html",
                    "href": "http://www.asperasoft.com/en/release_notes/default_1/release_notes_54",
                    "hreflang": "en",
                    "rel": "release-notes"
                }
            ]
        }
    ]
}
;
