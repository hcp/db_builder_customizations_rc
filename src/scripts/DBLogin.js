initRegCallback={
	success:function(o){
		regSuccess(o);
			},
	failure:function(o){
                document.getElementById("reg-proc-div").style.display="none";
                document.getElementById("reg-proc-contents").style.display="none";
		if (o.responseText.toLowerCase().indexOf("agree to the data use terms")>=0) {
			showTerms();
		} else {
			regFailed(o);
		}
	},
	scope:this
};
wbInitRegCallback={
	success:function(o){
		wbRegSuccess(o);
			},
	failure:function(o){
                document.getElementById("reg-proc-div").style.display="none";
                document.getElementById("reg-proc-contents").style.display="none";
		if (o.responseText.toLowerCase().indexOf("agree to the data use terms")>=0) {
			showTerms();
		} else {
			regFailed(o);
		}
	},
	scope:this
};
p2InitRegCallback={
	success:function(o){
		p2RegSuccess(o);
			},
	failure:function(o){
                document.getElementById("reg-proc-div").style.display="none";
                document.getElementById("reg-proc-contents").style.display="none";
		if (o.responseText.toLowerCase().indexOf("agree to the data use terms")>=0) {
			showTerms();
		} else {
			regFailed(o);
		}
	},
	scope:this
};
regCallback={
	success:function(o){
		regSuccess(o);
			},
	failure:function(o){
		regFailed(o);
	},
	scope:this
};
function regFailed(o) {
		if (o.responseText != null && o.responseText.length<100) {
			document.getElementById("reg-error-bar").innerHTML=o.responseText;
		} else {
			document.getElementById("reg-error-bar").innerHTML="The server could not process the registration"
		}
                document.getElementById("reg-proc-div").style.display="none";
                document.getElementById("reg-proc-contents").style.display="none";
		document.getElementById("reg-error-bar").style.display="block";
		document.getElementById("reg-error-bar").style.visibility="visible";
		ebNotify("Registration could not be completed");
		// Currently not using captcha
		//Recaptcha.reload();
		showReg();
}
function regSuccess(o) {
		ebNotify(o.responseText);
		regClose();
		document.getElementById("username").value=document.getElementById("reg-username").value;
		if (o.responseText.indexOf("Accepted")>0) {
                	document.getElementById("reg-proc-contents").innerHTML="<span style='color:#99FF99'>Registration accepted. &nbsp;Proceeding with login.</span>";
			document.getElementById("password").value=document.getElementById("reg-pw").value;
			document.getElementById("username").readOnly="true";
			document.getElementById("password").readOnly="true";
			document.getElementById("form1-submit").disabled=="true";
			document.getElementById("form1").submit();
		} else {
			document.getElementById("password").focus();
		}
}
function wbRegSuccess(o) {
		wbSuccess(o);
}
wbCallback={
	success:function(o){
		wbSuccess(o);
			},
	failure:function(o){
		wbFailed(o);
	},
	scope:this
};
function wbFailed(o) {
	regFailed(o);
}
function wbSuccess(o) {
		ebNotify(o.responseText);
		if (o.responseText.indexOf("Accepted")>0) {
			window.top.location = "/?view=ThankYou";
		} 
}



function p2RegSuccess(o) {
		p2Success(o);
}
p2Callback={
	success:function(o){
		p2Success(o);
			},
	failure:function(o){
		p2Failed(o);
	},
	scope:this
};
function p2Failed(o) {
	regFailed(o);
}
function p2Success(o) {
		ebNotify(o.responseText);
		if (o.responseText.indexOf("Accepted")>0) {
			window.top.location = "/?view=ThankYou";
		} 
}



function ebNotify(txt) {
	document.getElementById("error-bar").style.display="block";
	document.getElementById("error-bar").style.visibility="visible";
	document.getElementById("error-bar").innerHTML=txt;
}
function register() {
	document.getElementById("reg-error-bar").style.display="none";
	document.getElementById("reg-error-bar").style.visibility="hidden";
	showReg();
}
function showReg() {
	//document.getElementById("page-mask").style.display="block";
	document.getElementById("reg-div").style.display="block";
	document.getElementById("agree-div").style.display="none";
	document.forms['regForm'].elements['reg-username'].focus();
}
function regClose() {
	//document.getElementById("page-mask").style.display="none";
	document.getElementById("reg-div").style.display="none";
	document.getElementById("agree-div").style.display="none";
}
function showTerms() {
	//document.getElementById("page-mask").style.display="block";
	document.getElementById("reg-div").style.display="none";
	document.getElementById("agree-div").style.display="block";
	document.getElementById("btn-agree").focus();
}
function doRegister(connectURI) {
	var formObject = document.getElementById('regForm');
	document.getElementById('agreeToDataUseTerms').value="Y";
	YAHOO.util.Connect.setForm(formObject,false);
	document.getElementById('reg-proc-div').style.display="inline";
	document.getElementById('reg-proc-contents').innerHTML="Processing registration, please wait........";
	document.getElementById('reg-proc-contents').style.display="inline";
	YAHOO.util.Connect.asyncRequest('POST',connectURI,regCallback,null);
}
function wbRegister(connectURI) {
	var formObject = document.getElementById('regForm');
	document.getElementById('agreeToWBTerms').value="Y";
	YAHOO.util.Connect.setForm(formObject,false);
	document.getElementById('reg-proc-div').style.display="inline";
	document.getElementById('reg-proc-contents').innerHTML="Processing registration, please wait........";
	document.getElementById('reg-proc-contents').style.display="inline";
	YAHOO.util.Connect.asyncRequest('POST',connectURI,wbCallback,null);
}
function p2Register(connectURI) {
	var formObject = document.getElementById('regForm');
	document.getElementById('agreeToP2Terms').value="Y";
	YAHOO.util.Connect.setForm(formObject,false);
	document.getElementById('reg-proc-div').style.display="inline";
	document.getElementById('reg-proc-contents').innerHTML="Processing registration, please wait........";
	document.getElementById('reg-proc-contents').style.display="inline";
	YAHOO.util.Connect.asyncRequest('POST',connectURI,p2Callback,null);
}
function initRegisterCall(connectURI) {
	var formObject = document.getElementById('regForm');
	YAHOO.util.Connect.setForm(formObject,false);
	document.getElementById('reg-proc-div').style.display="inline";
	document.getElementById('reg-proc-contents').innerHTML="Processing registration, please wait........";
	document.getElementById('reg-proc-contents').style.display="inline";
	YAHOO.util.Connect.asyncRequest('POST',connectURI,initRegCallback,null);
}
function wbInitRegisterCall(connectURI) {
	var formObject = document.getElementById('regForm');
	YAHOO.util.Connect.setForm(formObject,false);
	document.getElementById('reg-proc-div').style.display="inline";
	document.getElementById('reg-proc-contents').innerHTML="Processing registration, please wait........";
	document.getElementById('reg-proc-contents').style.display="inline";
	YAHOO.util.Connect.asyncRequest('POST',connectURI,wbInitRegCallback,null);
}
function p2InitRegisterCall(connectURI) {
	var formObject = document.getElementById('regForm');
	YAHOO.util.Connect.setForm(formObject,false);
	document.getElementById('reg-proc-div').style.display="inline";
	document.getElementById('reg-proc-contents').innerHTML="Processing registration, please wait........";
	document.getElementById('reg-proc-contents').style.display="inline";
	YAHOO.util.Connect.asyncRequest('POST',connectURI,p2InitRegCallback,null);
}
function agreeChange(ele) {
	if (ele.checked) {
		document.getElementById("btn-agree").disabled=false;
	} else {
		document.getElementById("btn-agree").disabled=true;
	}
}
function noThanks() {
	document.getElementById('agreeToDataUseTerms').value="N";
	regClose();
	ebNotify("You must agree to Data Use Terms");
}
dataUseCallback={
	success:function(o){
		regClose();
		document.getElementById("doagree-div").style.display="none";
		document.getElementById("username").value=document.getElementById("doagree-user").value;
		document.getElementById("password").value=document.getElementById("doagree-pw").value;
		ebNotify("Data use terms accepted.  Proceeding with login....");
		document.getElementById("username").readOnly="true";
		document.getElementById("password").readOnly="true";
		document.getElementById("form1-submit").disabled=="true";
		document.getElementById("form1").submit();
			},
	failure:function(o){
		//document.getElementById("reg-error-bar").style.display="block";
		//document.getElementById("reg-error-bar").style.visibility="visible";
		//document.getElementById("doagree-div").style.display="none";
		//regClose();
		//ebNotify("Could not register data use acceptance");
		alert("Could not register data use acceptance, This could be a problem with the account information provided or a problem on the server.");
	},
	scope:this
};
wbDataUseCallback={
	success:function(o){
		window.top.location = "/?view=ThankYou";
			},
	failure:function(o){
		//document.getElementById("reg-error-bar").style.display="block";
		//document.getElementById("reg-error-bar").style.visibility="visible";
		//document.getElementById("doagree-div").style.display="none";
		//regClose();
		//ebNotify("Could not register data use acceptance");
		alert("Could not register data use acceptance, This could be a problem with the account information provided or a problem on the server.");
	},
	scope:this
};
p2DataUseCallback={
	success:function(o){
		window.top.location = "/?view=ThankYou";
			},
	failure:function(o){
		//document.getElementById("reg-error-bar").style.display="block";
		//document.getElementById("reg-error-bar").style.visibility="visible";
		//document.getElementById("doagree-div").style.display="none";
		//regClose();
		//ebNotify("Could not register data use acceptance");
		alert("Could not register data use acceptance, This could be a problem with the account information provided or a problem on the server.");
	},
	scope:this
};
function doAgree(connectURI) {
	var formObject = document.getElementById('doagree-form');
	YAHOO.util.Connect.setForm(formObject,false);
	YAHOO.util.Connect.asyncRequest('POST',connectURI,dataUseCallback,null);
}
function doAgreeLogin(val) {
   	document.getElementById("doagree-user").value=val;
	document.getElementById("datause-agree").value="true";
	//document.getElementById("page-mask").style.display="block";
	document.getElementById("reg-div").style.display="none";
	document.getElementById("agree-div").style.display="none";
	document.getElementById("doagree-div").style.display="block";
	if (val == undefined || val.length<1) {
   		document.getElementById("doagree-user").focus();
	} else {
   		document.getElementById("doagree-pw").focus();
	}
}
function doAgreeWB(connectURI) {
	var formObject = document.getElementById('doagree-form');
	YAHOO.util.Connect.setForm(formObject,false);
	YAHOO.util.Connect.asyncRequest('POST',connectURI,wbDataUseCallback,null);
}
function doAgreeWBLogin(val) {
   	document.getElementById("doagree-user").value=val;
	document.getElementById("wb-datause-agree").value="true";
	//document.getElementById("page-mask").style.display="block";
	document.getElementById("reg-div").style.display="none";
	document.getElementById("agree-div").style.display="none";
	document.getElementById("doagree-div").style.display="block";
	if (val == undefined || val.length<1) {
   		document.getElementById("doagree-user").focus();
	} else {
   		document.getElementById("doagree-pw").focus();
	}
}
function doAgreeP2(connectURI) {
	var formObject = document.getElementById('doagree-form');
	YAHOO.util.Connect.setForm(formObject,false);
	YAHOO.util.Connect.asyncRequest('POST',connectURI,p2DataUseCallback,null);
}
function doAgreeP2Login(val) {
   	document.getElementById("doagree-user").value=val;
	document.getElementById("p2-datause-agree").value="true";
	//document.getElementById("page-mask").style.display="block";
	document.getElementById("reg-div").style.display="none";
	document.getElementById("agree-div").style.display="none";
	document.getElementById("doagree-div").style.display="block";
	if (val == undefined || val.length<1) {
   		document.getElementById("doagree-user").focus();
	} else {
   		document.getElementById("doagree-pw").focus();
	}
}
