// indexOf method for IE8
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
        "use strict";
        if (this == null) {
            throw new TypeError();
        }
        var t = Object(this);
        var len = t.length >>> 0;
        if (len === 0) {
            return -1;
        }
        var n = 0;
        if (arguments.length > 1) {
            n = Number(arguments[1]);
            if (n != n) { // shortcut for verifying if it's NaN
                n = 0;
            } else if (n != 0 && n != Infinity && n != -Infinity) {
                n = (n > 0 || -1) * Math.floor(Math.abs(n));
            }
        }
        if (n >= len) {
            return -1;
        }
        var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
        for (; k < len; k++) {
            if (k in t && t[k] === searchElement) {
                return k;
            }
        }
        return -1;
    }
}

//function showRestrictedModalX(_opts){
//
////    _opts = (typeof _opts != 'undefined') ? _opts : {} ;
//
//    var default_message = '' +
//        '<p>You are about to download restricted and highly sensitive data. Please remember that you agreed to abide by the ' +
//        '<a href="http://humanconnectome.org/data/data-use-terms/restricted-access.html" target="_blank">HCP Restricted ' +
//        'Data Terms of Use</a>. You must take appropriate precautions to secure this data and prevent ' +
//        'distribution to unauthorized parties.</p>';
//
//    var modal_message = _opts.message || default_message ;
//
//    var modal_content = '' +
//
//            '<p style="float:left;padding-top:20px;padding-bottom:20px;padding-right:10px;">' +
//            '   <img src="'+serverRoot+'/style/furniture/icon-lock-64px.png" alt="Lock">' +
//            '</p>' +
//            '<h3>Warning!</h3>' +
//            //'</br>' +
//            modal_message
//        ;
//
//    var restrictedModalObj = {
//        width: _opts.width || 450,
//        height: _opts.height || 250,
//        title: _opts.title || 'Restricted Data Warning',
//        content: modal_content ,
//        okLabel: _opts.okLabel || 'I understand. Please continue.',
//        okAction: _opts.okAction || xmodal.message('Error','Error: Action is undefined.'),
//        cancelLabel: 'Cancel'
//    };
//
//    xmodal.open(restrictedModalObj);
//}


XNAT.app.dashboard = new function () {
    var that = this;
    this.constants = {
        "GROUP_LIST_DEFAULT_OPTION": "<img src=\""+ serverRoot + "/images/tool-open-group.png\" /><label>Open Group</label>"
    };
    this.filterChange = new YAHOO.util.CustomEvent("filter-change", this);
    this.onLoadComplete = new YAHOO.util.CustomEvent("load-complete", this);
    this.onHistoricalFiltersLoad = new YAHOO.util.CustomEvent("historical-filters-loaded", this);
    this.onLogFilterUpdate = new YAHOO.util.CustomEvent("historical-filters-updated", this);
    this.filters = {};
    this.fields = {};
    this.all_cols = new Array();//?project="+current_project_name
    this.numActiveFilters = 0; //This is the number of active filters. This does not count the hidden project filter.
    
    this.buildURI=function(uri){
    	if(current_project_name==undefined || current_project_name==""){
    		return serverRoot+ uri;
    	}else{
		return serverRoot+ uri+"?project="+current_project_name;
    	}
    }
    this.listing_defs = new Array();
    for(i=0; i<categories.length; i++) {
        this.listing_defs.push({"URL": this.buildURI("/REST/services/search/ddict/"+categories[i][0]), "ID": i, "label": categories[i][0], "category": categories[i][0]});
    }

    this.filterHeaders = new Array();
    this.DTs = new Array();//an array to hold the datatables that have been loaded in the page
    this.currentGroup = null;
    this.currentProjectKey = null;

    this.getSubjectURL=function(){
        return serverRoot+"/REST/services/search/filter/" + this.getSubjectViewId();
    }

    //returns the view id for the cached Subjects
    this.getSubjectViewId = function () {
        return this.getDTByLabel("Subject Information").dataTable.initResults.ResultSet.ID;
    };

    this.loadSubjectCSVNonRestricted=function(){
        var project = document.getElementById("dashboardProject").value;
        //window.location=this.getSubjectURL() +"?restricted=0&cachingHandler=filter&format=csv&removeDelimitersFromFieldValues=true"+this.buildFilters(this.getSubjectViewId());
        window.location=serverRoot+"/REST/search/dict/Subject Information/results?format=csv&removeDelimitersFromFieldValues=true&restricted=0&project="+project;
    }

    this.restrictedModal=function(){

        var that = this ;

        var modal_opts = {};
        modal_opts.okAction = function(){
            var subjectLabels = document.getElementById("subjectLabels").value;
            var project = document.getElementById("dashboardProject").value;
            // IE doesn't handle long URLs, so use the subject list from the http session instead of passing it in the request URL
            //window.location=serverRoot+"/REST/search/dict/Subject Information/results?format=csv&removeDelimitersFromFieldValues=true&subject="+subjectLabels+"&restricted=1,2&project="+project;
            window.location=serverRoot+"/REST/search/dict/Subject Information/results?format=csv&removeDelimitersFromFieldValues=true&restricted=1,2&project="+project;
        };

        showRestrictedModal(modal_opts);

    }

    //returns the view id for the cached Subjects
    this.getSessionsViewId = function () {
        return this.getDTByLabel("MR Sessions").dataTable.initResults.ResultSet.ID;
    };

    this.getDTByLabel = function (lbl) {
        for (var dTsC2 = 0; dTsC2 < XNAT.app.dashboard.DTs.length; dTsC2++) {
            var dt = XNAT.app.dashboard.DTs[dTsC2];
            if (dt.dataTable != null && dt.obj.label == lbl) {
                return dt;
            }
        }

        return null;
    }

    this.reset = function () {
        this.fields = {};
    }

    this.clean = function () {
        this.fields = {};
        this.filters = {};
    }

    this.indexOfFieldValue = function (_filter_id, _node) {
        for (var cfvI = 0; cfvI < this.fields[_filter_id].values.length; cfvI++) {
            if (this.fields[_filter_id].values[cfvI].value == _node.value) {
                return cfvI;
            }
        }

        return -1;
    }

    this.containsFilter = function (_filters, _value) {
        if (_filters == undefined) {
            return false;
        }
        if (_value == "") {
            return _filters.contains('NULL');
        } else {
            return _filters.contains(_value);
        }

    }

    this.renderFilters = function () {
        YUIDOM.get('filter_display').innerHTML = this.buildFilterPseudoQuery();
    };

    this.buildFilterPseudoQuery = function () {
        var temp = "";
        for (var key in this.filters) {
            var this_filters = this.filters[key];
            if (this_filters.length > 0) {
                var values;
                values = "";
                for (var v23 = 0; v23 < this_filters.length; v23++) {
                    if (values.length > 1)values += ",";
                    values += this_filters[v23];
                }

                if (temp.length > 1) {
                    temp += " AND ";
                }
                temp += this.filterHeaders[key] + " = (" + values + ")";
            }
        }
        return temp;
    };

    this.noCache = function () {
        return "&no_cache=" + (new Date()).getTime();
    };

    this.filtersOnly = function (view_id) {
        var temp="";
        for (var key in this.filters) {
            var this_filters = this.filters[key];
            if (this_filters.length > 0) {
                var values;
                values = "";
                for (var v23 = 0; v23 < this_filters.length; v23++) {
                    if (values.length > 0)values += ",";
                    values += this_filters[v23];
                }
                temp += "&" + this.translateFilterKey(key, view_id) + "=" + values;
            }
        }
        return temp;
    };

    this.buildFilters = function (view_id) {
        return this.noCache()+this.filtersOnly(view_id);
    };

    this.buildFilters = function (view_id) {
        return this.noCache()+this.filtersOnly(view_id);
    };
    
    this.filterCount=function(){
    	var fC=0;
    	for (var key in this.filters) {
            var this_filters = this.filters[key];
            if (this_filters.length > 0) {
                fC++;
            }
        }
    	return fC;
    }

    this.retrieveColByKey = function (key) {
        return XNAT.utils.find(this.all_cols, key, function (e1, e2) {
            return(e1.key == e2)
        });
    }

    //translate from globally defined column key to the specific one for a particular table
    this.translateFilterKey = function (key, view_id) {
        var col = this.retrieveColByKey(key);
        if (col == null) {
            return key;
        } else {
            try {
               return XNAT.utils.find(col.table_keys, view_id,function (e1, e2) {
                   return(e1.table == e2)
               }).key;
            } catch (e) {
               return key;
            }
        }
    }

    this.buildFilterMap = function () {
        var map = {};
        map["no_cache"] = (new Date()).getTime();
        for (var key in this.filters) {
            var this_filters = this.filters[key];
            if (this_filters.length > 0) {
                var values;
                values = "";
                for (var v23 = 0; v23 < this_filters.length; v23++) {
                    if (values.length > 0)values += ",";
                    values += this_filters[v23];
                }
                map[key] = values;
            }
        }
        return map;
    }

    this.resetFilter = function (field) {
        this.filters[field] = new Array();
        this.filterChange.fire();
    };

    this.addFilter = function (field, value, isLastInGroup) {
        this.markCurrentFilterAsModified();

        if (this.filters[field] == undefined) {
            this.filters[field] = new Array();
        }

        if (!this.filters[field].contains(value)) {
            this.filters[field].push(value);
        }

        if((typeof isLastInGroup == 'undefined') || (isLastInGroup)){
			$("#downloadButton").removeClass("hidden"); // reveals hidden Download button after all groups have loaded. 
			$("#downloadPlaceholder").addClass("hidden"); // hides placeholder
            isLoadingGroup=0;
            this.filterChange.fire(field);
        }
    };

    //Remove all stored filters, but keep the project filter
    this.resetStoredFilters = function () {
        var projectFilter = XNAT.app.dashboard.filters[this.currentProjectKey];
        XNAT.app.dashboard.filters = {};
        if (projectFilter != null && projectFilter != undefined) {
            XNAT.app.dashboard.filters[this.currentProjectKey] = projectFilter;
        }
    }

    this.showAllSubjects = function () {
        //clear existing filters
        XNAT.app.dashboard.resetStoredFilters();
        for (var fcC = window.filterCount; fcC >= 0; fcC--) {
            deleteFilter(fcC);
        }
        this.numActiveFilters = 0;
        this.setCurrentGroup(null);
        $('#group_selector').val(this.constants.GROUP_LIST_DEFAULT_OPTION);
    };

    this.removeFilter = function (field, value) {
        if (this.filters[field] != undefined && this.filters[field] != null && this.filters[field].indexOf(value) > -1){
            //If the filter is still applied (it is not currently being edited), you should decrement the number of active filters.
            XNAT.app.dashboard.numActiveFilters--;
        }
        this.markCurrentFilterAsModified();
        if (this.filters[field] == undefined) {
            this.filters[field] = new Array();
        }

        this.removeWinFilter(this.filters[field], value);
        this.filterChange.fire(field);
    };

    this.markCurrentFilterAsModified = function () {
        this.setCurrSubjDisplay();
        if (this.isCurrentFilterAGroup() && $('.currSubjDisplay').html().indexOf("Modified") < 0) {
            //If you are working with a group and modified it, add (Modified) the first time it gets modified.
            $('.currSubjDisplay').html($('.currSubjDisplay').html().replace("Group:", "Group(Modified):"));
        }
    }

    this.isCurrentFilterAGroup = function () {
        return this.currentGroup !== null;
    }

    this.setCurrentGroup = function (filterLabel) {
        if (filterLabel) {
            this.currentGroup = this.getFilterByLabel(filterLabel);
        }
        else {
            this.currentGroup = null;
        }

        this.setCurrSubjDisplay();
    }

    this.setCurrSubjDisplay = function () {
        var currSubjDisplayText;
        if (this.isCurrentFilterAGroup()) {
            currSubjDisplayText = 'Group: <span class="currGroupName" title="'+this.currentGroup.desc+'">' + this.currentGroup.desc + '</span>';
        }
        else if (0 === this.numActiveFilters) {
            currSubjDisplayText = "All Subjects";
        }
        else {
            currSubjDisplayText = "Filtered Subjects";
        }

        var html = currSubjDisplayText;
		
		var addlHtml = ''; 

        if (this.isCurrentFilterAGroup() && !this.currentGroup.isSiteFilter && this.currentGroup.desc!="My Latest Filter") {
            addlHtml += ' <button class="btn btn-xs btn-inline" onclick="XNAT.app.filterUI.deleteGroup();">Delete this Group</button>';
        }

        if ("All Subjects" !== currSubjDisplayText) {
            addlHtml += ' <button class="btn btn-xs btn-inline" onclick="XNAT.app.dashboard.showAllSubjects();">Show All Subjects</button>';
        }

        $('.currSubjDisplay').html(html); 
		if (addlHtml) { $('#group-actions').html(addlHtml).removeClass("hidden"); } 
		else { $('#group-actions').html('').addClass("hidden"); }
    };

    //for some reason IE was crashing on the indexOf call here.  So, i manually walked the array.  There must be a better way to do this.
    this.removeWinFilter = function (filter_array, remove_value) {
        for (var rwfI = 0; rwfI < filter_array.length; rwfI++) {
            if (filter_array[rwfI] == remove_value) {
                filter_array.splice(rwfI, 1);
                break;
            }
        }
    };

    //start fresh
    this.resetAll = function () {
        this.reset();
        this.renderAll();
    }

    this.onRenderRequest = new YAHOO.util.CustomEvent("render-request", this);
    this.onRenderRequest.subscribe(this.renderFilters, this, true);

    //reload all filters
    this.renderAll = function () {
        this.onRenderRequest.fire();
    };

    //called within the scope of the dt DatatableStoredSearch that was loaded
    this.handleDTLoad = function (event) {
        this.dt_loaded = true;

        var _done_loading = true;//have all of the datatables been loaded.
        for (var dTsC3 = 0; dTsC3 < XNAT.app.dashboard.DTs.length; dTsC3++) {
            var that_dt = XNAT.app.dashboard.DTs[dTsC3];
            if (that_dt.dt_loaded == undefined) {
                _done_loading = false;
            }
        }

        if (_done_loading) {
            XNAT.app.dashboard.onLoadComplete.fire();
            if(this.dataTable.hasBeenReset==undefined){
                this.dataTable.hasBeenReset=true;
                this.dataTable.resetSize();
            }
        }

    }

    //re-render filters when filters are changed
    this.filterChange.subscribe(this.renderAll, this, true);

    this.findMatchByNameAndField = function (columns, newField) {
        for (var cI23 = 0; cI23 < columns.length; cI23++) {
            if (columns[cI23].element_name == newField.element_name && columns[cI23].field_id == newField.id) {
                return columns[cI23];
            }
        }
        return null;
    }

    //navigates tables to identify fields
    //called multiple times for different tables.
    //creates a merged list of the fields that are common between the two tables
    //matching columns are equal when the element_name and field_id are equal (NOTE: the key may be different in different tables
    this.buildColumns = function () {
        try {
            for (var dtCI2 = 0; dtCI2 < this.DTs.length; dtCI2++) {//we now have a collection of data tables
                var dt = this.DTs[dtCI2];
                if (dt.dataTable != undefined) {//some tables may not be initialized yet
                    var cs = dt.dataTable.getColumns();
                    if (cs != undefined) {//some tables may not be initialized yet
                        for (var dtCI1 = 0; dtCI1 < cs.length; dtCI1++) {
                            var column = cs[dtCI1];

                            if (column != null) {
                                if (column.element_name != undefined) {
                                    if (window.available_elements.getByName(column.element_name)) {
                                        var element_display = window.available_elements.getByName(column.element_name).plural;
                                    }
                                    //Merge with prexisting... if its there
                                    var match = this.findMatchByNameAndField(this.all_cols, column);
                                    if (match == null) {
                                        this.all_cols.push(
                                            {key: column.key + "_" + dt.dataTable.initResults.ResultSet.ID,
                                                element_header: element_display,
                                                element_name: column.element_name,
                                                field_id: column.id,
                                                header: column.header,
                                                type: column.type,
                                                table_keys: new Array({key: column.key, table: dt.dataTable.initResults.ResultSet.ID})});
                                    } else {
                                        //see if this has already been added by comparing the generated key
                                        if (XNAT.utils.find(match.table_keys, column.key + "_" + dt.dataTable.initResults.ResultSet.ID, function (e1, e2) {
                                            return(e1.key == e2);
                                        }) == null) {
                                            match.table_keys.push({key: column.key, table: dt.dataTable.initResults.ResultSet.ID});//add a new column key
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        } catch (e) {
            showMessage("page_body", "Exception", e.message);
        }

	// MODIFIED:  MRH 2016/12/29.  This was previously doing a filtered return, returning only columns that were shared between
	// the tables.  I'm not sure why that was being done, since all the datasets contained the full set of columns...... 
	// I've removed the filtering and am just returning all columns now.
        return this.all_cols
    }

    //build generic filter string where the keys are the filter keys are generic IDs (rather than table keys)
    this.buildGenericFilterString = function () {
        var temp = "";
        for (var key in this.filters) {
            if(key!=this.currentProjectKey){//Don't save the hidden project filter.
                var this_filters = this.filters[key];
                if (this_filters.length > 0) {
                    var values;
                    values = "";
                    for (var v23 = 0; v23 < this_filters.length; v23++) {
                        if (values.length > 0)values += ",";
                        values += this_filters[v23];
                    }
                    var col = this.retrieveColByKey(key);
                    temp += "&" + col.element_name + "." + col.field_id + "=" + values;
                }
            }
        }
        return temp;
    }

    //Calculate the highest tier of any currently active filters
    this.getMaxTierCurrFilters = function () {
        var currMaxTier = 0;
        for (var key in this.filters) {
            var this_filters = this.filters[key];
            if (this_filters.length > 0) {
                if(key!="" && key!=this.currentProjectKey){//Don't save the hidden project filter.
                    var entry = XNAT.utils.find(dataDictionaryJSON, key, function (dictEntry, k) {
                        return (dictEntry.key == k);
                    });
                    if(entry.tier>currMaxTier){
                        currMaxTier = entry.tier;
                    }
                }
            }
       }
       return currMaxTier;
    }

    //take the string generated by the generic filter method above, and translate it into the appropriate filters for this context (fix the keys)
    this.translateFiltersFromGenericString = function (generic_filters) {
        if (generic_filters.startsWith("&")) {
            generic_filters = generic_filters.substring(1);
        }
        //ex. xnat:subjectData.AGE=>20&xnat:subjectData.GENDER_TEXT==M
        var gFilters = generic_filters.split("&");
        var parsedFilters = {};
        for (var gFC = 0; gFC < gFilters.length; gFC++) {
            var fName = gFilters[gFC].substring(0, gFilters[gFC].indexOf("="));
            var fValue = gFilters[gFC].substring(gFilters[gFC].indexOf("=") + 1);

            var fElement = fName.substring(0, fName.indexOf("."));
            var fField = fName.substring(fName.indexOf(".") + 1);

            var match = this.findMatchByNameAndField(this.all_cols, {'element_name': fElement, 'id': fField});
            if (match == null) {
                if (gFilters[gFC] != "") {
                    showMessage("page_body", "Notification", "Unable to restore field " + fElement + " " + fField);
                }
            } else {
                parsedFilters[match.key] = fValue;
            }
        }
        return parsedFilters;
    }

    this.databaseCsvs = [];

    this.previousFiltersLog = [];

    // this method will return the previous filters that were logged in the user search log
    this.loadFiltersLog = function () {
        this.loadUserFilters = {
            success: function (o) {
                this.previousFiltersLog = YAHOO.lang.JSON.parse(o.responseText);
                YAHOO.util.Connect.asyncRequest('GET', serverRoot + '/REST/config/dashboard/subject/filters.json?format=json&nocache=' + ((new Date()).getTime()), this.loadSiteFilters, null, this);
            },
            failure: function (e) {
            	if(e.status === 404) {
            		// there may not be any user-defined filters, that's OK, just show the sitewide filters
            		YAHOO.util.Connect.asyncRequest('GET', serverRoot + '/REST/config/dashboard/subject/filters.json?format=json&nocache=' + ((new Date()).getTime()), this.loadSiteFilters, null, this);
            	} else {
            		alert("Something went wrong loading the site filters");
            	}
            },
            scope: this
        };

        this.loadSiteFilters = {
            success: function (o) {
                var response = YAHOO.lang.JSON.parse(o.responseText);
                var siteFiltersLog = eval(response.ResultSet.Result[0].contents);
                this.previousFiltersLog = siteFiltersLog.concat(this.previousFiltersLog);
                this.onHistoricalFiltersLoad.fire(this.previousFiltersLog);
            },
            failure: function (e) {
            	if(e.status === 404) {
            		// there may not be any site-wide filters, that's OK, just show the user-defined filterse
            		this.onHistoricalFiltersLoad.fire(this.previousFiltersLog);
            	} else {
            		alert("Something went wrong loading the site filters");
            	}
            },
            scope: this
        };

        YAHOO.util.Connect.asyncRequest('GET', serverRoot + '/REST/user/cache/resources/searchlog/files/filters.json?nocache=' + ((new Date()).getTime()), this.loadUserFilters, null, this);
    }

    //build generic filter string and log it on the server
    this.logFilter = function (filterLabel) {
        var new_filters = this.buildGenericFilterString();

        var filter;
        if (this.isCurrentFilterAGroup() && filterLabel === this.currentGroup.desc) {
            // modifying current group
            filter = this.getFilterByLabel(filterLabel);
        }
        else if (XNAT.app.HcpUtil.isUserMostRecentDownloadGroupName(filterLabel)) {
            filter = this.getFilterByLabel(filterLabel);
            if (!filter) {
                filter = {};
                this.previousFiltersLog.unshift(filter);
            }
        }
        else {
            // adding new group
            filter = {};
            this.previousFiltersLog.push(filter);
        }

        filter.time = (new Date()).getTime();
        filter.filter = new_filters;
        filter.desc = filterLabel;
        filter.tier = this.getMaxTierCurrFilters();
        filter.project = current_project_name;
        this.updateAllFilters();
    }

    this.getFilterByLabel = function (filterLabel) {
        for (var i = 0; i < this.previousFiltersLog.length; ++i) {
            if (this.previousFiltersLog[i].desc === filterLabel) {
                return this.previousFiltersLog[i];
            }
        }
        return null;
    }

    this.updateAllFilters = function () {
        var filterLogCallback = {
            success: function (e) {
                this.onLogFilterUpdate.fire();
            },
            failure: function (e) {
                showMessage("page_body", "Notification", "Failed to log filter history.");
            },
            scope: this
        };

        var redactSiteFiltersBeforeSavingUserFilters = function () {
            var userFiltersLog = [];
            for (var i = 0; i < that.previousFiltersLog.length; ++i) {
                if (!that.previousFiltersLog[i].isSiteFilter) {
                    userFiltersLog.push(that.previousFiltersLog[i]);
                }
            }
            return userFiltersLog;
        };
        var userFiltersLog = redactSiteFiltersBeforeSavingUserFilters();

        YAHOO.util.Connect.asyncRequest('PUT', serverRoot + '/REST/user/cache/resources/searchlog/files/filters.json?inbody=true&XNAT_CSRF=' + window.csrfToken, filterLogCallback, YAHOO.lang.JSON.stringify(userFiltersLog), this);

        this.onHistoricalFiltersLoad.fire(this.previousFiltersLog);
    }

    this.deleteFilter = function () {
        for (var i = 0; i < this.previousFiltersLog.length; ++i) {
            if (this.isCurrentFilterAGroup() && this.previousFiltersLog[i].desc === this.currentGroup.desc) {
                this.previousFiltersLog.splice(i, 1);
            }
        }
        this.updateAllFilters();
    }
    
    this.initDataDictionaryLauncher = function() {
    	$("#search_tabs").on("click", "a.ddictLauncher", function(e) {
    		e.preventDefault(true);
    		
    		var dataTableId = this.id.replace("_ddict", "").replace("_dt", "");
    		var category = that.getCategoryForDataTable(dataTableId);
    		
	        var callback = {
	            success: function (o) {
		            xmodal.open({
		                kind:     'large',  // size - 'fixed','custom','large','med','small',
		                //width:     550,  // box width when used with 'fixed' size, horizontal margin when used with 'custom' size
		                //height:    350,  // box height when used with 'fixed' size, vertical margin when used with 'custom' size
		                scroll:   'yes', // does the content need to scroll? (yes/no)
		                box:      'info',  // class name of this modal box
		                title:    'Data Dictionary', // text for modal title bar - if empty will be "Alert" - use space for blank title
		                content:   o.responseText, // body content of modal window
		                okLabel:       'Close' // text for submit button - if blank uses "OK"
		            });
	            },
	            failure: function (e) {
	            	showMessage("page_body", "Error", e.status + ": " + e.responseText);
	            },
	            scope: this
	        };
	        YAHOO.util.Connect.asyncRequest('GET', serverRoot + '/REST/services/ddict/attributes/' + encodeURIComponent(category) + '?format=html&XNAT_CSRF=' + window.csrfToken, callback, null, this);    
    		
    		return false;
    	});
    }
    
    this.getCategoryForDataTable = function(dataTableId) {
    	for(var i = 0; i < this.listing_defs.length; ++i) {
    		if(this.listing_defs[i].ID === dataTableId) {
    			return this.listing_defs[i].category;
    		}
    	}
    	return null;
    }
};

//BEGIN modifications to add tabbed interface.

//render tab manager
var tab_module = new YAHOO.widget.Module("search_tab_module", {visible: true});

window.tab_manager = new TabManager();

window.tab_manager.setCsrfToken("$!XNAT_CSRF");

var currentTime = new Date();
var pageID = currentTime.getTime() + '_';

function TabManagerInit() {

    window.tab_manager.init(XNAT.app.dashboard.listing_defs, {showReload: false, showOptionsDropdown: false, showFilterDisplay: false, allowInTableMods: false});

    window.tab_manager.onDTcreate.subscribe(function (o, args, me) {
        var dt = args[0];
        XNAT.app.dashboard.DTs.push(dt);
        dt.onLoad.subscribe(function () {
            //when filters are changed, reload first page of data table
            XNAT.app.dashboard.filterChange.subscribe(dt.dataTable.resetSize, dt.dataTable, true);

            //next line caused the old filters dialog to be reset, when data tables where modified by other means... not sure if we still need it
            dt.onChange.subscribe(XNAT.app.dashboard.clean, XNAT.app.dashboard, true);
        });

        //create listener to handle DT loading
        dt.onComplete.subscribe(XNAT.app.dashboard.handleDTLoad, dt, true);
    });

    window.tab_manager.load({label: "Subject Information"});
    if(typeof connectomeType === "undefined"){
        window.tab_manager.load({label: "MR Sessions"});
    }

    XNAT.app.dashboard.initDataDictionaryLauncher();
}

YAHOO.util.Event.onDOMReady(TabManagerInit);


