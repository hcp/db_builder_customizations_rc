this.loadCsvByName=function(csvName,tier){
    $('#csv_selector').find('option').removeClass('selected').removeAttr("selected").prop('selected',false);
    xMenuCreate($('#csv_selector'));
    var project = document.getElementById("dashboardProject").value;
    if(tier>0){
        var that = this ;

        var modal_opts = {};
        modal_opts.okAction = function(){
            // IE doesn't handle long URLs, so use the subject list from the http session instead of passing it in the request URL
            //window.location=serverRoot+"/REST/search/dict/Subject Information/results?format=csv&removeDelimitersFromFieldValues=true&subject="+subjectLabels+"&restricted=1,2&project="+project;
            window.location=serverRoot+"/REST/search/dict/Subject Information/results?format=csv&csvName="+csvName+"&removeDelimitersFromFieldValues=true&restricted="+tier+"&project="+project;
        };

        showRestrictedModal(modal_opts);
    }
    else{
        window.location=serverRoot+"/REST/search/dict/Subject Information/results?format=csv&csvName="+csvName+"&removeDelimitersFromFieldValues=true&restricted=0&project="+project;
    }

}

//populate csvs in the hidden dropdown list
this.populateCsvs = function (csvs) {
    var cSelect = $('#csv_selector');
    cSelect.html('');//clear any previous csvs
    var addedBar = false;

    for (var pflC = 0; pflC < databaseCsvs.length; pflC++) {
        var logEntry = databaseCsvs[pflC];
        var tierOfOption = "";
        if(logEntry.tier!=undefined && logEntry.tier!=null){
            tierOfOption = "tier"+logEntry.tier;
        }
        var projectOfFilter = "";
        if(logEntry.projects!=undefined && logEntry.projects!=null && logEntry.projects.length>0 && logEntry.projects.contains(current_project_name)){
            projectOfFilter = current_project_name;
            //Display filter if it is not project specific or if you're on the project it applies to.
            if(tierOfOption=="tier1"){
                if(hasRestrictedAccess==true){
                    cSelect.append($('<option class="csv image '+tierOfOption+'" tier=1 data-img="'+serverRoot+'/style/furniture/icon-lock-tier1.png"></option>').prop("value", logEntry.name).prop("csvName", logEntry.name).text(logEntry.name.getExcerpt(60)));
                }
            }
            else if(tierOfOption=="tier2"){
                if(hasRestrictedAccess==true){
                    cSelect.append($('<option class="csv image '+tierOfOption+'" tier=2 data-img="'+serverRoot+'/style/furniture/icon-lock-tier2.png"></option>').prop("value", logEntry.name).prop("csvName", logEntry.name).text(logEntry.name.getExcerpt(60)));
                }
            }
            else{
                cSelect.append($('<option class="csv image '+tierOfOption+'" tier=0></option>').prop("value", logEntry.name).prop("csvName", logEntry.name).text(logEntry.name.getExcerpt(60)));
            }
        }

    }
    xMenuCreate(cSelect);
}

this.showCsvs = function () {
    $('#csv_selector_div').show();
    $('#csv_selector').focus();
}

this.hideCsvs = function () {
    $('#csv_selector_div').hide();
}

//when the csv selector is modified, download that csv
$("#csv_selector").change(function () {
    $('#csv_selector_div').hide();
    $('#csv_selector').blur();
    var tier = $('#open_csv').find('select option:selected')[0].attributes.tier.value;
    var oCsv = $(this).val();

    loadCsvByName(oCsv,tier);
})

$("#csv_selector").blur(function () {
    $('#csv_selector_div').hide();
})

this.loadCsvs = function () {
    this.loadUserCsvs = {
        success: function (o) {
            this.databaseCsvs = YAHOO.lang.JSON.parse(o.responseText);
            populateCsvs(this.databaseCsvs);
        },
        failure: function (e) {
            alert("Something went wrong loading the site csvs");
        },
        scope: this
    };
    var maxTierUserCanAccess = 0;
    if(hasRestrictedAccess){
        maxTierUserCanAccess = 2;
    }
    YAHOO.util.Connect.asyncRequest('GET', serverRoot + '/REST/services/ddict/csvs/'+current_project_name+'/'+maxTierUserCanAccess, this.loadUserCsvs, null, this);
}