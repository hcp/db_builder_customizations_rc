
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// HCP SPECIFIC JavaScript FUNCTIONS FOR USE IN FORMS - UTILITY FUNCTIONS //
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

function addLoadEvent(func) {
	var oldonload = window.onload;
	if (typeof window.onload != 'function') {
		window.onload = func;
	} else {
		window.onload = function() {
			if (oldonload) {
				oldonload();
			}
			func();
		}
	}
}

function pad(num, size) {
	var s = num+"";
	while (s.length < size) s = "0" + s;
	return s;
}

function getTimeString(sel) {
	var childNodes = sel.parentNode.getElementsByTagName("SELECT");
	var img_div = sel.parentNode.getElementsByTagName("DIV")[0];
	if(img_div!=undefined)img_div.innerHTML="";
	var hours = 0;
	var minutes = 0;
	var seconds = 0;
	var name;

	for(var childNodeCount=0;childNodeCount<childNodes.length;childNodeCount++) {
		thisName=childNodes[childNodeCount].name;
		if (thisName.indexOf(".hours")>0) {
			if (name==undefined) {
				name = thisName.substring(0,thisName.length-".hours".length);
			}
			if(childNodes[childNodeCount].selectedIndex==0) {
				return;
		 	} else {
		 		hours= childNodes[childNodeCount].options[childNodes[childNodeCount].selectedIndex].value;
			}
		} else if (thisName.indexOf(".minutes")>0) {
			if (name==undefined) {
				name = thisName.substring(0,thisName.length-".minutes".length);
			}
			if(childNodes[childNodeCount].selectedIndex==0) {
				return;
			} else {
				minutes = childNodes[childNodeCount].options[childNodes[childNodeCount].selectedIndex].value;
			}
		} else if (thisName.indexOf(".seconds")>0) {
			if (name==undefined) {
				name = thisName.substring(0,thisName.length-".seconds".length);
			}
			if(childNodes[childNodeCount].selectedIndex==0) {
		  		return;
		 	} else {
		  		seconds= childNodes[childNodeCount].options[childNodes[childNodeCount].selectedIndex].value;
		 	}
		}
	}
	// Set value for hidden field //
	var timeStr = pad(hours,2)+":" + pad(minutes,2) + ":" + pad(seconds,2);
	return timeStr;
}

function getTimeFromTimeBox(sel){
	return getTimeFromString(getTimeString(sel));
}

function validateTime(sel,name){
	var childNodes = sel.parentNode.getElementsByTagName("SELECT");
	var img_div = sel.parentNode.getElementsByTagName("DIV")[0];
	if(img_div!=undefined)img_div.innerHTML="";
	var hours = 0;
	var minutes = 0;
	var seconds = 0;
	var name;

	for(var childNodeCount=0;childNodeCount<childNodes.length;childNodeCount++) {
		thisName=childNodes[childNodeCount].name;
		if (thisName.indexOf(".hours")>0) {
			if (name==undefined) {
				name = thisName.substring(0,thisName.length-".hours".length);
			}
			if(childNodes[childNodeCount].selectedIndex==0) {
				return;
		 	} else {
		 		hours= childNodes[childNodeCount].options[childNodes[childNodeCount].selectedIndex].value;
			}
		} else if (thisName.indexOf(".minutes")>0) {
			if (name==undefined) {
				name = thisName.substring(0,thisName.length-".minutes".length);
			}
			if(childNodes[childNodeCount].selectedIndex==0) {
				return;
			} else {
				minutes = childNodes[childNodeCount].options[childNodes[childNodeCount].selectedIndex].value;
			}
		} else if (thisName.indexOf(".seconds")>0) {
			if (name==undefined) {
				name = thisName.substring(0,thisName.length-".seconds".length);
			}
			if(childNodes[childNodeCount].selectedIndex==0) {
		  		return;
		 	} else {
		  		seconds= childNodes[childNodeCount].options[childNodes[childNodeCount].selectedIndex].value;
		 	}
		}
	}
	// Set value for hidden field //
	var timeStr = pad(hours,2)+":" + pad(minutes,2) + ":" + pad(seconds,2);
	if ( name != undefined ) {
		var hF = document.getElementById(name);
		hF.value = timeStr;
	}
	if(!isValidTime(timeStr)) {
		for(var childNodeCount=0;childNodeCount<childNodes.length;childNodeCount++) {
			childNodes[childNodeCount].selectedIndex=0;
		 	if (childNodes[childNodeCount].name.indexOf(".hours")>0) {
		  		childNodes[childNodeCount].focus();
		 	}
	  	}
	  	alert("Please enter a valid time value.");
	} else {
		if(img_div!=undefined)img_div.innerHTML="<img src=\"" + serverRoot+ "/images/checkmarkGreen.gif\"/>";
	}
}

function isValidTime(timeStr) {
	if (timeStr==undefined) {
		return undefined;
	}
	var tA = timeStr.split(":");
	// parseInt treats leading zeros as base-8
	var h = parseInt(stripLeadingZeros(tA[0]));
	var m = parseInt(stripLeadingZeros(A[1]));
	var s = parseInt(stripLeadingZeros(A[2]));
	if ( h>=0 && h<=23 && m>=0 && m<=59 && s>=0 && s<=59 ) {
		return true;
	}
	return false;
}

function getTimeFromString(timeStr) {
	if (timeStr==undefined) {
		return undefined;
	}
	var tA = timeStr.split(":");
	// parseInt treats leading zeros as base-8
	var h = parseInt(stripLeadingZeros(tA[0]));
	var m = parseInt(stripLeadingZeros(tA[1]));
	var s = parseInt(stripLeadingZeros(tA[2]));
	return new Date(1970,0,1,h,m,s,0);
}

function stripLeadingZeros(str) {
   while(str.length>1 && str.indexOf(0)=="0") {
      str=str.substring(1);
   }
   return str;
}

function getRadioValue(rObj) {
	if(!rObj) {
		return "";
	}
	var rLen = rObj.length;
	if(rLen == undefined) {
		if(rObj.checked) {
			return rObj.value;
		} else {
			return "";
		}
	}
	for(var i = 0; i < rLen; i++) {
		if(rObj[i].checked) {
			return rObj[i].value;
		}
	}
	return "";
}

function getCheckboxValue(rObj) {
	return getRadioValue(rObj);
}

function getCheckboxValue01(rObj) {
	cbVal = getRadioValue(rObj);
	if (cbVal.trim().length == 0) {
		return 0;
	} else {
		return 1;
	}
}

function setValueByName(varName,varValue) {
	var varArray = document.getElementsByName(varName);
	for (var i=0;i<varArray.length;i++) {
		varArray[i].value = varValue;
	}
}

function setInnerHtmlByName(varName,varValue) {
	var varArray = getDocElementsByName(varName);
	for (var i=0;i<varArray.length;i++) {
		varArray[i].innerHTML = varValue;
	}
}

function getDocElementsByName(varName) {
	var varArray = document.getElementsByName(varName);
	// IE Workaround
	if (varArray.length==0) {
		varArray = [];
		var tagArray = document.getElementsByTagName('span');
		for (var i=0;i<tagArray.length;i++) {
			if (tagArray[i].getAttribute('name')==varName) {
				varArray.push(tagArray[i]);
			}
		}
	}
	return varArray;
}

function isValidDateValue(d) {
  if ( Object.prototype.toString.call(d) !== "[object Date]" )
    return false;
  return !isNaN(d.getTime());
}


function getDate(sel){

	var childNodes = sel.parentNode.getElementsByTagName("SELECT");
	var month =0;
	var day = 0;
	var year=0;
	for(var childNodeCount=0;childNodeCount<childNodes.length;childNodeCount++) {
		if (childNodes[childNodeCount].name.indexOf(".month")>0) {
			if (childNodes[childNodeCount].selectedIndex==0) {
	     			return;
			} else {
				try {
					month= childNodes[childNodeCount].options[childNodes[childNodeCount].selectedIndex].value;
					monthInt = parseInt(month);
					month = monthInt;
				} catch (err) { }
	   		}
		} else if (childNodes[childNodeCount].name.indexOf(".date")>0) {
			if(childNodes[childNodeCount].selectedIndex==0) {
				return;
	    		} else {
				try {
	     				day = childNodes[childNodeCount].options[childNodes[childNodeCount].selectedIndex].value;
				} catch (err) { }
	    		}
		} else if (childNodes[childNodeCount].name.indexOf(".year")>0) {
			if(childNodes[childNodeCount].selectedIndex==0) {
				return;
	    		} else {
				try {
	     				year = childNodes[childNodeCount].options[childNodes[childNodeCount].selectedIndex].value;
				} catch (err) { }
	    		}
	   	}
	}
	return new Date(year,month,day,0,0,0,0);

}

function getDateFromStr(dateStr, format) {

   var returnVal;
   if (format == null) { format = "MDY"; }
   format = format.toUpperCase();
   if (format.length != 3) { format = "MDY"; }
   if ( (format.indexOf("M") == -1) || (format.indexOf("D") == -1) || (format.indexOf("Y") == -1) ) { format = "MDY"; }
   if (format.substring(0, 1) == "Y") { // If the year is first
      var reg1 = /^\d{2}(\-|\/|\.)\d{1,2}\1\d{1,2}$/;
      var reg2 = /^\d{4}(\-|\/|\.)\d{1,2}\1\d{1,2}$/;
   } else if (format.substring(1, 2) == "Y") { // If the year is second
      var reg1 = /^\d{1,2}(\-|\/|\.)\d{2}\1\d{1,2}$/;
      var reg2 = /^\d{1,2}(\-|\/|\.)\d{4}\1\d{1,2}$/;
   } else { // The year must be third
      var reg1 = /^\d{1,2}(\-|\/|\.)\d{1,2}\1\d{2}$/;
      var reg2 = /^\d{1,2}(\-|\/|\.)\d{1,2}\1\d{4}$/;
   }
   // If it doesn't conform to the right format (with either a 2 digit year or 4 digit year), fail
   if ( (reg1.test(dateStr) == false) && (reg2.test(dateStr) == false) ) { return false; }
   var parts = dateStr.split(RegExp.$1); // Split into 3 parts based on what the divider was
   // Check to see if the 3 parts end up making a valid date
   if (format.substring(0, 1) == "M") { var mm = parts[0]; } else if (format.substring(1, 2) == "M") { var mm = parts[1]; } else { var mm = parts[2]; }
   if (format.substring(0, 1) == "D") { var dd = parts[0]; } else if (format.substring(1, 2) == "D") { var dd = parts[1]; } else { var dd = parts[2]; }
   if (format.substring(0, 1) == "Y") { var yy = parts[0]; } else if (format.substring(1, 2) == "Y") { var yy = parts[1]; } else { var yy = parts[2]; }
   if (parseFloat(yy) <= 50) { yy = (parseFloat(yy) + 2000).toString(); }
   if (parseFloat(yy) <= 99) { yy = (parseFloat(yy) + 1900).toString(); }
   var dt = new Date(parseFloat(yy), parseFloat(mm)-1, parseFloat(dd), 0, 0, 0, 0);
   if (parseFloat(dd) != dt.getDate()) { return undefined; }
   if (parseFloat(mm)-1 != dt.getMonth()) { return undefined; }
   return dt;

}

function getDateTime(sel){

	var childNodes = sel.parentNode.getElementsByTagName("SELECT");
	var month =0;
	var day = 0;
	var year=0;
	var hours=0;
	var minutes=0;
	var seconds=0;
	for(var childNodeCount=0;childNodeCount<childNodes.length;childNodeCount++) {
		if (childNodes[childNodeCount].name.indexOf(".month")>0) {
			if (childNodes[childNodeCount].selectedIndex==0) {
	     			return;
			} else {
				try {
					month= childNodes[childNodeCount].options[childNodes[childNodeCount].selectedIndex].value;
					// Adding 1 here to keep code same as getDate() function above that returns date from string
					// (may want to change that), subtracted from new Date call
					monthInt = parseInt(month);
					month = monthInt;
				} catch (err) { }
	   		}
		} else if (childNodes[childNodeCount].name.indexOf(".date")>0) {
			if(childNodes[childNodeCount].selectedIndex==0) {
				return;
	    		} else {
				try {
	     				day = childNodes[childNodeCount].options[childNodes[childNodeCount].selectedIndex].value;
				} catch (err) { }
	    		}
		} else if (childNodes[childNodeCount].name.indexOf(".year")>0) {
			if(childNodes[childNodeCount].selectedIndex==0) {
				return;
	    		} else {
				try {
	     				year = childNodes[childNodeCount].options[childNodes[childNodeCount].selectedIndex].value;
				} catch (err) { }
	    		}
		} else if (childNodes[childNodeCount].name.indexOf(".hours")>0) {
			if(childNodes[childNodeCount].selectedIndex==0) {
				return;
	    		} else {
				try {
	     				hours = childNodes[childNodeCount].options[childNodes[childNodeCount].selectedIndex].value;
				} catch (err) { }
	    		}
		} else if (childNodes[childNodeCount].name.indexOf(".minutes")>0) {
			if(childNodes[childNodeCount].selectedIndex==0) {
				return;
	    		} else {
				try {
	     				minutes = childNodes[childNodeCount].options[childNodes[childNodeCount].selectedIndex].value;
				} catch (err) { }
	    		}
		} else if (childNodes[childNodeCount].name.indexOf(".seconds")>0) {
			if(childNodes[childNodeCount].selectedIndex==0) {
				return;
	    		} else {
				try {
	     				seconds = childNodes[childNodeCount].options[childNodes[childNodeCount].selectedIndex].value;
				} catch (err) { }
	    		}
	   	}
	}
	
	return new Date(year,month,day,hours,minutes,seconds,0);

}

String.prototype.trim = function() { 
	return this.replace(/^\s+/, '').replace(/\s+$/, '');
};


//////////////////////////////////
// NUMERIC CONVERSION FUNCTIONS //
//////////////////////////////////
function toInteger(val) {
	if (isInteger(val)) {
		return parseInt(val);
	}
	return Number.NaN;
}
function toFloat(val) {
	if (isFloat(val)) {
		return parseFloat(val);
	}
	return Number.NaN;
}
function toNumber(val) {
	return toFloat(val);
}
function isInteger(val) {
	if (isNaN(parseInt(val)) || parseInt(val)!=Number(val)) { 
		return false;
	}
	return true;
}
function isFloat(val) {
	if (isNaN(parseFloat(val)) || parseFloat(val)!=Number(val)) { 
		return false;
	}
	return true;
}



///////////////////////////////////////////////////
// Functions to Handle Additional Visits and ASQ //
///////////////////////////////////////////////////

/***** NEEDS REFACTORED *****/

function hideOtherVisits() {
	// Checks for and hides Other Visit dates that are not currently set
/*
	var date = [new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/OtherVisitDate1.month")[0])),
				new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/OtherVisitDate2.month")[0])),
				new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/OtherVisitDate3.month")[0]))];

	for(var i = 0; i <= date.length; i++) {
		var curVisit = "OtherVisit" + (i+1);
		
		if (date[i] == "Invalid Date") {
    		document.getElementById(curVisit).style.display="none";
    	}
	}
*/
	var date1 = new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/OtherVisitDate1.month")[0]));
  	var date2 = new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/OtherVisitDate2.month")[0]));
    var date3 = new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/OtherVisitDate3.month")[0]));

//    var inDate = new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/date.month")[0]));

//    if (inDate == "Invalid Date")
//    	document.getElementById('addvisit').disabled = "disabled";
    
    if (date1 == "Invalid Date")
    	document.getElementById('OtherVisit1').style.display="none";
    
    if (date2 == "Invalid Date")
    	document.getElementById('OtherVisit2').style.display="none";

    if (date3 == "Invalid Date")
    	document.getElementById('OtherVisit3').style.display="none";
    else { // if date3 is set
    	try {  // passes when the button doesn't exist on the report
    		document.getElementById('addvisit').disabled = "disabled";
    		document.getElementById('addvisit').value = "Max Visits Reached";
    	} catch(err) {;}
    }
}


function hideOtherASQ() {

//	var valuesPresent = getDocElementsByName("hcpvisit:HCPVisitData/HCPASQ2/TobaccoUseToday")[0].value;
	var date2 = new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/ASQDate2.month")[0]));
	var date3 = new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/ASQDate3.month")[0]));
	var date4 = new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/ASQDate4.month")[0]));

	if (date2 == "Invalid Date")
    	document.getElementById('ASQ2Form').style.display="none";
    
    if (date3 == "Invalid Date")
    	document.getElementById('ASQ3Form').style.display="none";

    if (date4 == "Invalid Date")
    	document.getElementById('ASQ4Form').style.display="none";
    else { // if date4 is set
    	try {
    		document.getElementById('addasq').disabled = "disabled";
    		document.getElementById('addasq').value = "Max ASQs Reached";
    	} catch(err) {;}
    }
/*
	if (valuesPresent) {
		document.getElementById('ASQ2Form').style.display="block";
		document.getElementById('addasq').disabled = "disabled";
	}
	else {
		document.getElementById('ASQ2Form').style.display="none";
	}
*/
}



function addVisit() {

	var date1 = new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/OtherVisitDate1.month")[0]));
	var date2 = new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/OtherVisitDate2.month")[0]));
	var date3 = new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/OtherVisitDate3.month")[0]));
	var inDate = new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/date.month")[0]));
	//var yesDate = new Date

	if (inDate != "Invalid Date") {
		if (date1 == "Invalid Date") {
	    	document.getElementById('OtherVisit1').style.display="block";
	    }
	    else if (date2 == "Invalid Date") {
	    	document.getElementById('OtherVisit2').style.display="block";
	    }
	    else if (date3 == "Invalid Date") {
	    	document.getElementById('OtherVisit3').style.display="block";
	    	try {
	    		document.getElementById('addvisit').disabled = "disabled";
	    		document.getElementById('addvisit').value = "Max Visits Reached";
	    	} catch(err) {;}
	    }
	}

	//document.getElementById('addvisit').disabled = "disabled";
}


function addASQ() {
//	document.getElementById('ASQ2Form').style.display="block";
//	document.getElementById('addasq').disabled = "enabled";

	var date2 = new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/ASQDate2.month")[0]));
	var date3 = new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/ASQDate3.month")[0]));
	var date4 = new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/ASQDate4.month")[0]));
//	var inDate = new Date(getDate(getDocElementsByName("hcpvisit:HCPVisitData/date.month")[0]));

	if (date2 == "Invalid Date") {
    	document.getElementById('ASQ2Form').style.display="block";
    }
    else if (date3 == "Invalid Date") {
    	document.getElementById('ASQ3Form').style.display="block";
    }
    else if (date4 == "Invalid Date") {
    	try {
    		document.getElementById('ASQ4Form').style.display="block";
    		document.getElementById('addasq').disabled = "disabled";
    		document.getElementById('addasq').value = "Max ASQs Reached";
    	} catch(err) {;}
    }
}



function outOfRangeWarning() {
	// Checks for out of range readings and warns user if found on string box change.
	// Range types: Systolic (BP_Sys) and Diastolic (BP_Dias) Blood Pressure
	// 				Hematocirt Samples, Thyroid Stimulating Hormones, and HbA1C
	
	var outOfRange = false;

	// Blood Pressure
	var bp_sys = toInteger(document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/BP_Sys").value);
	var bp_dia = toInteger(document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/BP_Dias").value);

	if (bp_sys > 120 || bp_dia > 80) {
		document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/BP_Sys").style.background = "#fcc";
		document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/BP_Dias").style.background = "#fcc";
		document.getElementById("bp_warn").style.visibility="visible";
		outOfRange = true;
	} else {
		document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/BP_Sys").style.background = "";
		document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/BP_Dias").style.background = "";
		document.getElementById("bp_warn").style.visibility="hidden";
	}


	// Hematocrit Sample
	var hema1 = toInteger(document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/HematocritSample1").value);
	var hema2 = toInteger(document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/HematocritSample2").value);
	var gender = getRadioValue(getDocElementsByName("hcpvisit:HCPVisitData/HCPIntakeInterview/Gender"));

	if (gender == 'M') {
		if ((hema1 < 40 || hema1 > 50) && (hema2 < 40 || hema2 > 50)) {
			document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/HematocritSample1").style.background = "#fcc";
			document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/HematocritSample2").style.background = "#fcc";
			document.getElementById("hema_warn").style.visibility="visible";
			outOfRange = true;
		} else {
			document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/HematocritSample1").style.background = "";
			document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/HematocritSample2").style.background = "";
			document.getElementById("hema_warn").style.visibility="hidden";
		}
	}
	else if (gender == 'F') {
		if ((hema1 < 36 || hema1 > 44) && (hema2 < 36 || hema2 > 44)) {
			document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/HematocritSample1").style.background = "#fcc";
			document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/HematocritSample2").style.background = "#fcc";
			document.getElementById("hema_warn").style.visibility="visible";
			outOfRange = true;
		} else {
			document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/HematocritSample1").style.background = "";
			document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/HematocritSample2").style.background = "";
			document.getElementById("hema_warn").style.visibility="hidden";
		}
	}


	// Thyroid Stimulating Hormone & Hemogolbin A1C
	var tsh = toFloat(document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/TSH").value);
	var hba1c = toFloat(document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/HBA1C").value);

	if(tsh < 0.35 || tsh > 5.5) {
		document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/TSH").style.background = "#fcc";
		document.getElementById("tsh_warn").style.visibility="visible";
		outOfRange = true;
	} else {
		document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/TSH").style.background = "";
		document.getElementById("tsh_warn").style.visibility="hidden";
	}

	if(hba1c < 4.0 || hba1c > 6.0) {
		document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/HBA1C").style.background = "#fcc";
		document.getElementById("hba_warn").style.visibility="visible";
		outOfRange = true;
	} else {
		document.getElementById("hcpvisit:HCPVisitData/HCPIntakeInterview/HBA1C").style.background = "";
		document.getElementById("hba_warn").style.visibility="hidden";
	}

	if (outOfRange)
		document.getElementById("rangeWarning").style.visibility="visible";
	else
		document.getElementById("rangeWarning").style.visibility="hidden";

}

