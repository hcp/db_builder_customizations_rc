/*
 * Functions for HCP Login page
 */

var HCP = HCP || {};
HCP.login = HCP.login || {};

(function(login){

    // defaults for modals on Login page
    // (shift up 20% of window height)
    login.xmodalDefaults = window.xmodalDefaults || function(){
        return {
            top: '-20%',
            bottom: '0px'
        };
    };
    
    login.formattedMessage = function(msg){
        return '<p class="hcp-msg">' + msg + '</p>';
    };


    function validEmailFormat(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }


    //////////////////////////////////////////////////
    // -------------------------------------------- //
    // FORGOT PASSWORD OR USERNAME
    // -------------------------------------------- //
    login.forgot = function( $modal ){

        // these functions are called through the
        // 'forgot' modal on the Login.vm page

        this.$modal = $modal;
        this.$form = $modal.find('form.forgot');

        // attach event handlers to form elements
        this.formSetup = function(){
            var $inputs = this.$form.find('input.forgot');
            $inputs.val('');
            $inputs.keyup(function(){
                if (!$(this).val()) { return }
                $inputs.not(this).val('');
            });
        };

        // make sure either 'email' or 'username' field has a value
        // otherwise show a message and stop form submission
        // old function: forgotValidation()
        this.validate = function() {
            var $email = this.$form.find('input.email');
            var email = $email.val();
            var username = this.$form.find('input.username').val();
            var opts = login.xmodalDefaults();
            var msg = 'Error.';
            if (!email && !username) {
                msg = login.formattedMessage('Please enter your email address or username.');
                xmodal.message('Error', msg, opts);
                return false;
            }
            if (email && !validEmailFormat(email)){
                msg = login.formattedMessage('Please enter an email address in the format "name@domain.com" and try again.');
                opts.action = function(){$email.focus()};
                xmodal.message('Invalid Email Format', msg, opts);
                return false;
            }
            return true;
        };

        // send AJAX request to get forgotten username or password
        // old function: forgotLoginRequest()
        this.request = function(){

            var params = [
                    'account=' + this.$form.find('input[name="username"]').val(),
                    'email=' + this.$form.find('input[name="email"]').val()
            ];
            if (window.csrfToken) {
                params.push('XNAT_CSRF=' + window.csrfToken);
            }

            xmodal.loading.open('Processing request...');

            var forgotRequest = $.ajax({
                type: 'GET',
                url: serverRoot + '/data/services/forgotlogin?' + params.join('&'),
                cache: false
            });
            forgotRequest.done(function( data, textStatus, jqXHR ){
                xmodal.loading.close();
                xmodal.close( login.forgot.$modal );
                var msg = login.formattedMessage( data );
                xmodal.message( msg, login.xmodalDefaults() );
            });
            forgotRequest.fail(function( jqXHR, textStatus, errorThrown ){
                xmodal.loading.close();
                var msg = login.formattedMessage( jqXHR.responseText );
                xmodal.message( msg, login.xmodalDefaults() );
            });
        };
    };
    // -------------------------------------------- //
    //////////////////////////////////////////////////
    //login.forgot = forgot; // do we need to expose this globally? Yes.




    //////////////////////////////////////////////////
    // -------------------------------------------- //
    // RE-SEND VERIFICATION EMAIL
    // -------------------------------------------- //
    login.resendVerification = function(e){

        e.preventDefault();

        var _count = 0;

        // AJAX submission (action for OK button
        function requestVerificationEmail(email){

            var loader_id = 'loader' + (++_count);

            xmodal.loading.open({ id: loader_id, title: 'Please Wait...' });

            var msg = login.xmodalDefaults();
            msg.buttonLabel = 'OK';
            msg.beforeShow = function(){
                xmodal.loading.close(loader_id);
            };

            var requestURL = serverRoot + '/data/services/sendEmailVerification?email=' + encodeURIComponent(email) ;

            var request = $.ajax({
                type: 'POST',
                url: requestURL
            });

            request.done(function( data, status ){
                msg.title = 'Email Sent';
                msg.content = 'A verification email has been sent to ' + email + '.';
                msg.action = function(){
                    //if (window.requestModal) {
                    //    xmodal.close(window.requestModal);
                    //}
                    //else {
                    xmodal.closeAll();
                    //}
                    window.location = serverRoot + '/app/template/Login.vm';
                };
                xmodal.message(msg)
            });

            request.fail(function( data, status, error ){

                var _status = data.status;

                msg.title = 'Error';

                if (_status === 503){
                    msg.content = 'You have exceeded the allowed number of email requests. Please try again later.';
                    xmodal.message(msg);
                }
                else if (_status === 400){
                    msg.content = 'Unknown email address.';
                    xmodal.message(msg);
                }
                else {
                    msg.content = 'An unexpected error has occurred. Please contact your administrator.';
                    xmodal.message(msg);
                }
            });
        }


        var __modal, __emailInput;

        var requestModal = login.xmodalDefaults();
        //requestModal.id = 'request-modal'
        requestModal.width = 400;
        requestModal.height = 200;
        requestModal.title = 'Resend Verification Email';
        //requestModal.top = modal_top;
        requestModal.template = $('#resend-email-template');
        requestModal.afterShow = function(obj){
            __modal = obj.$modal;
            __emailInput = __modal.find('#requestEmail');
            __emailInput.focus();
        };
        requestModal.buttons = {
            ok: {
                label: 'Go',
                close: false,
                isDefault: true,
                action: function(obj){
                    var email = __emailInput.val();
                    // opts = defaults for missing or invalid email address
                    var opts = login.xmodalDefaults();
                    opts.action = function(){
                        setTimeout(function(){
                            __emailInput.focus().select();
                        },10);
                    };
                    if (!email){
                        opts.title = 'Missing Email Address';
                        opts.content = 'Please enter an email address and try again.';
                        xmodal.message(opts);
                        return false;
                    }
                    if (!validEmailFormat(email)){
                        opts.title = 'Invalid Email Format';
                        opts.content = 'Please enter a valid email address in the format "name@domain.com" and try again.';
                        xmodal.message(opts);
                        return false;
                    }
                    //window.requestModal = obj.id;
                    requestVerificationEmail(email);
                }
            },
            close: {
                label: 'Close'
            }
        };

        // open the modal with the email address input
        xmodal.open(requestModal);

    };
    // -------------------------------------------- //
    //////////////////////////////////////////////////




    //////////////////////////////////////////////////
    // -------------------------------------------- //
    // REGISTRATION
    // -------------------------------------------- //
    //////////////////////////////////////////////////
    login.register = function( $modal ){

        this.$modal = $modal;
        this.$form = this.__form = $modal.find('form.register');

        var register = this;

        // object to hold input DOM (jQuery) objects
        var inputs = {};

        // functions to work with each input
        function InputActions( $input ){

            // the 'container' also contains the error elements
            this.getInputContainer = function(){
                return $input.closest('.inputContainer');
            };
            this.$inputContainer = this.getInputContainer();

            // confusing? yes. 'wrapper' EXCLUDES the error elements
            this.getInputWrapper = function(){
                return this.$inputContainer.find('.inputWrapper');
            };
            this.$inputWrapper = this.getInputWrapper();

            this.highlight = function( x ){
                if ( x === 'on' ){
                    this.$inputWrapper.addClass('highlighted');
                    this.$inputWrapper.find('.helptext').removeClass('hidden');
                }
                else if ( x === 'off' ){
                    this.$inputWrapper.removeClass('highlighted');
                    this.$inputWrapper.find('.helptext').addClass('hidden');
                }
            };

            this.resetErrors = function(){
                $input.removeClass('error');
                this.$inputWrapper.removeClass('error');
                this.$inputContainer.find('.error').addClass('hidden');
            };

            this.showError = function(error, wrapper){
                this.$inputContainer.find(error).removeClass('hidden');
                // add and remove the classes
                if (wrapper !== false){ // 'false' prevents 'error' class on wrapper
                    $input.addClass('error');
                    this.$inputWrapper.addClass('error');
                }
            };

            this.hideError = function(){
                var $error = this.$inputContainer.find('.error');
                // add and remove the classes
                $input.removeClass('error');
                this.$inputWrapper.removeClass('error');
                $error.addClass('hidden');
            };

            this.checkRequired = function(){
                if (!$input.val()) {
                    this.showError('.required.error');
                    return false;
                }
                else {
                    return true;
                }
            };
        }

        // object for validation methods
        var validate = {};

        // method names correspond to the "name" attribute
        // of elements we want to validate
        validate.username = function( $input, actions ){
            inputs.$username = $input;
            // allow only letters, numbers, and underscore
            var valid = true;
            var $lowercase = actions.$inputContainer.find('.username-lowercase').hide();
            //var usernamePattern = new RegExp('[^a-z0-9]','g');
            var usernamePattern = /^[a-z][a-z0-9]*$/g;
            var Username = $input.val(); // as-is
            var username = Username.toLowerCase();
            if (username && !username.match(usernamePattern)){
                actions.showError('.username-invalid');
                valid = false;
            }
            if (Username !== username){
                $input.one('focus.lowercase', function(){
                    $lowercase.hide();
                });
                $lowercase.show();
                $input.val(username);
            }
            // TODO: add 'duplicate' class if AJAX check returns true (for duplicate)
            if ($input.hasClass('duplicate')){
                actions.showError('.username-duplicate');
                valid = false;
            }
            return valid;
            // TODO: add AJAX verification to make sure username doesn't exist
            // will have to do that on form submission
//            var checkUsername = $.ajax({
//                type: 'POST',
//                url: serverRoot + '/registerUser'
//            });
//            checkUsername.done(function(data){
//                if (data === false){
//                    return true
//                }
//            })
        };

        validate.pw = function( $input, actions ){
            inputs.$pw = $input;
            var valid = true;
            var pwd = $input.val();
            //    if ((pwd) && (pwd.replace(/^[ ]*/,'').replace(/[ ]*$/,'').length >= 5)) {
            if (pwd && pwd.trim().length < 5 ){
                actions.showError('.password-simple');
                valid = false;
            }
            return valid;
        };

        validate.pwc = function( $input, actions ){
            inputs.$pwc = $input;
            var valid = true;
            var pwc = $input.val();
            //    if ((pwd) && (pwd.replace(/^[ ]*/,'').replace(/[ ]*$/,'').length >= 5)) {
            if (pwc && pwc !== inputs.$pw.val()){
                actions.showError('.password-mismatch');
                valid = false;
            }
            return valid;
        };

        validate.email = function ( $input, actions ){
            inputs.$email = $input;
            var valid = true;
            var email = $input.val();

            if (email && !validEmailFormat(email)){
                actions.showError('.email-invalid');
                valid = false;
            }
            return valid;
        };


//        validate.firstname = function( $input, actions ){
//            register.$firstname = $input;
//        };

        // -------------------------------------------- //
        // MASTER METHOD TO CALL SPECIFIC METHOD FOR
        // THE INPUT WE'RE WORKING WITH
        validate.thisInput = function( $input, actions ){
            var valid = true;
            var name = $input.attr('name');
//            var valid = true;
            if ($.isFunction(validate[name])){
                valid = validate[name]($input, actions);
            }
            else {
                actions.checkRequired();
            }
            if (valid) {
                actions.resetErrors();
                actions.highlight('off');
            }
            else {
                $input.val('').focus();
            }
            return valid;
        };
        // -------------------------------------------- //



        // -------------------------------------------- //
        // INITIALIZE THE REGISTRATION FORM
        // -------------------------------------------- //
        this.setupForm = function(){

            var $form = this.$form;

            // attach focusin and focusout event listeners
            // to 'required' form inputs
            $form.find('input.required').each(function(){

                var $input = $(this);

                // create a unique set of actions
                // to attach to each input
                var actions = new InputActions($input);

                $input.focusin(function(){
                    //actions.resetErrors();
                    actions.highlight('on');
                });

                $input.focusout(function(){
                    validate.thisInput($input, actions);
                });

            });

//            $form.on('submit', function(){
//
//                $form.find('input.required').each(function(){
//
//                });
//
//                $.each(validate, function( prop, fn ){
//                    fn();
//                });
//
//            });

        };

        // -------------------------------------------- //
        // VALIDATE THE REGISTRATION FORM ON SUBMIT
        // -------------------------------------------- //
        this.validateForm = function(){

            var errors = 0;

            register.$form.find('input.required').each(function(){

                var $input = $(this);

                // new set of actions for each input? yes.
                var actions = new InputActions($input);

                actions.resetErrors();

                var empty = false;

                if (!actions.checkRequired()){
                    errors++;
                    empty = true; // set validateForm() to false
                }

                if (!empty && !validate.thisInput($input, actions)){
                    errors++;
                }

            });

            if (jsdebug && console && console.log){
                console.log('errors: ' + errors);
            }

            return (errors === 0); // return true if zero errors

        };


        this.subscribeToEmailLists = function( email ){

            var lists = [];

            // the email list checkboxes are not in the main form,
            // but ARE in the modal
            register.$modal.find('input.email-list').each(function(){

                if (this.checked){

                    var form = $(this).data('form');
                    var $form = $('#'+form);

                    $form.find('input[name="email"]').val(email);
                    $form.submit();

                    lists.push(this.title);

                    if (jsdebug && console && console.log){
                        console.log('subscribed to ' + this.id + ' list.');
                    }

                }
            });

            if (lists.length){
                return '<b>' + lists.join('</b> and <b>') + '</b>';
            }
            else {
                return false;
            }

        };

    };
    // -------------------------------------------- //
    //////////////////////////////////////////////////
    //login.register = register; // do we need to expose the whole thing globally?
    //login.register = { setupForm: register.setupForm };


})(HCP.login);
