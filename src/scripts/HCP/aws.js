// JavaScript Document

/* MODAL CONFIGURATION */

// check and setup HCP.aws namespace object
var HCP = HCP || {};
HCP.aws = HCP.aws || {};

(function( aws, $ ) {

    // private AWS object
    var _aws={};

    // classnames for dialogs
    var aws_class = 'aws';
    var aws_class_success = 'aws aws-success';
    var aws_class_failure = 'aws aws-failure';

    // common settings for aws dialogs
    var aws_defaults = {
        width: 450,
        height: 250,
        scroll: false,
        enter: false
    };

    // 0: if user without signed DUT clicks AWS link.
    var aws_no_dut = $.extend( {}, aws_defaults, {
        title: 'AWS Connection: HCP Data Use Terms Required',
        content: $('#aws-no-dut').html(),
        buttons: {
            close: {
                label: 'Okay',
                isDefault: true
            }
        }
    });

    // 1: if user with signed DUT clicks on AWS link. Goal: create AWS credentials.
    var aws_setup = $.extend( {}, aws_defaults, {
        title: 'AWS Connection: Set Up Credentials',
        content: $('#aws-setup').html(),
        classNames: aws_class,
        buttons: {
            ok: {
                label: 'Create my AWS Credentials',
                isDefault: true,
                close: false,
                action: function (obj) {
                    // opens "processing modal"
                    _aws.setupInit();
                }
            },
            cancel: {
                label: 'Cancel',
                close: true,
                link: true
            }
        }
    });

    console.log(aws_setup);

    // 2: progress modal opened by aws_setup.
    var aws_setup_processing = $.extend( {}, aws_defaults, {
        title: 'AWS Connection: Set Up Credentials',
        content: $('#aws-setup-processing').html(),
        classNames: aws_class,
        buttons: {
            cancel: {
                label: 'Cancel',
                close: false, // disable the default modal close behavior. Force user to confirm.
                link: true,
                action: function (obj) {
//                    // confirm that the user wants to cancel this operation. If so, close the modal.
//                    if (confirm("Are you sure you want to cancel this operation?")) {
//                        // mckay, do we need to explicitly cancel the AWS request?
//					    xmodal.closeAll();
//                    }
                    // the xmodal way:
                    var confirmCancel = {
                        content: 'Are you sure you want to cancel this operation?',
                        okLabel: 'Yes, Cancel',
                        okAction: function(){ xmodal.closeAll() },
                        cancelLabel: 'No, Continue',
                        cancelAction: function(){ /* do stuff on cancel */ }
                    };
                    xmodal.confirm(confirmCancel);
                }
            }
        }
    });

    // 3: success modal opened by AWS credential creation call
    var aws_setup_success = $.extend( {}, aws_defaults, {
        width: 500,
        height: 425,
        title: 'AWS Connection: Success',
        content: $('#aws-setup-success').html(),
        classNames: aws_class_success,
        onClose: function (obj) {
            location.reload();
        },
        buttons: {
            close: {
                label: 'Close',
                isDefault: true,
                close: true,
                action: function (obj) {
                    location.reload();
                }
            }
        }
    });

    // 4: failure modal opened by AWS credential creation call. 
    var aws_setup_failure = $.extend( {}, aws_defaults, {
        title: 'AWS Connection Manager: Failure',
        content: $('#aws-setup-failure').html(),
        classNames: aws_class_failure,
        buttons: {
            ok: {
                label: 'Contact Support',
                isDefault: true,
                close: false,
                action: function (obj) {
                    $(location).attr('href', 'mailto:support@humanconnectome.org?subject=AWS Failure&body=Reason: '+obj.$modal.find('.aws-errormessage').html());
                }
            },
            close: {
                label: 'Close',
                link: true
            }
        }
    });

    // 5: user-initiated modal. displays stored user credentials, and has a link to create new. 
    var aws_recreate = $.extend( {}, aws_defaults, {
        width: 500,
        height: 425,
        title: 'AWS Connection Manager',
        content: $('#aws-confirm').html(),
        classNames: aws_class_success,
        beforeShow: function (obj) {
            obj.$modal.find('.aws-accesskey').val(awsAccessKey);
        },
        buttons: {
            ok: {
                label: 'Recreate my AWS Credentials',
                isDefault: true,
                close: false,
                action: function (obj) {
                    // opens "Recreate Credentials modal"
                    _aws.resetInit();
                }
            },
            cancel: {
                label: 'Cancel',
                close: true,
                link: true
            }
        }
    });

    // 6: progress modal opened by aws_confirm.
    var aws_recreate_processing = $.extend( {}, aws_defaults, {
        title: 'AWS Connection: Processing',
        content: $('#aws-recreate-processing').html(),
        classNames: aws_class,
        buttons: {
            cancel: {
                label: 'Cancel',
                close: false, // disable the default modal close behavior. Force user to confirm.
                link: true,
                action: function (obj) {
                    var $processing = obj.$modal;
                    // confirm that the user wants to cancel this operation. If so, close the modal.
//                    if (confirm("Are you sure you want to cancel this operation?")) {
//                        // mckay, do we need to explicitly cancel the AWS request?
//					    xmodal.closeAll();
//                    }
                    // the xmodal way:
                    var confirmCancel = {
                        content: 'Are you sure you want to cancel this operation?',
                        okLabel: 'Yes, Cancel',
                        okAction: function(){ xmodal.close($processing) },
                        cancelLabel: 'No, Continue',
                        cancelAction: function(){ /* do stuff on cancel */ }
                    };
                    xmodal.confirm(confirmCancel);
                }
            }
        }
    });

    // 7: success modal opened by AWS credential creation call
    var aws_recreate_success = $.extend( {}, aws_defaults, {
        width: 500,
        height: 425,
        title: 'AWS Connection: Success',
        content: $('#aws-recreate-success').html(),
        classNames: aws_class_success,
        buttons: {
            close: {
                label: 'Close',
                isDefault: true,
                close: true
            }
        }
    });

    // 8: failure modal opened by AWS credential creation call.
    var aws_recreate_failure = $.extend( {}, aws_defaults, {
        title: 'AWS Connection Manager: Failure',
        content: $('#aws-recreate-failure').html(),
        classNames: aws_class_failure,
        buttons: {
            ok: {
                label: 'Contact Support',
                isDefault: true,
                close: true,
                action: function (obj) {
                    $(location).attr('href', 'mailto:support@humanconnectome.org');
                }
            },
            close: {
                label: 'Close',
                link: true
            }
        }
    });

    // 9: user-initiated modal. displays stored user credentials relative to a specific project, and has a link to create new.
    var aws_confirm_proj = $.extend( {}, aws_defaults, {
        width: 500,
        height: 425,
        title: 'AWS Connection Manager',
        content: $('#aws-confirm-proj').html(),
        classNames: aws_class_success,
        buttons: {
            ok: {
                label: 'Recreate my AWS Credentials',
                isDefault: true,
                close: false,
                action: function (obj) {
                    // opens "Recreate Credentials modal"
                    _aws.setupInit();
                }
            },
            cancel: {
                label: 'Cancel',
                close: true,
                link: true
            }
        }
    });

var awsBucketChangeNotification = function(showRegenerate){ 
		xmodal.message({title: "Important Message",
				content: "<h3>IMPORTANT:  The AWS bucket for HCP data has temporarily changed.</h3><b>The new bucket is " + 
					"<em>s3://hcp-openaccess-temp/</em><br><br>" + 
					((showRegenerate) ? "<em>This change requires AWS keys to be regenerated.</em><br><br>" : "") +
					"</b>REASON:  Recent changes in the AWS Public Dataset Program has required migration of HCP data to a new AWS account." +
					"  While we migrate the original s3://hcp-openaccess/ bucket, the data must be accessed through a temporary bucket location, s3://hcp-openaccess-temp." +
					"  It is expected that the migration may take 4 - 6 weeks, after which time the data will once again be accessible via the original bucket.",
				height: 300,
				width: 700 });	
}


    /* BUTTON CLICK CONFIGURATION */
    $(document).ready(function () {
        $('.aws-launcher-nodut').on('click', function () {
            xmodal.open(aws_no_dut);
        });
        $('.aws-launcher-enable').on('click', function () {
            xmodal.open(aws_setup);
		//awsBucketChangeNotification(false);
        });

        $('.aws-launcher-recreate').on('click', function () {
            xmodal.open(aws_recreate);
		//awsBucketChangeNotification(true);
        });
    });



    /* AMAZON REQUESTS AND MODAL CHAINING LOGIC */

    // initial credential setup

    // if you need to expose an '_aws' internal function,
    // add it to the 'aws' (not '_aws') object
    // and call it as HCP.aws.functionName()
    _aws.setupSuccess = function (o) {
        var newAWSUserInfo = o.responseText;
        var newAWSUserInfoArray = newAWSUserInfo.split(",");
        if(newAWSUserInfoArray.length==3){
            aws_setup_success.beforeShow = function (obj) {
                var accessKey = newAWSUserInfoArray[1];
                var secretKey = newAWSUserInfoArray[2];
                obj.$modal.find('input.aws-accesskey').val(accessKey);
                obj.$modal.find('input.aws-secretkey').val(secretKey);
                awsAccessKey=newAWSUserInfoArray[1];
            };
            xmodal.closeAll();
            xmodal.open(aws_setup_success);
        }
        else{
            _aws.setupFailure(o);
        }
    };
    // to expose above function externally:
    // aws.setupSuccess = _aws.setupSuccess;
    // call as HCP.aws.setupSuccess()


    _aws.setupFailure = function (o) {
        // mckay: get error messages as a result from a failed call, then plug them into the modal using the beforeShow method. I'm inserting dummy values here.
        var errorMsg = o.responseText;
	if (errorMsg.includes("Status Code: 409")) {
       		console.log(errorMsg);
        	xmodal.closeAll();
		xmodal.confirm({
			content:  "<b>ERROR:  It appears an AWS account already exists for this username.  Regenerate keys for this account?</b>",
				okAction: function() { _aws.resetInit() },
				cancelAction: function(){ return },
				width: 420,
				height: 240
		});
		//xmodal.open(aws_recreate);
		return;
	} if (errorMsg.includes("could not insert:") && errorMsg.includes("org.nrg.xnat.entities.AmazonData")) {
		// This will happen if we already had database records.  Let's just do a key reset
		_aws.resetInit();
		return;
	}
        aws_setup_failure.beforeShow = function (obj) {
            obj.$modal.find('.aws-errormessage').html(errorMsg);
        };
        xmodal.closeAll();
        xmodal.open(aws_setup_failure);
    };

    _aws.setupCallback = {
        success: _aws.setupSuccess,
        failure: _aws.setupFailure,
        cache: false, // Turn off caching for IE
        scope: this
    };

    _aws.setupInit = function () {
	    xmodal.open(aws_setup_processing);
        YAHOO.util.Connect.asyncRequest('GET',serverRoot + "/data/services/amazon/new",_aws.setupCallback,null,this);
    };

    // credential recreation

    _aws.resetSuccess = function (o) {
        var newAWSUserInfo = o.responseText;
        var newAWSUserInfoArray = newAWSUserInfo.split(",");
        if(newAWSUserInfoArray.length==3){
            aws_recreate_success.beforeShow = function (obj) {
                var accessKey = newAWSUserInfoArray[1];
                var secretKey = newAWSUserInfoArray[2];
                obj.$modal.find('input.aws-accesskey').val(accessKey);
                obj.$modal.find('input.aws-secretkey').val(secretKey);
                awsAccessKey=newAWSUserInfoArray[1];
            };
            xmodal.closeAll();
            xmodal.open(aws_recreate_success);
        }
        else{
            _aws.resetFailure(o);
        }
    };

    _aws.resetFailure = function (o) {
        // mckay: get error messages as a result from a failed call, then plug them into the modal using the beforeShow method. I'm inserting dummy values here.
        var errorMsg = o.responseText;
	if (errorMsg.includes("Status Code: 404")) {
       		console.log(errorMsg);
        	xmodal.closeAll();
		xmodal.confirm({
			content:  "<b>ERROR:  Could not find AWS account.  This AWS account may have been removed.  Attempt to regenerate it?</b>",  
				//okAction: function() { xmodal.open(aws_setup) },
				okAction: function() { _aws.setupInit() },
				cancelAction: function(){ return },
				width: 420,
				height: 240
		});
		//xmodal.open(aws_recreate);
		return;
	}
        aws_recreate_failure.beforeShow = function (obj) {
            var errorMsg = '';
            obj.$modal.find('.aws-errormessage').html(errorMsg);
        };
        xmodal.closeAll();
        xmodal.open(aws_recreate_failure);
    };

    _aws.resetCallback = {
        success: _aws.resetSuccess,
        failure: _aws.resetFailure,
        cache: false, // Turn off caching for IE
        scope: this
    };

    _aws.resetInit = function () {
        xmodal.open({
        	width: 300,
	        height: 200,
	        title: 'Processing Request',
	        content: '<img src="' + serverRoot + '/images/loading_spinner.gif" alt="loading">Processing Request',
	        buttons: { }
	});
        YAHOO.util.Connect.asyncRequest('GET',serverRoot + "/data/services/amazon/reset",_aws.resetCallback,null,this);
    }

})( HCP.aws, jQuery );
