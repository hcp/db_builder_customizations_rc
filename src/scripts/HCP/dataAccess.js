/*
 * Functions for HCP data use terms dialogs
 */

var HCP = HCP || {};
HCP.dataAccess = HCP.dataAccess || {};
var terms = [];

(function(access){
    // external functions should be moved
    // here and accessed as needed via
    // HCP.dataAccess.funcName();
    // internal functions can stay private
})(HCP.dataAccess);

function getDutList() {
    var uri = serverRoot + '/REST/config/ldap/access.json?contents=true';

    YAHOO.util.Connect.asyncRequest('GET', uri,
        {
            success : function (o) {
                var json = YAHOO.lang.JSON.parse(o.responseText);
                window.terms = json['accessList'];
                dutCheck();
            },
            cache : false
        });
}
getDutList();

/*
 * ---
 * Data Use Terms controls
 * ---
 */

function dutConfirm(ele) {
    /*
     * Change the page according to their acceptance of terms.
     * Requires a page refresh. Add the DUT version accepted as a hash to the URL beforehand, so we can parse it and launch a modal.
     */
    window.location.hash = ele;
    window.location.reload(true);
}

// if a single "I agree to terms" checkbox is needed, use this.
function scrollToEnable( $container, $el ){
    // enable '$el' after scrolling to bottom of '$container'
    $container.on('scroll', function(){
        var $this = $(this);
        if ($this.scrollTop() + $this.innerHeight() >= this.scrollHeight) {
            $el.removeClass('disabled').prop('disabled',false).show();
        }
    });
}

function agreeChange( $checkbox, $target ) {
    // if user checks box, don't let them uncheck.
    // enable the target (button) and guide them to it.
    $checkbox.prop('disabled',true);
    $target.removeClass('disabled').prop('disabled',false).focus();
}

function dutError( ele, responseText ) {
    xmodal.loading.close('dut-processing');
    xmodal.message('Error','Could not register data use acceptance ' + responseText);
    /*
     * Change the page according to their acceptance of terms.
     * Would this require a page refresh in XNAT?
     $('#dut-' + ele).addClass("hidden");
     $('#dut-thanks-div').removeClass("hidden").show();
     $('#reg-thanks-msg').text("").append("Could not register data use acceptance" + responseText );
     */
}

function dutSubmit(terms) {
    //$('#dut-processing-div').removeClass("hidden").show();
    xmodal.loading.open({id:'dut-processing',title:'Processing Request...'});
    getDutList();

    var arg, params;

    function doRequest(){
        var url = serverRoot +'/REST/services/datause?terms='+params+'&acceptTerms=true';
        if (window.csrfToken){
            url += '&XNAT_CSRF=' + window.csrfToken;
        }
        YAHOO.util.Connect.asyncRequest('POST', url,
            {
                success:duPostSuccess,
                failure:duPostFailure,
                argument:arg,
                cache:false
            });
    }

    for (var i = 0; i < window.terms.length; i++) {
        var dut = window.terms[i]['name'];
        if (terms == dut) {
            arg = [window.terms[i]['name']];
            params = window.terms[i]['name'];
            doRequest();
            break;
        }
    }
}

function duPostSuccess(o) {
    if (o.responseText.toUpperCase() == "TRUE" || o.responseText.toUpperCase() == "MOD") {
        dutConfirm(o.argument);
    } else {
        dutNotYet(o.argument,o.responseText);
    }
}

function duPostFailure(o) {
    dutError(o.argument,o.responseText);
}

function duGetSuccess(o) {
    var dutCheck = (o.responseText.toUpperCase() === "TRUE");
    if (o.responseText.toUpperCase() === "TRUE") {
        dutAlready(o.argument);
    } else if (o.responseText.toUpperCase() == "MOD") {
        window.location.reload(true);
        return;
    } else {
        dutNotYet(o.argument,false);
    }
    receivedDutCheckResponse(o.argument, dutCheck);
}

function duGetFailure(o) {
    if (!(jq.cookie(o.argument[0]) == "true" && jq.cookie("button_user") == dutUser)) {
        dutNotYet(o.argument,false);
    }
    receivedDutCheckResponse(o.argument, false);
}

function receivedDutCheckResponse(ele, isDutAlready) {
    dutCheckResults[ele[0]] = isDutAlready;
    if(++numberOfDutChecksCompleted === 4) {
        if(dutCheckResults["Phase2OpenAccess"]) {
            XNAT.app.HcpSplash.loadPhase2Data();	// this guy will close our modal when he's done
        }
        else {
            closeModalPanel("HcpSplashModal");
        }
    }
}

var numberOfDutChecksCompleted;
var dutCheckResults;

function dutCheck() {

    // Check data use acceptance status
    var arg;
    var params;
    numberOfDutChecksCompleted = 0;
    dutCheckResults = {};

    openModalPanel("HcpSplashModal", "Loading data...");

    for (var i = 0; i < window.terms.length; i++) {
        arg = [window.terms[i]['name']];
        params = 'terms='+window.terms[i]['name']+'&XNAT_CSRF='+window.csrfToken;
        YAHOO.util.Connect.asyncRequest('GET',serverRoot +'/REST/services/datause?'+params,
            {
                success:duGetSuccess,
                failure:duGetFailure,
                argument:arg,
                cache:false
            });
    }
}

function dutAlready(eleArr) {
    for(var i = 0; i < eleArr.length; ++i) {
        var ele = eleArr[i];
        // DUT accepted - show agreed button
        jq.cookie(ele,"true");
        $('#list-' + ele).find('.permission-controlled')
            .removeClass("hidden disabled");

        $('#dut-processing-div').addClass("hidden").hide();
        $('#list-' + ele).find('input.request')
            .removeClass("hidden disabled").show()
            .removeClass("request")
            .addClass("accepted")
            .attr("disabled","disabled")
            .val("Agreed");
        $('#div-' + ele).addClass("hidden").hide();
        $('#div2-' + ele).removeClass("hidden disabled").show();
        $('#btn-' + ele).removeClass("hidden disabled").show();

        // highlight acceptance of terms
        $('#list-' + ele + ' .dut')
            .addClass("accepted")
            .text("Terms of Use Accepted")
    }
}

var dutUser;

function dutNotYet(eleArr,force) {
    for(var i = 0; i < eleArr.length; ++i) {
        var ele = eleArr[i];
        // DUT not yet accepted - show request button
        if (force || !(jq.cookie(ele) == "true" && jq.cookie("button_user") == dutUser)) {
            jq.cookie(ele,"false");
        }
        $('#dut-processing-div').addClass("hidden").hide();
        $('#list-' + ele).find('input.request')
            .removeClass("hidden");
        $('#div-' + ele).removeClass("hidden disabled").show();
        $('#div2-' + ele).addClass("hidden").hide();
        $('#btn-' + ele).addClass("hidden").hide();

        // highlight acceptance of terms
        $('#list-' + ele + ' .dut')
            .removeClass("accepted")
            .text("Terms of Use")
    }
}

function initIndexWindow() {
    //alert("Vers 1");
    getDutList();
    //dutCheck();
}

function doInitButtons(user) {
    // Speed up setting up of buttons using values from cookies
    dutUser = jq.cookie("button_user");
    if ( jq.cookie("button_user") == user) {
        dutCookieSet("Phase1PilotData");
        dutCookieSet("Phase2OpenAccess");
        dutCookieSet("WorkbenchData");
    }
    jq.cookie("button_user",user);
}

function dutCookieSet(ele) {
    if (jq.cookie(ele) == "true" ) {
        dutAlready(ele);
    } else {
        dutNotYet(ele,false);
    }
}

function scrollFunc(ele,check) {
    if (ele.clientHeight + ele.scrollTop >= ele.scrollHeight) {
        $(check).removeProp('disabled').removeClass('disabled');
    }
}


/*
 * -- 'master' function for restricted access modal  --
 */
function showRestrictedModal(_opts){

//    _opts = (typeof _opts != 'undefined') ? _opts : {} ;

    var default_message = '' +
        '<p>You are about to download restricted and highly sensitive data. Please remember that you agreed to abide by the ' +
        '<a href="http://humanconnectome.org/data/data-use-terms/restricted-access.html" target="_blank">HCP Restricted ' +
        'Data Terms of Use</a>. You must take appropriate precautions to secure this data and prevent ' +
        'distribution to unauthorized parties.</p>';

    var modal_message = _opts.message || default_message ;

    var modal_content = '' +

            '<p style="float:left;padding-top:20px;padding-bottom:20px;padding-right:10px;">' +
            '   <img src="'+serverRoot+'/style/furniture/icon-lock-64px.png" alt="Lock">' +
            '</p>' +
            '<h3>Warning!</h3>' +
            //'</br>' +
            modal_message
        ;

    var restrictedModalObj = {
        width: _opts.width || 450,
        height: _opts.height || 250,
        title: _opts.title || 'Restricted Data Warning',
        content: modal_content ,
        buttons: {
            ok: {
                label: _opts.okLabel || 'I understand. Please continue.',
                //classNames: 'btn-agree disabled',
                action: _opts.okAction || function(){ xmodal.message('Error','Error: Action is undefined.') },
                close: true
            },
            close: {
                label: 'Cancel'
            }
        }//,
//        footerContent: '' +
//            '<label class="dut-accept-message" style="position:relative;font-size:13px;">' +
//            '<input type="checkbox" class="agree-check"> ' +
//            'I agree to abide by these terms of use.</label>'
//        okLabel: _opts.okLabel || 'I understand. Please continue.',
//        okAction: _opts.okAction || xmodal.message('Error','Error: Action is undefined.'),
//        cancelLabel: 'Cancel'
    };

    xmodal.open(restrictedModalObj);

}




$(function(){

    var $body = $body || $(document.body);

    /*
     * -- overlay function for data releases  --
     */

    $('.show-more').hover(function(){
        // perform on hover
        $(this).addClass('hover');
    },function(){
        // perform on mouse-out
        $(this).removeClass('hover');
    });



    /*
     * -- DUT controls for acceptance of terms --
     */

    // initialize and launch modal based on the terms associated with the "DUT Ruquired" button clicked by the user
    // data attributes on that button are used to pass those terms to this script
    $body.on('click', '.dut-launch', function(){
        if (jsdebug && console && console.log){
            console.log("DUT button clicked", $(this));
        }
        var dutTitle = $(this).data('duttitle');
        var dut = $(this).data('dut');
        var dutModalId = 'dut_modal_'+dut;

        // initialize DUT form
        $('#'+ dutModalId + ' .dut-accept-message').hide();
        $('#'+ dutModalId + ' .agree-check').prop('checked',false).prop('disabled',false);
        $('#'+ dutModalId + ' .btn-agree').addClass('disabled');

        var dutModal = {
            //id: dutModalId,
            width: '60%',
            height: '80%',
            //size: 'med',
            minWidth: 700,
            minHeight: 400,
            title: 'Terms of Use: ' + dutTitle,
            className: 'dut-launch-modal',
            template: dutModalId,
            footerContent: '' +
                '<label class="dut-accept-message" style="position:relative;font-size:13px;">' +
                '<input type="checkbox" class="agree-check"> ' +
                'I agree to abide by these terms of use.</label>',
            buttons: {
                ok: {
                    label: 'Accept Terms and Access Dataset',
                    action: function(obj){
                        var $form = obj.$modal.find('#dut-form-'+dut);
                        if (jsdebug && console && console.log){
                            console.log("clicked, submitting #dut-form-" + dut);
                        }
                        $form.submit();
                    },
                    classNames: 'btn-agree',
                    isDefault: true,
                    disabled: true
                },
                close: {
                    label: 'Cancel'
                }
            },
            afterShow: function(obj){
                var $modal = obj.$modal;
                //scrollToEnable($modal.find('.body .dutModal'), $modal.find('.dut-accept-message'));
                $modal.on('change', '.agree-check', function(){
                    agreeChange($(this), $modal.find('.btn-agree'));
                });
            }
        };
        xmodal.open(dutModal);
    });


    // IS THIS STILL USED???
    // ???
    // if checkboxes are used for each term, use this.
    $body.on('click', '.term-check', function(){
        var $modal = $(this).closest('div.xmodal');
        // check this element and disable it
        $(this).prop('checked',true).prop('disabled',true);
        // check to see if each element has been checked ("accepted"), and if so, allow user to accept all terms and continue with order.
        var acceptance;
        $('.term-check').each(function(){
            acceptance = true;
            if ( $(this).prop('checked') === false ) { acceptance=false; return false; }
        });
        if (acceptance) {
            // display an acceptance message in the footer of the DUT modal
            $modal.find('.footer .content input').prop('checked',true);
            $modal.find('label').html('You have accepted each of the terms of use');
            // enable the form submit button
            $modal.find('.button.default').removeClass('hidden disabled');
        }
    });
});