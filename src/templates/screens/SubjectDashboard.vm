#* @vtlvariable name="om" type="org.nrg.xdat.om.XnatProjectdata" *#
#* @vtlvariable name="data" type="org.apache.turbine.util.RunData" *#
#* @vtlvariable name="project" type="java.lang.String" *#
#* @vtlvariable name="attributeMap" type="java.lang.String" *#
#* @vtlvariable name="categories" type="java.lang.String" *#
#* @vtlvariable name="assessments" type="java.lang.String" *#
#* @vtlvariable name="content" type="org.apache.turbine.services.pull.tools.ContentTool" *#
##Developer: Tim Olsen tim@radiologics.com
##<script type="text/javascript" src="$content.getURI('/scripts/data-access.js')"></script>
<script type="text/javascript" src="$content.getURI('/scripts/HCP/dataAccess.js')"></script>
<script type="text/javascript" src="$content.getURI('/scripts/datasetUtils/datasetUtils.js')"></script>
<div id="header">
    #parse("/screens/DataView.vm")
	<div class="project-page header">
    <!-- Orient the user here. Where are we? -->
		#if($om.getId())
    		<h1 id="page-title">Subject Dashboard: $!om.getName()</h1>
		#else
    		<h1 id="page-title">Subject Dashboard: All Data</h1>
		#end
    </div>
</div>
	
<script>
var server= "$content.getURI("images")/";
var user_email= "$data.getSession().getAttribute("user").getEmail()";
var categories = $categories;
var assessments = $assessments;
var attributeMap = $attributeMap;
var current_project_name = '$project';
var current_tier = '$data.getSession().getAttribute("tier")';
var alertProjectChange = "true"; // alert users upon project change.
#set($accessLevel = $data.getSession().getAttribute("userTiers").get($project).get("accessLevel"))
#if($accessLevel > 0)
    var hasRestrictedAccess = true;
#else
    var hasRestrictedAccess = false;
#end
</script>

<input type="hidden" id="subjectLabels" value="${data.getSession().getAttribute("downloadSubjects")}" />
<input type="hidden" id="dashboardProject" value="${data.getSession().getAttribute("project")}" />

<style type="text/css">
    body div.xmenu { margin: 0 ; top: 0 ; position: relative ; }
    body div.xmenu > div { background: transparent ; border-color: transparent ; border-radius: 0 ; }
    body div.xmenu ul { min-width: 100% ; }
    body div.xmenu.open > div { background: transparent ; }
    .filter-row div.xmenu div.xmenu_button {
        width: 148px ; height: 28px ;
        background: transparent url($content.getURI('style/furniture/select-bg2.png')) center right no-repeat ; }
    .filter-row div.xmenu.open div.xmenu_button { background-image: url($content.getURI('style/furniture/select-bg2-active.png')); }
    .filter-row div.xmenu.disabled div.xmenu_button { background-image: url($content.getURI('style/furniture/select-bg2-disabled.png')); }
    .filter-row div.xmenu .button_content { width: 110px ; height: 28px ; margin: 0 ; line-height: 30px ; letter-spacing: 0.02em ; color: #000 ; background: transparent ; }
    body div.xmenu ul li.image a { padding-left: 22px ; background-position: 4px 4px ; }
    body div.xmenu .button_content img { top: -2px ; padding-right: 2px ; }
</style>

<div name="content area" id="toolbar-positioner">

    <div id="toolbar-container" class="subjectKey">
        <div id="toolbar"> <!-- toolbar listing order from right to left -->

            <div class="toolset vertical">
                <div class="toolset-header">Get Data</div>
                <div class="toolset-content">
                    #set( $projPageUri = "/data/projects/$project" )

                    <div class="tool" onclick="window.location.assign('$content.getURI($projPageUri)')">
						<span class="icon icon-dashboard"></span>
						<label>Dataset Resources</label>
					</div>
                    <div class="tool" id="open_csv">
                        <select class="xmenu" id="csv_selector" size="15" style="display:none;"></select>
                        <span class="button_content static" style="width:auto;height:auto;margin:0;padding:0;line-height:1;background:transparent;">
                            <span class="icon icon-group"></span>
							<label>Download CSV</label>
                        </span>
                    </div>
                    <div id="downloadButton" class="tool hidden" onclick="XNAT.app.filterUI.postFiltersActionPreSave();"><form id="filtersActionForm" action="$content.getURI('app/action/DownloadPackagesAction')" method="POST"></form>
						<span class="icon icon-download"></span>
                        <label>Download Images</label>
                    </div>
					<div id="downloadPlaceholder" class="tool disabled"> <!-- placeholder is necessary for positioning -->
						<span class="icon icon-timer"></span>
						<label>Please Wait...</label>
					</div>
                </div>
            </div>

            <div class="toolset vertical">
                <div class="toolset-header">Select Groups</div>
                <div class="toolset-content">

                    <div class="tool" id="open_group">
                        <select class="xmenu" id="group_selector" size="15" style="display:none;"></select>
                        <span class="button_content static" style="width:auto;height:auto;margin:0;padding:0;line-height:1;background:transparent;">
                            <span class="icon icon-group"></span>
							<label>Open Group</label>
                        </span>
                    </div>

                    <div id="saveGroup" class="tool" onclick="XNAT.app.filterUI.saveGroup();">
                        <span class="icon icon-group-save"></span>
                        <label>Save Group</label>
                    </div>
                </div>
            </div>
			
			<div class="toolset current-selection">
                <div class="toolset-header">Current Selection</div>
                <div class="toolset-content">
                    <h3 class="currSubjDisplay subject-group-icon">All Subjects</h3>
                    <p class="counts"></p>
					<p id="group-actions" class="hidden"></p>
                </div>
            </div>
			
        </div>
    </div> <!-- end toolbar -->
	
	<script>
		// clean up appearance of toolbar, after all dynamic elements have been loaded.
		$(document).ready(function(){
			toolbarCleanup(); 
		});
	</script>
	
	<div id="filterset-container" style="position:relative">
		<div><div class="toolset-header">Data Filters</div></div>
		<form id="filterForm" name="filterForm" class="filters" onsubmit="return false">
		</form>
	
		<div class="filter-container">
			<div id="addNewFilter" class="addNewFilter filter-row active" style="display: none;"><input name="newFilter" type="button" id="newFilter" class="newFilter" value="Add New Filter" onclick="newFilter();"/></div>
		</div>
	</div>
</div>

<div id="dashboard_results" style="clear:both">
    <!--<div class="dashboardPanel fullCol">
        <h1>Result Set <span id="filter_display" class="filter_display"></span><span class="dash_control" style="margin-bottom:5px;padding-right:24px"><a id="dash_control_link" onclick="toggle_dashboard();">Hide Filters</a></span></h1>
        <div class="yui-skin-sam">
            <div id="primary_table" style="overflow:auto"></div>
        </div>
	</div>-->

    <div id="search_tab_module">
        <div class="bd">
            <div id="search_tabs"></div>
        </div>
    </div>
</div>

<script type="text/javascript" src="$content.getURI("scripts/generated/xdat_stored_search.js")"></script>
<script type="text/javascript" src="$content.getURI("scripts/generated/xdat_search_field.js")"></script>
<script type="text/javascript" src="$content.getURI("scripts/generated/xdat_criteria.js")"></script>
<script type="text/javascript" src="$content.getURI("scripts/generated/xdat_criteria_set.js")"></script>
<script type="text/javascript" src="$content.getURI("scripts/dashboard/tabManager_filter.js")"></script>
<script type="text/javascript" src="$content.getURI("scripts/search/saveSearch.js")"></script>
<script type="text/javascript" src="$content.getURI("scripts/dashboard/searchManager_filter.js")"></script>
<script type="text/javascript" src="$content.getURI('scripts/dashboard/dataTableSearch_filter.js')"></script>
<script type="text/javascript" src="$content.getURI('scripts/dashboard/dataTableStoredSearch_filter.js')"></script>
<script type="text/javascript" src="$content.getURI('scripts/dashboard/HcpUtil.js')"></script>
<script type="text/javascript" src="$content.getURI('scripts/dashboard/csvs.js')"></script>
<script type="text/javascript" src="$content.getURI('scripts/dashboard/dashboard.js')"></script>
<script type="text/javascript" src="$content.getURI('scripts/dashboard/watermark.js')"></script>
<script type="text/javascript" src="$content.getURI("scripts/dashboard/filterUI.js")"></script>
<script type="text/javascript" src="$content.getURI("scripts/dashboard/FilterLabelEditor.js")"></script>

## -----------------------------------------------------------------------------------------------------
## make sure these are called properly from HeaderIncludes.vm before completely removing
##
##<script type="text/javascript" src="$content.getURI("scripts/xModal/xModal.js")"></script>
##<script type="text/javascript" src="$content.getURI("scripts/xMenu/xMenu.js")"></script>
##<script type="text/javascript" src="$content.getURI("scripts/tabWrangler/tabWrangler.js")"></script>
## -----------------------------------------------------------------------------------------------------

<script type="text/javascript">
    jq(document).ready(function() {
        #if($autoOpenGroupName)
            XNAT.app.filterUI.autoOpenGroup("$autoOpenGroupName");
        #elseif($autoOpenSubject)
            XNAT.app.filterUI.autoOpenSubject("$autoOpenSubject");
        #else
            XNAT.app.filterUI.openNormally();
        #end
    });
    jq(window).load(function(){
        wrangleTabs('#search_tabs');
    });

</script>
