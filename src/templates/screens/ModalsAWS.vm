## prespecify modal contents for the AWS workflow. modals will be chained by a series of functions. 

<!-- modal 0: "AWS Connection: HCP Data Use Terms Required" -->
<div id="aws-no-dut" class="html-template existing" style="display:none;">
  <div style="background:url(/style/icons/icon-alert-48.png) left top no-repeat; padding-left: 60px;">
	<h4>Alert!</h4>
	<p>In order to access HCP data on Amazon Web Services, you need to sign the open access data use terms here on ConnectomeDB.</p>
    <p><a href="http://humanconnectome.org/AWS-help" title="HCP documentation for AWS users">Need help using AWS?</a></p>
  </div>
</div>

<!-- modal 1: "AWS Connection Manager: Set Up Credentials" -->
<div id="aws-setup" class="html-template existing" style="display:none;">
  <h4>Set up AWS Access for HCP Data</h4>
  <p>To get started, we will need to create a login for you that will work with the HCP data bucket on Amazon S3. You will receive a login (the same as your ConnectomeDB login), an access key, and a "secret key". These will only be issued once, so please store them in a safe place. </p>
  <p><a href="http://humanconnectome.org/AWS-help" title="HCP documentation for AWS users">Need help using AWS?</a></p>
</div>

<!-- modal 2: "AWS Connection Manager: Processing" -->
<div id="aws-setup-processing" class="html-template existing" style="display:none;">
  <h4>Creating AWS Access Keys for HCP Data</h4>
  <p style="background:url(/scripts/xmodal-v1/loading_bar.gif) left center no-repeat; background-size: 330px 27px; height: 27px;"></p>
  <ul class="modal-updates" style="color: #808080; font-style: italic">
  	<li class="connecting">Connecting...</li>
	<li class="sending" style="display:none">Sending access request to Amazon</li>
	<li class="receiving" style="display:none">Receiving key pair</li>
	<li class="creating" style="display:none">Creating download link</li>
  </ul>
</div>

<!-- modal 3: "AWS Connection Manager: Success" -->
<div id="aws-setup-success" class="html-template existing" style="display:none;">
  <h4>Successfully created AWS credentials</h4>
  <div class="friendlyForm">
	<p><label>Username</label><input class="aws-username" type="text" readonly value="$data.getSession().getAttribute("user").getUsername()" /></p>
	<p><label>Access Key ID</label><input class="aws-accesskey" type="text" readonly value="" /></p>
	<p><label>Secret Access Key</label><input class="aws-secretkey" type="text" readonly value="" /></p>
  </div>
  <p>Note: Your secret access key will not be stored in ConnectomeDB. Please store this information in a safe place. (Access keys can be regenerated as needed.)</p>
  <p><a href="http://humanconnectome.org/AWS-help" title="HCP documentation for AWS users">Next Steps</a></p>
</div>

<!-- modal 4: "AWS Connection Manager: Failure" -->
<div id="aws-setup-failure" class="html-template existing" style="display:none;">
  <h4>Oops!</h4>
  <p>We encountered an error when attempting to set up your AWS access. Please notify a ConnectomeDB admin. </p>
  <p><textarea class="aws-errormessage" style="height: 4em; width: 100%;"></textarea></p>
</div>

<!-- modal 5: "AWS Connection Manager" -->
<div id="aws-confirm" class="html-template existing" style="display:none;">
  <h4>AWS Access Enabled</h4>
  <div class="friendlyForm">
	<p><label>Username</label><input class="aws-username" type="text" readonly value="$data.getSession().getAttribute("user").getUsername()" /></p>
	<p><label>Access Key ID</label><input class="aws-accesskey" type="text" id="existingAccessKey" readonly value="$!data.getSession().getAttribute("awsAccessKey")" /></p> ## mckay: need stored value
  </div>
  <p>You can choose to generate a new access key pair, if you cannot access your HCP AWS account. Please note: your original access keys will be disabled if you generate new ones.</p>
  <p><a href="http://humanconnectome.org/AWS-help" title="HCP documentation for AWS users">Need help using AWS?</a></p>
</div>

<!-- modal 6: "AWS Connection Manager: Processing" -->
<div id="aws-recreate-processing" class="html-template existing" style="display:none;">
  <h4>Recreating AWS Access Keys for HCP Data</h4>
  <p style="background:url(/scripts/xmodal-v1/loading_bar.gif) left center no-repeat; background-size: 330px 27px; height: 27px;"></p>
  <ul class="modal-updates" style="color: #808080; font-style: italic">
      <li class="connecting">Connecting...</li>
      <li class="sending" style="display:none">Sending access request to Amazon</li>
      <li class="receiving" style="display:none">Receiving key pair</li>
      <li class="creating" style="display:none">Creating download link</li>
  </ul>
</div>

<!-- modal 7: "AWS Connection Manager: Success" -->
<div id="aws-recreate-success" class="html-template existing" style="display:none;">
  <p><strong>Successfully recreated AWS credentials</strong></p>
  <div class="friendlyForm">
	<p><label>Username</label><input class="aws-username" type="text" readonly value="$data.getSession().getAttribute("user").getUsername()" /></p>
	<p><label>Access Key ID</label><input class="aws-accesskey" type="text" readonly value="" /></p>
	<p><label>Secret Access Key</label><input class="aws-secretkey" type="text" readonly value="" /></p>
  </div>
  <p>Note: Your secret access key will not be stored in ConnectomeDB. Please store this information in a safe place. (Access keys can be regenerated as needed.)</p>
  <p><a href="http://humanconnectome.org/AWS-help" title="HCP documentation for AWS users">Need help using AWS?</a></p>
</div>

<!-- modal 8: "AWS Connection Manager: Failure" -->
<div id="aws-recreate-failure" class="html-template existing" style="display:none;">
  <h4>Oops!</h4>
  <p>We encountered an error when attempting to recreate your AWS access. Your original credentials may now be disabled as well. Please notify a ConnectomeDB admin. </p>
  <p><textarea class="aws-errormessage" style="height: 4em; width: 100%;"></textarea></p>
</div>

<!-- modal 9: "AWS Connection Manager" -->
<div id="aws-confirm-proj" class="html-template existing" style="display:none;">
  <p><strong>Access This Data on AWS</strong></p>
  <p>This dataset is available in on AWS in the HCP bucket at <span class="aws-dataset-url">hcp-openaccess/500subject/</span>. <a href="http://humanconnectome.org/AWS-help" title="HCP documentation for AWS users">Need help using AWS?</a></p>
  <div class="friendlyForm">
	<p><label>Username</label><input class="aws-username" type="text" readonly value="$data.getSession().getAttribute("user").getUsername()" /></p>
	<p><label>Access Key ID</label><input class="aws-accesskey" type="text" readonly value="" /></p> ## mckay: need stored value
  </div>
  <p>You can choose to generate a new access key pair, if you cannot access your HCP AWS account. Please note: your original access keys will be disabled if you generate new ones.</p>
  <p><a href="http://humanconnectome.org/AWS-help" title="HCP documentation for AWS users">Need help using AWS?</a></p>
</div>