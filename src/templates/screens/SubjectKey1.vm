<script type="text/javascript" src="$content.getURI("scripts/project/accessibility.js")"></script>

$page.setTitle("XDAT")
$page.setLinkColor($ui.alink)
$page.setVlinkColor($ui.vlink)
#set($months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"])
#set($days = [ 1..31 ])
#set($years = [ $!turbineUtils.getYear()..1900])
#if ($data.message)
<script type="text/javascript">
	var messageText = "$data.message"
	$("#message-container").append("<div class='warning'><span class='icon icon-sm icon-close'></span> "+messageText+"</div>"); 
	$(document).ready(function(){ 
		$("#message-container div").on("click",function(){ $(this).slideUp(200) }); 
	}); 
</script>
#end

#if($vr)
<script type="text/javascript">
	var messageText = "$vr.toHTML();"
	$("#message-container").append("<div class='error'><span class='icon icon-sm icon-close'></span> <strong>Form Validation Error, please contact administrator:</strong><br /> "+messageText+"</div>"); 
	$(document).ready(function(){ 
		$("#message-container div").on("click",function(){ $(this).slideUp(200) }); 
	}); 
</script>
#end

<div id="header">
	#parse("/screens/DataView.vm")
	<div class="subject-page header">
	#if($projectID) 
		<h1 id="page-title">Edit HCP Subject Key: $getName</h1>
        <p>HCP Subject Key ID: $subjectKeyProjectId</p>
	#else
		<h1 id="page-title">Create Subject Key</h1>
		<div id="info-button" class="button-large">
		  <ul style="padding-left: 15px;">
			<li><a href="javascript:" onclick="whatModal();">What do you mean by "Coded ID"?</a></li>
			<li><a href="javascript:" onclick="howModal();">How do I create a Subject Key?</a></li>
		  </ul>
		</div>
		<p style="margin-bottom: 1em;">Connectome researchers who have used restricted data in their subject analysis are required to use Coded IDs in their articles or posters, rather than using original subject IDs. (See <a href="http://humanconnectome.org/data/data-use-terms/restricted-access.html" target="_blank">HCP Restricted Data Use Terms</a>, part C) This interface allows you to upload a Subject Key, which maps your Coded IDs to the original subject IDs for each of the subjects you used. </p>
		<p><strong>Step 1:</strong> Describe your Subject Key. (You can edit these descriptions later, before you make it viewable by other users.)</p>
	#end
	</div>
</div>

<script>
	function whatModal() { 
		xModalMessage('Defining "Subject Key" and "Coded ID"', '<p>A <strong>Coded ID</strong> is a new label to describe a subject without allowing readers to connect that subject back to its HCP ID. For example, "Subject A" could be a Coded ID for HCP Subject 100307.</p><p>A <strong>Subject Key</strong> is a collection of Coded IDs and the HCP IDs for each subject you cite. Only HCP users with restricted data access may view these Subject Keys.</p>');
	}
	
	function howModal() { 
		xModalMessage('How do I create a Subject Key?', '<p>We encourage researchers to keep track of their Coded IDs in a spreadsheet. ConnectomeDB allows users to upload a simple spreadsheet into a Subject Key on our server, describe it, link it to published research, and make it viewable by other HCP users with restricted access.</p>');
	} 
</script>

#if($data.getSession().getAttribute("hasRestrictedAccess"))
<!-- You must have restricted access to edit Subject Keys. -->

	#if($projectID)
	<form name="form1" class="friendlyForm" method="post" action="$link.setAction("ModifyProject")" ONSUBMIT="return validateProjectForm();">
	#else
	<form name="form1" class="friendlyForm" method="post" action="$link.setAction("AddProject")" ONSUBMIT="return validateProjectForm();">
	#end

		#if($projectID)
			#set( $parentTemplate = 'edit' )
		#else
			#set( $parentTemplate = 'add' )
		#end
		#if( $projType == 'subjectKey' )
			#parse("/screens/xnat_projectData/edit/details_subject_key.vm")
		#else
			<div class="error">Something has gone wrong. Please contact a ConnectomeDB administrator, or try this action again.</div>
		#end
	
		#xdatEditProps($item $edit_screen)
		
		<div class="inputWrapper wide">
			<p class="form-submit">
				<span style="margin-right: 20px;">
					#set($umbrellaProj = $data.getSession().getAttribute("project"))
					<a href="javascript:" onclick="window.location.assign('/data/projects/$umbrellaProj')">Cancel</a>
				</span>
				<input type="submit" name="eventSubmit_doPerform" value="Submit"/>
			</p>
		</div>
        	
	</form>
	
	<script type="text/javascript">
	
		function subOnEnter(e,inp) {
			// get the event
			var evt = e||window.event;
			// pressed enter?
			if(evt.keyCode == 13) {
				// in your case, you want to validate, so:
				if(validate_form()) {
					// submit the form (will not fire onsubmit, validation above)
					inp.form.submit();
					// note we re using the passed inputs .form instead of a literal
				}
			}
		}
	
		function validateProjectForm(){
			if(document.getElementById("xnat:projectData/name").value==""){
				xModalMessage('Project Validation', 'Please specify a $displayManager.getSingularDisplayNameForProject().toLowerCase() title.');
				document.getElementById("xnat:projectData/name").focus();
				return false;
			}
	
			if(document.getElementById("xnat:projectData/secondary_ID").value==""){
				xModalMessage('Project Validation', 'Please specify a short title for use in menus.');
				document.getElementById("xnat:projectData/secondary_ID").focus();
				return false;
			}
	
			return true;
		}
	</script>
#else
<div class="banner error">You must have access to restricted data in order to create or edit subject keys. <a href="http://humanconnectome.org/data/data-use-terms/restricted-access-overview.html" target="_blank" title="Learn more about HCP Restricted Data Terms and Usage">HCP Restricted Data Use Terms</a>.</div>
#end