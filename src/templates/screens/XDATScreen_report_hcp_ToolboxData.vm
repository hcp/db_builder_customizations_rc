<!-- BEGIN XDATScreen_report_hcp_ToolboxData.vm -->
$page.setTitle("NIH Toolbox Assessment")
#parse("/screens/HcpReportTop.vm")

<div class="report-info-block">
	<p><i>*Denotes Explanation of Scoring Type</i></p>

	<h2 class="expts_header">Cognition</h2>
	<table width="100%">
	  <thead>
		<tr><th>Instrument</th><th>Unadjusted Scale Score</th><th>Age-Adjusted Scale Score</th></tr>
	  </thead>
	  
	  <tbody>
		<tr><td>Dimensional Card Sort Test</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_dim_card_uscr")</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_dim_card_ascr")</td></tr>
		<tr><td>Flanker Inhibitory Control and Attention Test</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_flanker_uscr")</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_flanker_ascr")</td></tr>
		<tr><td>List Sorting Working Memory Test</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_list_sort_uscr")</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_list_sort_ascr")</td></tr>
		<tr><td>Oral Reading Recognition Test</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_oral_read_uscr")</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_oral_read_ascr")</td></tr>
		<tr><td>Pattern Comparison Processing Speed Test</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_pattern_uscr")</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_pattern_ascr")</td></tr>
		<tr><td>Picture Sequence Memory Test</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_pic_seq_uscr")</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_pic_seq_ascr")</td></tr>
		<tr><td>Picture Vocabulary Test</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_pic_vocab_uscr")</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_pic_vocab_ascr")</td></tr>
		<tr><td>Cognition Fluid Composite</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_fluid_comp_uscr")</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_fluid_comp_ascr")</td></tr>
		<tr><td>Cognition Early Childhood Composite</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_early_comp_uscr")</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_early_comp_ascr")</td></tr>
		<tr><td>Cognition Total Composite</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_total_comp_uscr")</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_total_comp_ascr")</td></tr>
		<tr><td>Cognition Crystallized Composite</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_crystal_comp_uscr")</td><td>$!item.getDoubleProperty("hcp:ToolboxData/cog_crystal_comp_ascr")</td></tr>
	  </tbody>
	</table>
	<p><i>*Unadjusted Scale Score: <br>&nbsp;&nbsp;Mean of 100, SD of 10 (all Toolbox Norming Sample that took this measure (18 and older)</i></p>
	<p><i>*Age-Adjusted Scale Score: <br>&nbsp;&nbsp;Mean of 100 SD of 10 (age appropriate part of Toolbox Norming Sample, bands of 18-29, or 30-35)</i></p>

	<h2 class="expts_header">Emotion</h2>
	<table width="100%">
	  <thead>
		<tr><th>Instrument</th><th>T-Score</th></tr>
	  </thead>
	  
	  <tbody>
		<tr><td>Fear-Somatic Arousal Survey</td><td>$!item.getDoubleProperty("hcp:ToolboxData/emo_fear-som_tscr")</td></tr>
		<tr><td>Friendship Survey</td><td>$!item.getDoubleProperty("hcp:ToolboxData/emo_friend_tscr")</td></tr>
		<tr><td>General Life Satisfaction Survey</td><td>$!item.getDoubleProperty("hcp:ToolboxData/emo_life_sat_tscr")</td></tr>
		<tr><td>Instrumental Support Survey</td><td>$!item.getDoubleProperty("hcp:ToolboxData/emo_inst_sup_tscr")</td></tr>
		<tr><td>Loneliness Survey</td><td>$!item.getDoubleProperty("hcp:ToolboxData/emo_lonely_tscr")</td></tr>
		<tr><td>Meaning and Purpose Survey</td><td>$!item.getDoubleProperty("hcp:ToolboxData/emo_meaning_tscr")</td></tr>
		<tr><td>Perceived Hostility Survey</td><td>$!item.getDoubleProperty("hcp:ToolboxData/emo_hostil_tscr")</td></tr>
		<tr><td>Perceived Rejection Survey</td><td>$!item.getDoubleProperty("hcp:ToolboxData/emo_reject_tscr")</td></tr>
		<tr><td>Perceived Stress Survey</td><td>$!item.getDoubleProperty("hcp:ToolboxData/emo_stress_tscr")</td></tr>
		<tr><td>Positive Affect Survey</td><td>$!item.getDoubleProperty("hcp:ToolboxData/emo_pos_affect_tscr")</td></tr>
		<tr><td>Sadness Survey</td><td>$!item.getDoubleProperty("hcp:ToolboxData/emo_sadness_tscr")</td></tr>
		<tr><td>Self-Efficacy Survey</td><td>$!item.getDoubleProperty("hcp:ToolboxData/emo_self-eff_tscr")</td></tr>
		<tr><td>Anger-Affect Survey</td><td>$!item.getDoubleProperty("hcp:ToolboxData/emo_anger-aff_tscr")</td></tr>
		<tr><td>Anger-Hostility Survey</td><td>$!item.getDoubleProperty("hcp:ToolboxData/emo_anger-hostil_tscr")</td></tr>
		<tr><td>Anger-Physical Agression Survey</td><td>$!item.getDoubleProperty("hcp:ToolboxData/emo_anger-phys_tscr")</td></tr>
		<tr><td>Emotional Support Survey</td><td>$!item.getDoubleProperty("hcp:ToolboxData/emo_support_tscr")</td></tr>
		<tr><td>Fear Affect Survey</td><td>$!item.getDoubleProperty("hcp:ToolboxData/emo_fear-aff_tscr")</td></tr>
	  </tbody>
	</table>
	<p><i>*T-Score: <br>&nbsp;&nbsp;Mean of 50, SD of 10 (all Toolbox Norming Sample that took this measure (18 and older)</i></p>


	<h2 class="expts_header">Motor</h2>
	<table width="100%">
	  <thead>
		<tr><th>Instrument</th><th>Unadjusted Scale Score</th><th>Age-Adjusted Scale Score</th><th>Computed Score</th></tr>
	  </thead>
	  
	  <tbody>
		<tr><td>Grip Strength Test</td><td>$!item.getDoubleProperty("hcp:ToolboxData/mot_grip_uscr")</td><td>$!item.getDoubleProperty("hcp:ToolboxData/mot_grip_ascr")</td><td></td></tr>
		<tr><td>2-Minute Walk Endurance Test</td><td>$!item.getDoubleProperty("hcp:ToolboxData/mot_walk_2min_uscr")</td><td>$!item.getDoubleProperty("hcp:ToolboxData/mot_walk_2min_ascr")</td><td></td></tr>
		<tr><td>4-Meter Walk Gait Speed Test</td><td></td><td></td><td>$!item.getDoubleProperty("hcp:ToolboxData/mot_walk_4m_cscr")</td></tr>
		<tr><td>9-Hole Pegboard Dexterity Test</td><td>$!item.getDoubleProperty("hcp:ToolboxData/mot_pegboard_uscr")</td><td>$!item.getDoubleProperty("hcp:ToolboxData/mot_pegboard_ascr")</td><td></td></tr>
	  </tbody>
	</table>
	<p><i>*Unadjusted Scale Score: <br>&nbsp;&nbsp;Mean of 100, SD of 15 (all Toolbox Norming Sample)</i></p>
	<p><i>*Age-Adjusted Scale Score: <br>&nbsp;&nbsp;Mean of 100, SD of 15 (age appropriate part of Toolbox Norming Sample, bands of 18-29, or 30-35)</i></p>
	<p><i>*Computed Score: <br>&nbsp;&nbsp;Meters per second (no other scores available)</i></p>

	<h2 class="expts_header">Sensory</h2>
	<table width="100%">
	  <thead>
		<tr><th>Instrument</th><th>Unadjusted Scale Score</th><th>Age-Adjusted Scale Score</th><th>T-Score</th><th>Computed Score</th></tr>
	  </thead>
	  
	  <tbody>
		<tr><td>Odor Identification Test</td><td>$!item.getDoubleProperty("hcp:ToolboxData/sen_odor_uscr")</td><td>$!item.getDoubleProperty("hcp:ToolboxData/sen_odor_ascr")</td><td></td><td></td></tr>
		<tr><td>Pain Interference Survey</td><td></td><td></td><td>$!item.getDoubleProperty("hcp:ToolboxData/sen_pain_tscr")</td><td></td></tr>
		<tr><td>Pain Intensity Test (Raw Score)</td><td></td><td></td><td>$!item.getIntegerProperty("hcp:ToolboxData/sen_pain_intense_raw")</td><td></td></tr>
		<tr><td>Taste Intensity Test</td><td>$!item.getDoubleProperty("hcp:ToolboxData/sen_taste_uscr")</td><td>$!item.getDoubleProperty("hcp:ToolboxData/sen_taste_ascr")</td><td></td><td></td></tr>
		<tr><td>Words-in-Noise Test (Version 1.0)</td><td></td><td></td><td></td><td>$!item.getDoubleProperty("hcp:ToolboxData/sen_noise_cscr")</td></tr>
		<tr><td>Words-in-Noise Test (Version 2.0)</td><td></td><td></td><td></td><td>$!item.getDoubleProperty("hcp:ToolboxData/sen_noisev2_cscr")</td></tr>
	  </tbody>
	</table>
	<p><i>*Unadjusted Scale Score: <br>&nbsp;&nbsp;Mean of 100, SD of 15 (all Toolbox Norming Sample)</i></p>
	<p><i>*Age-Adjusted Scale Score: <br>&nbsp;&nbsp;Mean of 100, SD of 15 (age appropriate part of Toolbox Norming Sample, bands of 18-29, or 30-35)</i></p>
	<p><i>*T-Score: <br>&nbsp;&nbsp;Mean of 50, SD of 10 (all Toolbox Norming Sample that took this measure (18 and older)</i></p>
	<p><i>*Computed Score: <br>&nbsp;&nbsp;Decibels of Signal To Noise</i></p>
</div> <!-- end .report-info-block --->

#parse("/screens/ReportProjectSpecificFields.vm")

<!-- end XDATScreen_report_hcp_ToolboxData.vm -->
