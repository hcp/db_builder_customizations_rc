<div class="dataset_toolbox report-info-block">
    <div class="dataset_tool">
        <p><strong>Data Reference</strong></p>
        <p>
            <img src="$content.getURI('/images/icon-document.png')" align="left" width="16" height="16">
            <a href="http://humanconnectome.org/documentation/" target="_blank" title="Open reference in new window">HCP Data Reference Manual</a></p>
        <p>
            <img src="$content.getURI('/images/icon-document.png')" align="left" width="16" height="16">
            <a href="http://humanconnectome.org/documentation/MEG1/" target="_blank" title="Open reference in new window">MEG Release Documentation</a></p>

        #if($user.canRead("xnat:subjectData/project",$data.getSession().getAttribute("defaultRestrictedProject")))
            <p><img src="$content.getURI('/images/icon-document.png')" align="left" width="16" height="16">
                <a href="http://humanconnectome.org/data/data-use-terms/restricted-data-reference.html" target="_blank" title="Open reference in new window">Restricted Data Reference</a></p>
        #end

        <p><strong>Purchase data and storage</strong></p>
        <p><img src="$content.getURI('/images/shipping_icon_HCP.png')" height="32" width="32" align="left"><a href="http://humanconnectome.org/connectome-in-a-box/">Order Connectome <br>in a Box</a></p>
        <p><strong>Pipeline Software</strong></p>
        <p><img src="$content.getURI('/images/icon-document.png')" align="left" width="16" height="16"><a href="http://humanconnectome.org/documentation/MEG1/meg-pipeline.html" target="_blank">MegConnectome Software</a></p>
    </div>
    <div class="dataset_tool">
        #set( $uriPrefix =  '/REST/search/dict/Subject Information/results?format=csv&removeDelimitersFromFieldValues=true&restricted=0&project=')
        #set( $uriEnd = $data.getSession().getAttribute("defaultProject"))
        #set( $csvURI = "$uriPrefix$uriEnd" )
        <p><strong>Quick Downloads</strong></p>
        <p>
            <img src="$content.getURI('/images/icon-patch.png')" align="left" width="16" height="16"> <a href="$content.getURI('/app/action/ChooseDownloadResources?project=HCP_Resources&resource=Patch&filePath=HCP-Patch-March2014.zip')">March 2014 Data Patch</a> <span class="tip_icon" style="margin-right:3px;left:2px;top:3px;">
											<span class="tip shadowed" style="top:-11px;z-index:10000;white-space:normal;left:0px;width:300px;background-color:#ffc;">
												This patch file addresses issues with Task fMRI analysis and physiological data. It also includes all changes from the Q1-Q2 data patch.  <a href="http://humanconnectome.org/documentation/updates/march-2014-patch-release.html" target="_blank" style="font-weight:bold; color: #0168db">Details</a>
											</span>
										</span>
        </p>
        <p>
            <img src="$content.getURI('/images/icon-document.png')" align="left" width="16" height="16"> <a href="$content.getURI('/app/action/ChooseDownloadResources?project=HCP_Resources&resource=Scripts&filePath=HCP_TFMRI_scripts.zip')">E-Prime fMRI scripts</a></p>
        <p>
            <img src="$content.getURI('/images/icon-document.png')" align="left" width="16" height="16"> <a href="$content.getURI('/app/action/ChooseDownloadResources?project=HCP_Resources&resource=Scripts&filePath=scanner_performance_data.zip')">In-Scanner Performance Data</a>
							<span class="tip_icon" style="margin-right:3px;left:2px;top:3px;">
								<span class="tip shadowed" style="top:-11px;z-index:10000;white-space:normal;left:0px;width:300px;background-color:#ffc;">
									This ZIP archive contains all "stats.csv" outputs from each of the Task-fMRI scans. Each CSV file captures subject performance characteristics that are not yet searchable in the ConnectomeDB database.
								</span>
							</span>
        </p>
        <p>
            <img src="$content.getURI('/images/tool-download-images.png')" align="left" width="16" height="16"> <a href="$content.getURI($csvURI)">Download Behavioral Data</a></p>
        #if($user.canRead("xnat:subjectData/project",$data.getSession().getAttribute("defaultRestrictedProject")))
            <p>
                <img src="$content.getURI('/images/tool-download-images.png')" align="left" width="16" height="16"> <a id="download_restricted" href="javascript:;" style="color: #900;">Restricted Data</a></p>
        #end
    </div>
    <script>
        // disable links when resources are not ready
        $('.notReady').on("click",function(e) { e.preventDefault(); alert('Coming Soon'); });
    </script>
    <div class="dataset_tool">
        <p><strong>Group Average Functional Connectivity</strong></p>
        <p><img src="$content.getURI('/images/tool-download-images.png')" align="left" width="16" height="16"> <a href="$content.getURI('/app/action/ChooseDownloadResources?project=HCP_Resources&resource=GroupAvg&filePath=HCP_Q1-Q6_R468_rfMRI_groupPCA_d4500_Eigenmaps.zip')">Group-PCA Eigenmaps for 468 Subjects (many related)</a>
						<span class="tip_icon" style="margin-right:3px;left:2px;top:3px;">
							<span class="tip shadowed" style="top:-11px;z-index:10000;white-space:normal;left:-150px;width:300px;background-color:#ffc;">
								These Group-PCA Eigenmaps can be used to create dense connectomes locally in wb_command, the command line program distributed with Connectome Workbench. <a href="http://humanconnectome.org/software/workbench-command.php" target="_blank" style="font-weight:bold; color: #0168db">See Documentation</a>
							</span>
						</span>
        </p>
        <p><strong>Dense Connectomes </strong></p>
        <p style="font-size:11px">These files can be downloaded, accessed directly via Connectome Workbench, or created locally using Group-PCA Eigenmaps. <a href="http://humanconnectome.org/documentation" target="_blank" title="Open HCP Documentation in a new window" style="font-weight:bold">See Documentation</a></p>
        <p><img src="$content.getURI('/images/tool-download-images.png')" align="left" width="16" height="16"> <a href="$content.getURI('/app/action/ChooseDownloadResources?project=HCP_Resources&resource=GroupAvg&filePath=HCP_Q1-Q6_R468_rfMRI_groupPCA_d4500.zip')">468 Subjects (Many Related), full correlation</a></p>
        <p><img src="$content.getURI('/images/tool-download-images.png')" align="left" width="16" height="16"> <a href="$content.getURI('/app/action/ChooseDownloadResources?project=HCP_Resources&resource=GroupAvg&filePath=HCP_Q1-Q6_R468_rfMRI_groupPCA_d4500_MGTR.zip')">468 Subjects (Many Related), mean-gray-timecourse-regressed</a></p>
    </div>

    <div id="dataset_tool_custom_group" class="dataset_tool" style="display:none;">

        <form id="exploreSubjectGroupForm" name="exploreSubjectGroupForm" method="post" action="$content.getURI('/app/template/SubjectDashboard.vm')">
            <select id="custom_group_selector" name="custom_group_selector"></select>
            <span class="button_content" style="width:160px;height:28px;line-height:28px;padding-left:42px;background:transparent url($content.getURI('style/furniture/icon-subject-group.png')) 8px 0 no-repeat;">Open Custom Group...</span>
            <input type="hidden" id="subjectGroupName" name="subjectGroupName" />
        </form>
    </div>
</div>
