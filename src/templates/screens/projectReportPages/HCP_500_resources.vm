<script type="text/javascript">
	$(document).ready(function(){ 
		// handler for restricted CSV download button
		$('body').on('click','#download_restricted',function(){
            var restrictedModalObj = {};
            restrictedModalObj.okAction = function(){
                // Hardcoding project ok here since the VM name contains project ID
                window.location=serverRoot+"/REST/search/dict/Subject Information/results?format=csv&removeDelimitersFromFieldValues=true&restricted=1,2&project=HCP_500";
            };
            showRestrictedModal(restrictedModalObj);
        });
	});
</script>

## Reset this list here so that we don't get a previously filtered list from the Dashboard
$data.getSession().setAttribute("downloadSubjects", "")

#set($userTiersMap = $data.getSession().getAttribute("userTiers"))
#set($projTierAccess = $userTiersMap.get("HCP_500").get("accessLevel"))

<div class="report-info-block">

## Check tier access and also whether user can read the project
#if (! $user.canRead("xnat:subjectData/project", $project.getId()))
    <p>Please sign the Data Use Terms in order to access these project resources.</p>
#else

  <div class="dataset_toolbox" style="min-height: 280px;">
    <div class="dataset_tool">
        <p><strong>Data Reference</strong></p>
        <p>
            <span class="icon icon-sm icon-alert"></span>
            <a href="https://wiki.humanconnectome.org/display/PublicData/HCP+Data+Release+Updates%3A+Known+Issues+and+Planned+fixes" target="_blank" title="HCP Wiki: Known Data Issues and Planned Fixes">Known Data Issues and Planned Fixes</a>
        </p>
        <p>
            <span class="icon icon-sm icon-docs"></span>
            <a href="http://humanconnectome.org/documentation/" target="_blank" title="Open reference in new window">HCP Data Reference Manual</a></p>
        <p>
            <span class="icon icon-sm icon-docs"></span>
            <a href="https://wiki.humanconnectome.org/display/PublicData/HCP+Users+FAQ" target="_blank" title="Open FAQ in new window">HCP Users FAQ</a></p>
        <p>
            <span class="icon icon-sm icon-docs"></span>
            <a href="https://wiki.humanconnectome.org/display/PublicData/MEG+Data+FAQ" target="_blank" title="Open FAQ in new window">MEG Data FAQ</a></p>
        <p>
			<span class="icon icon-sm icon-docs"></span>
			<a href="https://wiki.humanconnectome.org/display/PublicData/HCP+Data+Dictionary+Public-+500+Subject+Release" target="_blank" title="HCP Wiki: Public HCP Dictionary">HCP Data Dictionary </a>
		    <span class="tip_icon" style="margin-right:3px;left:2px;top:3px;">
				<span class="tip shadowed" style="top:15px;z-index:10000;white-space:normal;left:-120px;width:300px;background-color:#ffc;">
					The Data Dictionary contains names, column header abbreviations, and descriptions for data in ConnectomeDB collected on individual subjects, including demographic, behavioral, and physical measurements.
				</span>
			</span>
        </p>
		
		#if($data.getSession().getAttribute("tier") > 0)
            <p><span class="icon icon-sm icon-docs"></span>
                <a href="http://humanconnectome.org/data/data-use-terms/restricted-data-reference.html" target="_blank" title="Open reference in new window">Restricted Data Reference</a></p>
        #end

        <p><strong>Pipeline Software</strong></p>
        <p><span class="icon icon-sm icon-docs"></span><a href="https://github.com/Washington-University/Pipelines" target="_blank">HCP MR Pipelines Software</a></p>
        <p><span class="icon icon-sm icon-docs"></span><a href="http://humanconnectome.org/documentation/MEG1/meg-pipeline.html" target="_blank">MegConnectome Software</a></p>
    </div>
    <div class="dataset_tool">
        #set( $csvURI = '/REST/search/dict/Subject Information/results?format=csv&removeDelimitersFromFieldValues=true&restricted=0&project=HCP_500' )
        <p><strong>Purchase data and storage</strong></p>
        <p><span class="icon icon-sm icon-cinab"></span><a href="http://humanconnectome.org/connectome-in-a-box/">Order Connectome in a Box</a></p>
        <p><strong>Quick Downloads</strong></p>
        <p>
            <span class="icon icon-sm icon-patch"></span> 
			<a href="$content.getURI('/app/action/ChooseDownloadResources?project=HCP_Resources&resource=Patch&filePath=HCP-Patch-March2014.zip')">March 2014 Data Patch</a> 
			<span class="tip_icon" style="margin-right:3px;left:2px;top:3px;">
				<span class="tip shadowed" style="top:15px;z-index:10000;white-space:normal;left:-136px;width:300px;background-color:#ffc;">
					This patch file addresses issues with Task fMRI analysis and physiological data. It also includes all changes from the Q1-Q2 data patch.  <a href="http://humanconnectome.org/documentation/updates/march-2014-patch-release.html" target="_blank" style="font-weight:bold; color: #0168db">Details</a>
				</span>
			</span>
        </p>
        <p>
            <span class="icon icon-sm icon-docs"></span> <a href="$content.getURI('/app/action/ChooseDownloadResources?project=HCP_Resources&resource=Scripts&filePath=HCP_TFMRI_scripts.zip')">E-Prime fMRI scripts</a></p>
        <p>
            <span class="icon icon-sm icon-docs"></span> <a href="$content.getURI('/app/action/ChooseDownloadResources?project=HCP_Resources&resource=Scripts&filePath=scanner_performance_data.zip')">In-Scanner Performance Data</a>
            <span class="tip_icon" style="margin-right:3px;left:2px;top:3px;">
                <span class="tip shadowed" style="top:15px;z-index:10000;white-space:normal;left:-169px;width:300px;background-color:#ffc;">
                    This ZIP archive contains all "stats.csv" outputs from each of the Task-fMRI scans. Each CSV file captures subject performance characteristics that are not yet searchable in the ConnectomeDB database.
                </span>
            </span>
        </p>
        <p>
            <span class="icon icon-sm icon-download"></span> <a href="$content.getURI($csvURI)">Download Behavioral Data</a></p>
        #if($data.getSession().getAttribute("tier") > 0)
            <p><span class="icon icon-sm icon-download"></span> <a id="download_restricted" href="javascript:" style="color: #900;">Download Restricted Data</a></p>
        #end
        <p> <span class="icon icon-sm icon-download"></span><a type="text/csv" href="$content.getURI('/REST/projects/HCP_Resources/resources/FreeSurferResults/files/unrestricted_hcp_freesurfer.csv')">Download Expanded FreeSurfer Data</a></p>
    </div>
    <script>
        // disable links when resources are not ready
        $('.notReady').on("click",function(e) { e.preventDefault(); alert('Coming Soon'); });
    </script>
    <div class="dataset_tool">
        <p><strong>Group Average Functional Connectivity</strong> - Updated April 2015</p>
        <p style="font-size:11px">The dense connectome file can be downloaded or accessed directly via Connectome Workbench. <a href="http://humanconnectome.org/documentation/S500/R468_Group-average_rfMRI_Connectivity_April2015.pdf" target="_blank" title="Open HCP Documentation in a new window" style="font-weight:bold">See Documentation</a></p>
        <p><span class="icon icon-sm icon-download"></span> <a href="$content.getURI('/app/action/ChooseDownloadResources?project=HCP_Resources&resource=GroupAvg&filePath=HCP_S500_R468_rfMRI_MIGP_d4500ROW_zcorr.zip')">468 Subjects (Many Related) Dense Connectome</a> (33GB)
            <span class="tip_icon" style="margin-right:3px;left:2px;top:3px;">
                <span class="tip shadowed" style="top:15px;z-index:10000;white-space:normal;left:-320px;width:400px;background-color:#ffc;">
                    Improved group-average full correlation functional connectivity matrix ("dense" functional connectome, the grayordinate x grayordinate full correlation matrix) for the R468 group. <a href="http://humanconnectome.org/documentation/mound-and-moat-effect.html" target="_blank" title="Open HCP Documentation in a new window" style="font-weight:bold">See Documentation</a> for more information on the included improvements.
                </span>
            </span>
        </p>
        <p><span class="icon icon-sm icon-download"></span> <a href="$content.getURI('/app/action/ChooseDownloadResources?project=HCP_Resources&resource=GroupAvg&filePath=HCP_S500_R468_rfMRI_groupPCA_d4500_Eigenmaps.zip')">468 Subjects Group-PCA Eigenmaps</a>
            <span class="tip_icon" style="margin-right:3px;left:2px;top:3px;">
                <span class="tip shadowed" style="top:15px;z-index:10000;white-space:normal;left:-235px;width:400px;background-color:#ffc;">
                    (2GB) Group-PCA eigenmaps that can be used as input to group-ICA. They can also be used to generate the dense connectome, but to do this optimally is not trivial, and requires following the procedures outlined in the <a href="http://humanconnectome.org/documentation/mound-and-moat-effect.html" target="_blank" title="Open HCP Documentation in a new window" style="font-weight:bold">Documentation</a>
                </span>
            </span>
        </p>
        <p style="font-size:11px">This is identical to the previously released file named <br><i>HCP_Q1-Q6_R468_rfMRI_groupPCA_d4500_Eigenmaps.dtseries.nii</i></p>
        <p><br /><strong>HCP500 Parcellation+Timeseries+Netmats (PTN)</strong></p>
        <p><span class="icon icon-sm icon-download"></span> <a href="$content.getURI('/app/action/ChooseDownloadResources?project=HCP_Resources&resource=GroupAvg&filePath=HCP500_Parcellation_Timeseries_Netmats.zip')">468 Subjects (Many Related)</a>
            <span class="tip_icon" style="margin-right:3px;left:2px;top:3px;">
                <span class="tip shadowed" style="top:15px;z-index:10000;white-space:normal;left:-167px;width:400px;background-color:#ffc;">
                    Resting-state fMRI analyses including group-ICA "parcellations", individual subject node-timeseries, and individual subject netmats (parcellated connectomes). <a href="http://humanconnectome.org/documentation/S500/HCP500_GroupICA+NodeTS+Netmats_Summary_28aug2014.pdf" target="_blank" style="font-weight:bold; color: #0168db">See Documentation</a>.
                </span>
            </span>
        </p>
    </div>

    <div id="dataset_tool_custom_group" class="dataset_tool" style="display:none;">

        <form id="exploreSubjectGroupForm" name="exploreSubjectGroupForm" method="post" action="$content.getURI('/app/template/SubjectDashboard.vm')">
            <select id="custom_group_selector" name="custom_group_selector"></select>
            <span class="button_content" style="width:160px;height:28px;line-height:28px;padding-left:42px;background:transparent url($content.getURI('style/furniture/icon-subject-group.png')) 8px 0 no-repeat;">Open Custom Group...</span>
            <input type="hidden" id="subjectGroupName" name="subjectGroupName" />
        </form>
    </div>
  </div>

#end

</div>
