<p><strong>MGH-UCLA HCP Consortium Open Access Data Use Terms </strong></p>
<p>I request access to data collected by the MGH-UCLA consortium of the Human Connectome Project (HCP) for the purpose of scientific investigation, teaching or the planning of clinical research studies and agree to the following terms: </p>
<ol start="1">
	<li>I will receive access to de-identified data and will not attempt to establish the identity of, or attempt to contact any of the HCP subjects. </li>
	<li>I will not further disclose these data beyond the uses outlined in this agreement and my data use application. </li>
	<li>I will require anyone on my team who utilizes these data, or anyone with whom I share these data to comply with this data use agreement. </li>
	<li>I will accurately provide the requested information for persons who will use these data and the analyses that are planned using these data.</li>
	<li>I will respond promptly and accurately to requests to update this information. </li>
	<li>I will comply with any rules and regulations imposed by my institution and its institutional review board in requesting these data.</li>
	<li>I will ensure that Investigators who utilize HCP data use appropriate administrative, physical and technical safeguards to prevent use or disclosure of the data other than as provided for by this Agreement.</li>
	<li>I will report any use or disclosure of the data not provided for by this Agreement of which I become aware within 15 days of becoming aware of such use or disclosure.</li>
</ol>
<p>If I publish abstracts using data from HCP, I agree to the following:</p>
<ol start="9">
	<li>I will cite HCP as the source of data and the HCP funding sources in the abstract as space allows in the acknowledgements.</li>
	<li>Group authorship of HCP will not be cited in the authorship line of the abstract.</li>
</ol>
<p>If I publish manuscripts using data from HCP, I agree to the following:</p>
<ol start="11">
	<li>I will acknowledge the HCP project as a source of data and include language similar to the following:</li>
	<ul>
		<li>Data collection and sharing for this project was provided by the MGH-UCLA Consortium of the Human Connectome Project (HCP; Principal Investigators: Bruce Rosen, M.D., Ph.D., Arthur W. Toga, Ph.D., Van J. Weeden, MD). HCP funding was provided by the National Institute of Dental and Craniofacial Research (NIDCR), the National Institute of Mental Health (NIMH), and the National Institute of Neurological Disorders and Stroke (NINDS). HCP data are disseminated by the Laboratory of Neuro Imaging at the University of California, Los Angeles.</li>
	</ul>
	<li>I will include language similar to the following in the methods section of my manuscripts in order to accurately acknowledge data gathering by the HCP investigators. Depending upon the length and focus of the article, it may be appropriate to include more or less than the example below, however, inclusion of some variation of the language shown below is mandatory.</li>
	<ul>
		<li>Data used in the preparation of this work were obtained from the Human Connectome Project (HCP) database (https://db.humanconnectome.org/). The MGH-UCLA consortium of the HCP project (Principal Investigators: Bruce Rosen, M.D., Ph.D., Martinos Center at Massachusetts General Hospital; Arthur W. Toga, Ph.D., University of California, Los Angeles, Van J. Weeden, MD, Martinos Center at Massachusetts General Hospital) is supported by the National Institute of Dental and Craniofacial Research (NIDCR), the National Institute of Mental Health (NIMH) and the National Institute of Neurological Disorders and Stroke (NINDS). HCP is the result of efforts of co-investigators from the University of California, Los Angeles, Martinos Center for Biomedical Imaging at Massachusetts General Hospital (MGH), Washington University in Saint Louis, and the University of Minnesota.</li>
	</ul>
	<li>I understand that failure to abide by these guidelines will result in termination of my privileges to access HCP data.</li>
</ol>
<p>v. 2012-Jan-5</p>