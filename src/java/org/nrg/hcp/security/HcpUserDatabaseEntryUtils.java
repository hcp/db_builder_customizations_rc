// Copyright 2012 Washington University School of Medicine All Rights Reserved
package org.nrg.hcp.security;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nrg.hcp.security.HcpLdapHelper.DataUseException;
import org.nrg.hcp.security.HcpLdapHelper.*;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.entities.XDATUserDetails;
import org.nrg.xdat.entities.XdatUserAuth;
import org.nrg.xdat.om.XdatUserGroupid;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.base.BaseXnatProjectdata;
import org.nrg.xdat.security.UserGroup;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.services.XdatUserAuthService;
import org.nrg.xdat.turbine.utils.PopulateItem;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFT;
import org.nrg.xft.XFTTable;
import org.nrg.xft.db.PoolDBUtils;
import org.nrg.xft.event.EventDetails;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.EventRequirementAbsent;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import java.util.*;

public class HcpUserDatabaseEntryUtils {

	static org.apache.log4j.Logger logger = Logger.getLogger(HcpUserDatabaseEntryUtils.class);

	protected static boolean removeUserFromXnatGroups(XDATUser user, String[] groupsArray) throws DataUseException {
		// Remove users project access
		boolean modified = false;
		projloop:
		for (String group : groupsArray) {
			for (String groupKey : user.getGroups().keySet()) {
				if (groupKey.equals(group)) {
					String projId = group.substring(0, group.lastIndexOf("_"));
					XnatProjectdata proj = XnatProjectdata.getXnatProjectdatasById(projId, user, false);
					try {
						// Create user object because user from getUsers method doesn't have xnat ID assigned
						EventDetails edet = EventUtils.newEventInstance(EventUtils.CATEGORY.PROJECT_ACCESS, EventUtils.TYPE.PROCESS, EventUtils.REMOVE_USER_FROM_PROJECT);
						final PersistentWorkflowI wrk=PersistentWorkflowUtils.buildOpenWorkflow(user, proj.getItem(), edet);
						EventMetaI c = wrk.buildEvent();

						if (removeGroupMember(group, proj, user, edet)) {
							PersistentWorkflowUtils.complete(wrk, c);
							user.initGroups();
							user.getGroups();
							logger.debug("Remove from project -- group=" + group + "  --  user=" + user.getLogin());
							modified = true;
						} else {
							PersistentWorkflowUtils.fail(wrk, c);
						}

					} catch (Exception e) {
						logger.error("ERROR:  Could not remove users from project " + proj.getId(),e);
					}
					continue projloop;
				}
			}
	    }
		return modified;
	}

	public static boolean addUserToXnatGroups(XDATUser user, String[] xnatGroups) throws DataUseException {
		boolean modified = false;

		for (String group : xnatGroups) {
			String projS = group.substring(0, group.lastIndexOf("_"));
			XnatProjectdata proj = XnatProjectdata.getXnatProjectdatasById(projS, user, false);

			try {
				final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, proj.getItem(),
						EventUtils.newEventInstance(EventUtils.CATEGORY.PROJECT_ACCESS,EventUtils.TYPE.PROCESS, EventUtils.ADD_USER_TO_PROJECT));
				EventMetaI c = wrk.buildEvent();

				logger.debug("Add to group =" + group + "  --  user=" + user.getLogin());
				PersistentWorkflowUtils.complete(wrk, c);

				if (addGroupMember(group, proj, user, null, true)) {
					modified = true;
					user.initGroups();
					user.getGroups();
				} else {
					PersistentWorkflowUtils.fail(wrk, c);
				}
			} catch (Exception e) {
				throw new DataUseException("ERROR:  Could not add user to group " + group, e);
			}
		}
		return modified;
	}

	// Allows a userid that doesn't normally have access to add their self to a group.
	// This seems preferable than acting as project owner (acquiring that username) and doing the add
    protected static boolean addGroupMember(String group_id, XnatProjectdata proj, XDATUser user, EventMetaI ci, boolean checkExisting) throws Exception {
		UserGroup existingProjGroup = user.getGroupByTag(proj.getId());

		if (existingProjGroup != null) {
			// Already in this group or a project owner, which we leave alone or has higher level access
			if (existingProjGroup.getId().equals(group_id) || existingProjGroup.getId().endsWith("_owner") || hasHigherLevelAccess(existingProjGroup.getId(), group_id))
				return false;

			// remove from other groups in project
			// Remove any admin defined group membership for this project, leaving the user in the last group in the access list
			// Assumes ldap config service has groups in superceding order, e.g., Restricted is later than Open for a project in the config
			// NOTE: This should be taken care of by a hasHigherLevelAccess() method
			EventDetails edet = EventUtils.newEventInstance(EventUtils.CATEGORY.PROJECT_ACCESS, EventUtils.TYPE.PROCESS, EventUtils.REMOVE_USER_FROM_PROJECT);
			removeGroupMember(existingProjGroup.getId(), proj, user, edet);
		}
        try {
            GrantedAuthority newAuth = new GrantedAuthorityImpl(group_id);
            ((XDATUserDetails) user).getAuthorities().add(newAuth);
            SecurityContext securityContext = SecurityContextHolder.getContext();
            Authentication oldAuth = securityContext.getAuthentication();
            Collection<GrantedAuthority> oldAuths =  oldAuth.getAuthorities();
            List<GrantedAuthority> newAuthList = new ArrayList<GrantedAuthority>();
            for(GrantedAuthority tempAuth : oldAuths){
                newAuthList.add(tempAuth);
            }
            newAuthList.add(newAuth);
            Authentication authentication = new UsernamePasswordAuthenticationToken(oldAuth.getPrincipal(), oldAuth.getCredentials(), newAuthList);
            securityContext.setAuthentication(authentication);
        }catch(Exception e){
            logger.error("Failed to add new user permission to user object in session",e);
        }

		// add to group_id, return true
		final String confirmquery = "SELECT * FROM xdat_user_groupid WHERE groupid='" + group_id + "' AND groups_groupid_xdat_user_xdat_user_id=" + user.getXdatUserId() + ";";
		XFTTable t = XFTTable.Execute(confirmquery, user.getDBName(), user.getUsername());
		if(t.size() == 0) {
			final XdatUserGroupid map = new XdatUserGroupid((UserI) user);
			map.setProperty(map.getXSIType() +".groups_groupid_xdat_user_xdat_user_id", user.getXdatUserId());
			map.setGroupid(group_id);
			SaveItemHelper.authorizedSave(map, user, false, false, ci);
			return true;
		}
		return false;
	}

	private static boolean hasHigherLevelAccess(String existingGroup, String newGroup) {
		// TODO - make more general
		if (existingGroup.toLowerCase().contains("restrict"))
			return true;
		return false;
	}

	// This is mostly the method from the BaseXnatProjectdata class, however it allows a userid that doesn't normally have access to remove
	// their self from a group.  This seems preferable than acting as project owner (acquiring that username) and doing the remove
	public static boolean removeGroupMember(String group_id, XnatProjectdata proj, XDATUser user, EventDetails edet) throws Exception{
//		String group_id = proj.getId() + "_" + BaseXnatProjectdata.COLLABORATOR_GROUP;

		final String confirmquery = "SELECT * FROM xdat_user_groupid WHERE groupid='" + group_id + "' AND groups_groupid_xdat_user_xdat_user_id=" + user.getXdatUserId() + ";";
		XFTTable t=XFTTable.Execute(confirmquery, user.getDBName(), user.getUsername());

		if(t.size()>0){
			final String query = "DELETE FROM xdat_user_groupid WHERE groupid='" + group_id + "' AND groups_groupid_xdat_user_xdat_user_id=" + user.getXdatUserId() + ";";

			PersistentWorkflowI wrk= PersistentWorkflowUtils.buildOpenWorkflow(user, user.getXSIType(), user.getXdatUserId().toString(), proj.getId(), edet);
			try {
				PoolDBUtils.ExecuteNonSelectQuery(query,proj.getItem().getDBName(), user.getLogin());
				PoolDBUtils.PerformUpdateTrigger(user.getItem(), user.getLogin());
				PersistentWorkflowUtils.complete(wrk, wrk.buildEvent());
				return true;
			} catch (Exception e) {
				PersistentWorkflowUtils.fail(wrk,wrk.buildEvent());
				throw e;
			}
		}
		return false;
	}

	/*
	 * NOTE:  The following methods are slightly modified methods from HibernateXdatUserAuthService
	 */

    public static XDATUserDetails handleNewLdapUser(final String id, String username, String email, String firstName, String lastName) {
        XDATUserDetails userDetails = null;

        try {
            logger.debug("Adding LDAP user '" + username + "' to database.");

            PersistentWorkflowI wrk = PersistentWorkflowUtils.buildAdminWorkflow(null, "xdat:user", username, EventUtils.newEventInstance(EventUtils.CATEGORY.SIDE_ADMIN, EventUtils.TYPE.WEB_FORM, "Created user from LDAP", null, null));

            try {
                XDATUser newUser = createXDATUser(username, email, firstName, lastName);

                SaveItemHelper.authorizedSave(newUser, XDAT.getUserDetails(), true, false, true, false, wrk.buildEvent());
                wrk.setId(newUser.getStringProperty("xdat_user_id"));

                XdatUserAuth newUserAuth = new XdatUserAuth(username, XdatUserAuthService.LDAP, id, username, true, 0);
                XDAT.getXdatUserAuthService().create(newUserAuth);

                // <HACK_ALERT>
                /*
                * We must save enabled flag to DB as true above, because the administrator code for enabling a user account does not flip this flag
                * (no time to mess with that now).
                * But for purposes of determining whether or not the user can log in right now after we've just created their account,
                * we use the system-wide auto-enable config setting.
                * Must clone a new object to return, rather than modifying the existing, so that Hibernate still saves the desired values to the DB.
                */
                newUserAuth = new XdatUserAuth(newUserAuth);
                newUserAuth.setEnabled(newUserAccountsAreAutoEnabled());
                // </HACK_ALERT>

                userDetails = new XDATUserDetails(newUser);
                userDetails.setAuthorization(newUserAuth);

                XDAT.setUserDetails(userDetails);

                PersistentWorkflowUtils.complete(wrk, wrk.buildEvent());
            } catch (Exception e) {
                logger.error(e);
                try {
                    PersistentWorkflowUtils.fail(wrk, wrk.buildEvent());
                } catch (Exception e1) {
                    logger.error(e);
                }
            }
        } catch (EventRequirementAbsent exception) {
            logger.error(exception);
            throw new UsernameNotFoundException(SpringSecurityMessageSource.getAccessor().getMessage("JdbcDaoImpl.notFound", new Object[]{username}, "Username {0} not found"));
        }

        return userDetails;
    }

    protected static XDATUser createXDATUser(final String username, final String email, final String firstName, final String lastName) throws Exception {
        Map<String, String> newUserProperties = new HashMap<String, String>();
        newUserProperties.put(XFT.PREFIX + ":user.login", username);
        newUserProperties.put(XFT.PREFIX + ":user.email", email);
        newUserProperties.put(XFT.PREFIX + ":user.primary_password", null);
        newUserProperties.put(XFT.PREFIX + ":user.firstname", firstName);
        newUserProperties.put(XFT.PREFIX + ":user.lastname", lastName);
        newUserProperties.put(XFT.PREFIX + ":user.primary_password.encrypt", "true");
        // TODO: Need to add ability to verify email address in cases where we may not completely trust LDAP repo.
        //newUserProperties.put(XFT.PREFIX + ":user.verified", "true");
        newUserProperties.put(XFT.PREFIX + ":user.verified", Boolean.toString(!XDAT.verificationOn()));
        newUserProperties.put(XFT.PREFIX + ":user.enabled", newUserAccountsAreAutoEnabled().toString());

        PopulateItem populater = new PopulateItem(newUserProperties, null, XFT.PREFIX + ":user", true);
        ItemI item = populater.getItem();

        item.setProperty("xdat:user.assigned_roles.assigned_role[0].role_name", "SiteUser");
        item.setProperty("xdat:user.assigned_roles.assigned_role[1].role_name", "DataManager");

        return new XDATUser(item);
    }

    public static Boolean newUserAccountsAreAutoEnabled() {
        return XFT.GetUserRegistration();
    }
	
	/*
	 * NOTE:  End HibernateXdatUserAuthService methods
	 */

	public static Boolean hasAccessLevel(XDATUser user, String proj, int tier) {
		if (user.isSiteAdmin()) { return true; }

		Boolean hasAccess = false;
		HcpLdapHelper ldap = null;
		try {
			ldap = HcpLdapHelper.getInstance();
		} catch(HcpLdapException e) {
			logger.error(e);
			return false;
		}
		Map<String, Object> userTiers = ldap.getUserTiers(user);
		Map<String, Object> projTiers = (HashMap)userTiers.get(proj);
		if (projTiers == null) { return false; } // Project not in tiers map

		Integer accessLevel = (Integer)projTiers.get("accessLevel");

		if (accessLevel >= tier)
			hasAccess = true;

		return hasAccess;
	}

    public static Integer getAccessLevel(XDATUser user, String proj) {
        HcpLdapHelper ldap = null;
        try {
            ldap = HcpLdapHelper.getInstance();
        } catch(HcpLdapException e) {
            logger.error(e);
            return 0;
        }
        Map<String, Object> userTiers = ldap.getUserTiers(user);
        Map<String, Object> projTiers = (HashMap)userTiers.get(proj);

		if (projTiers == null)
			return 0;
		else
        	return (Integer)projTiers.get("accessLevel");
    }
}