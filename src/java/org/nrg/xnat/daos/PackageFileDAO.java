package org.nrg.xnat.daos;

import java.util.Collection;
import java.util.List;

import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.xnat.entities.PackageFile;
import org.springframework.stereotype.Repository;

/**
 * Created with IntelliJ IDEA.
 * User: mmckay01
 * Date: 10/1/13
 * Time: 12:48 PM
 * To change this template use File | Settings | File Templates.
 */

@Repository
public class PackageFileDAO extends AbstractHibernateDAO<PackageFile> {
	
	public List getCountsAndSizes(final Collection<String> pathList,final Collection<String> subjectList) {
		if (subjectList != null && pathList != null) {
			return this.getSession().createQuery(
							"select sum(p.fileSize) as fileSize,sum(p.fileCount) as fileCount, count(p.subject) as subjectCount, p.packageName from PackageFile p where p.projectPath in (:paths) and p.subject in (:subjectIds) and p.enabled!=false " +
							"group by p.packageName"
					).setParameterList("paths",pathList).setParameterList("subjectIds", subjectList).list(); 
		} else if (subjectList == null && pathList == null) {
			return this.getSession().createQuery(
							"select sum(p.fileSize) as fileSize,sum(p.fileCount) as fileCount, count(p.subject) as subjectCount, p.packageName from PackageFile p where p.enabled!=false group by p.packageName"
					).list();
		} else if (subjectList != null) {
			return this.getSession().createQuery(
							"select sum(p.fileSize) as fileSize,sum(p.fileCount) as fileCount, count(p.subject) as subjectCount, p.packageName from PackageFile p where p.subject in (:subjectIds) and p.enabled!=false group by p.packageName"
					).setParameterList("subjectIds", subjectList).list(); 
		} else if (pathList != null) {
			return this.getSession().createQuery(
							"select sum(p.fileSize) as fileSize,sum(p.fileCount) as fileCount, count(p.subject) as subjectCount, p.packageName from PackageFile p where p.projectPath in (:paths) and p.enabled!=false group by p.packageName"
					).setParameterList("paths",pathList).setParameterList("subjectIds", subjectList).list(); 
		}
		return null;
	}
	
	public List getCountsAndSizes() {
		return getCountsAndSizes(null,null);
	}
	
	public List getCountByPackageIds(final Collection<String> pathList,final Collection<String> subjectList,final Collection<String> packageIdList) {
		if (subjectList != null && pathList != null) {
			return this.getSession().createQuery(
							"select count(p.subject) as subjectCount from PackageFile p where p.projectPath in (:paths) and p.subject in (:subjectIds) and p.packageName in (:packageIds) and p.enabled!=false"
					).setParameterList("paths",pathList).setParameterList("subjectIds", subjectList).setParameterList("packageIds",packageIdList).list(); 
		} else if (subjectList == null && pathList == null) {
			return this.getSession().createQuery(
							"select count(p.subject) as subjectCount from PackageFile p where p.packageName in (:packageIds) and p.enabled!=false"
					).setParameterList("packageIds", packageIdList).list();
		} else if (subjectList != null) {
			return this.getSession().createQuery(
							"select count(p.subject) as subjectCount from PackageFile p where p.subject in (:subjectIds) and p.packageName in (:packageIds) and p.enabled!=false"
					).setParameterList("subjectIds", subjectList).setParameterList("packageIds", packageIdList).list(); 
		} else if (pathList != null) {
			return this.getSession().createQuery(
							"select count(p.subject) as subjectCount from PackageFile p where p.projectPath in (:paths) and p.packageName in (:packageIds) and p.enabled!=false"
					).setParameterList("paths",pathList).setParameterList("subjectIds", subjectList).setParameterList("packageIds", packageIdList).list(); 
		}
		return null;
	}
	
	public List getCountByPackageIds(final Collection<String> packageIdList) {
		return getCountByPackageIds(null,null,packageIdList);
	}
	
}
