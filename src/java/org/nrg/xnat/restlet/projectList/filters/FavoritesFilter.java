package org.nrg.xnat.restlet.projectList.filters;

import java.util.Map;

import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.xdat.search.DisplayCriteria;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xnat.restlet.resources.ProjectListResource.ProjectListFilterI;
import org.restlet.data.Status;

public class FavoritesFilter implements ProjectListFilterI {

	@Override
	public void applyFilter(final Map<String,String> params, final CriteriaCollection andCC, final CriteriaCollection orCC, final XDATUser user) throws ServerException,ClientException{
		if(params!=null && params.containsKey("favorite")){
			try {
				DisplayCriteria dc = new DisplayCriteria();
				dc.setSearchFieldByDisplayField("xnat:projectData","PROJECT_FAV");
				dc.setComparisonType(" LIKE ");
				dc.setValue("% " + user.getLogin() + " %",false);
				orCC.addCriteria(dc);
			} catch (Throwable e) {
				throw new ClientException(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY, e);
			}
		}
	}

	@Override
	public boolean match(Map<String, String> params) {
		return (params!=null && params.containsKey("favorite"));
	}

}
