package org.nrg.xnat.restlet.extensions;

import org.restlet.data.Status;
import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xnat.download.RefreshPackagesRequest;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.quartz.JobExecutionException;

import org.restlet.Context;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.Destination;

/**
 * Created with IntelliJ IDEA.
 * User: mmckay01
 * Date: 10/31/13
 * Time: 1:11 PM
 * To change this template use File | Settings | File Templates.
 */
@XnatRestlet({"/services/refreshpackages/{PROJECT}","/services/refreshpackages/{PROJECT}/subjects/{SUBJECTS}","/services/refreshpackages","/services/refreshpackages/subjects/{SUBJECTS}"})
public class RefreshPackagesResource extends SecureResource {
    private static final String URL_ENCODING = "UTF-8";
    private String _subjects = null;
    private String _project = null;
    static Logger logger = Logger.getLogger(RefreshPackagesResource.class);

    public RefreshPackagesResource(final Context context, final Request request, final Response response) {
        super(context, request, response);
        try{
            _subjects = URLDecoder.decode(StringUtils.defaultString((String) getRequest().getAttributes().get("SUBJECTS")),
                URL_ENCODING);
            _project = URLDecoder.decode(StringUtils.defaultString((String) getRequest().getAttributes().get("PROJECT")),
                    URL_ENCODING);
        } catch (UnsupportedEncodingException exception) {
            logger.error("Encoding exception on authentication attempt", exception);
        }
    }

    @Override
    public boolean allowGet() { return true; }

    @Override
    public boolean allowPost() { return true; }

    @Override
    public boolean allowPut() { return false; }

    @Override
    public boolean allowDelete() { return false; }

    @Override
    public void handleGet() {
        handlePost();
    }

    @Override
    public void handlePost() {
        if(user.isSiteAdmin()){
            if(!(StringUtils.isBlank(_project))){
                if (StringUtils.isBlank(_subjects)) {
                    try {
                        final RefreshPackagesRequest refreshPackagesRequest = new RefreshPackagesRequest(_project);
                        sendRefreshPackagesRequest(refreshPackagesRequest);
                        this.getResponse().setStatus(Status.SUCCESS_OK, "Your refresh packages request has been successfully queued.");
                    } catch (Exception e) {
                        logger.error("Exception updating the package information table.", e);
                        this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, "Exception updating the package information table.");
                    }
                }
                else{
                    //Only refresh specified subjects
                    final String[] subjectArray = _subjects.split(",");
                    try{
                        for(String subject : subjectArray){
                            if(subject.matches("[A-Za-z0-9]+")){
                                final RefreshPackagesRequest refreshPackagesRequest = new RefreshPackagesRequest(_project);
                                refreshPackagesRequest.setSubDir(subject);
                                sendRefreshPackagesRequest(refreshPackagesRequest);
                            }
                        }
                        this.getResponse().setStatus(Status.SUCCESS_OK, "Your refresh packages requests have been successfully queued.");
                    }
                    catch (Exception e) {
                        logger.error("Exception updating the package information table for subjects: "+_subjects+".", e);
                        this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, "Exception updating the package information table for subjects: "+_subjects+"." );
                    }
                }
            }
            else{
                logger.error("No project specified. Cannot update the package information table for subjects: "+_subjects+".");
                this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, "No project specified. Cannot update the package information table for subjects: "+_subjects+"." );
            }
        }
        else{
            logger.warn("User does not have permission to refresh package data.");
            this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN, "User does not have permission to refresh package data.");
        }
        return;
    }

    private void sendRefreshPackagesRequest(final RefreshPackagesRequest refreshPackagesRequest) {
        final Destination destination = XDAT.getContextService().getBean("refreshPackagesRequest", Destination.class);
        final JmsTemplate jmsTemplate = XDAT.getContextService().getBean(JmsTemplate.class);
        jmsTemplate.convertAndSend(destination, refreshPackagesRequest);
    }

}
