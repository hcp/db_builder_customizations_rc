package org.nrg.xnat.restlet.extensions;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import org.nrg.xdat.om.XnatProjectdata;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.bean.XdatCriteriaBean;
import org.nrg.xdat.bean.XdatCriteriaSetBean;
import org.nrg.xdat.bean.XdatSearchBean;
import org.nrg.xdat.bean.XdatSearchFieldBean;
import org.nrg.xdat.bean.XdatStoredSearchBean;
import org.nrg.xnat.entities.datadictionary.Attribute;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.representations.BeanRepresentation;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.services.datadictionary.DataDictionaryService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.resource.Representation;
import org.restlet.resource.Variant;

import javax.servlet.http.HttpSession;

/**
 * @author tim@deck5consulting.com
 * @since 2/22/13
 *
 * Restlet used to build search xmls based on the data dictionary contents
 */
@XnatRestlet("/services/search/ddict/{CATEGORIES}")
public class DictionaryBasedSearch  extends SecureResource {
	static org.apache.log4j.Logger logger = Logger.getLogger(DictionaryBasedSearch.class);
	private final DataDictionaryService _service;
	private HttpSession httpSession;
	private String categoryS;
    private String restrictedProject;
	/**
	 * @param context standard
	 * @param request standard
	 * @param response standard
	 */
	public DictionaryBasedSearch(Context context, Request request, Response response) {
		super(context, request, response);
		
		//make sure we have access
		_service = XDAT.getContextService().getBean(DataDictionaryService.class);
		httpSession = getHttpSession();

        //Store the restricted project that the data dictionary information should be obtained from if this is a subject key project. Eventually this should be the umbrella project obtained from the project object (and this could be obtained in getRepresentation instead).
        //restrictedProject = StringUtils.trimToEmpty(XDAT.getConfigService().getConfigContents("dashboard", "project"))+"_RST";

		try {
			categoryS=URLDecoder.decode((String)getParameter(request, "CATEGORIES"),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("",e);
		}
		
		if(_service!=null && categoryS!=null){
			this.getVariants().add(new Variant(MediaType.TEXT_XML));
		}
	}
	
	/* (non-Javadoc)
	 * @see org.restlet.resource.Resource#getRepresentation(org.restlet.resource.Variant)
	 */
	@Override
	public Representation getRepresentation(Variant variant) {
		final MediaType mt = overrideVariant(variant);

		String[] categories={};
		String proj = this.getQueryVariable("project");
		XnatProjectdata project = XnatProjectdata.getProjectByIDorAlias(proj, user, false);
		restrictedProject = project.getRootProject().getId();
		int tier = (Integer) httpSession.getAttribute("tier");

		if(StringUtils.isNotEmpty(categoryS)){
			categories=categoryS.split("[|]");
		}
		
		//initialize search
		final XdatStoredSearchBean search = new XdatStoredSearchBean();
		search.setRootElementName("xnat:subjectData");
		search.setDescription(categoryS);
		search.setBriefDescription(categoryS);
		
		int count=0;

        if((project!=null) && (project.getFieldByName("connectometype")!=null) && (project.getFieldByName("connectometype").equals("subjectKey"))){
            //add all fields from all columns visible=false unless the categories match requested ones
            for(final String category:_service.getCategories(restrictedProject, tier)){
                for(final String assessment:_service.getAssessments(category, restrictedProject, tier)){
                    for(final Attribute attribute:_service.getAttributes(category, assessment, restrictedProject, tier)){
                        final XdatSearchFieldBean column=new XdatSearchFieldBean();
                        column.setElementName(attribute.getXsiType());
                        column.setFieldId(attribute.getFieldId());
                        column.setHeader(attribute.getColumnHeader());
                        column.setSequence(count++);
                        if(!ArrayUtils.contains(categories, category) && !attribute.getSystemId().equals("xnat:subjectData/subject_label")){
                            column.setVisible("false");
                        }
                        search.addSearchField(column);
                        if(count==1){
                            //adding subject_key column
                            final XdatSearchFieldBean codedIdColumn=new XdatSearchFieldBean();
                            codedIdColumn.setElementName("xnat:subjectData");
                            codedIdColumn.setFieldId("SUB_PROJECT_CODED_ID");
                            codedIdColumn.setValue(proj);
                            codedIdColumn.setHeader("Coded ID");
                            codedIdColumn.setSequence(count++);
                            search.addSearchField(codedIdColumn);
                        }
                    }
                }
            }
        }
        else{
            //add all fields from all columns visible=false unless the categories match requested ones
            for(final String category:_service.getCategories(proj, tier)){
                for(final String assessment:_service.getAssessments(category, proj, tier)){
                    for(final Attribute attribute:_service.getAttributes(category, assessment, proj, tier)){
                        final XdatSearchFieldBean column=new XdatSearchFieldBean();
                        column.setElementName(attribute.getXsiType());
                        column.setFieldId(attribute.getFieldId());
                        column.setHeader(attribute.getColumnHeader());
                        column.setSequence(count++);
                        if(!ArrayUtils.contains(categories, category) && !attribute.getSystemId().equals("xnat:subjectData/subject_label")){
                            column.setVisible("false");
                        }
                        search.addSearchField(column);
                    }
                }
            }
        }
        /** COMMENTED OUT:  2016/11/30.  I don't think this is needed anymore and we need to handle MR Sessions differently now that we have 7T data
		//add MR Columns, if requested.  This is temporarily hard coded because the data dictionary doesn't currently support unfilterable columns yet.
		if(ArrayUtils.contains(categories, "MR Sessions")){
			String[][] mr_fields={
					{"LABEL","Label"},
					{"SCANNER","Scanner"},
					{"MR_SCAN_COUNT_AGG","Scans"}
			};
			
			for(String[] field:mr_fields){
				final XdatSearchFieldBean column2=new XdatSearchFieldBean();
				column2.setElementName("xnat:mrSessionData");
				column2.setFieldId(field[0]);
				column2.setHeader(field[1]);
				column2.setSequence(count++);
				search.addSearchField(column2);
			}
			
			search.setRootElementName("xnat:mrSessionData");
		}
		*/
		
		//add constraints for project if requested
		if(proj!=null){
			final XdatCriteriaSetBean cs= new XdatCriteriaSetBean();
			cs.setMethod("OR");
			
			for(final String p: org.springframework.util.StringUtils.commaDelimitedListToSet(proj)){
				XdatCriteriaBean c=new XdatCriteriaBean();
				c.setSchemaField(search.getRootElementName()+"/project");
				c.setComparisonType("=");
				c.setValue(p);
				cs.addCriteria(c);

				c=new XdatCriteriaBean();
				c.setSchemaField(search.getRootElementName()+"/sharing/share/project");
				c.setComparisonType("=");
				c.setValue(p);
				cs.addCriteria(c);

                if((project.getFieldByName("connectometype")!=null) && (project.getFieldByName("connectometype").equals("subjectKey"))){
                    c=new XdatCriteriaBean();
                    c.setSchemaField("xnat:subjectData.SUB_PROJECT_CODED_ID=" + p);
                    c.setComparisonType(" IS NOT ");
                    c.setValue(" NULL ");
                    cs.addCriteria(c);
                }
			}
			
			search.addSearchWhere(cs);

		}
		
		return new BeanRepresentation(search,mt);
	}
}
