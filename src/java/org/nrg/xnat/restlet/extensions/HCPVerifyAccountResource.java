/**
 * Copyright 2011 Washington University
 */
package org.nrg.xnat.restlet.extensions;

import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.hcp.security.HcpLdapHelper;
import org.nrg.hcp.security.HcpLdapHelper.DataUseException;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xdat.entities.AliasToken;
import org.nrg.xdat.services.AliasTokenService;

import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.util.RequestUtil;
import org.restlet.Context;
import org.restlet.resource.Representation;
import org.restlet.resource.Resource;
import org.restlet.resource.StringRepresentation;
import org.restlet.data.Form;
import org.restlet.data.Parameter;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;

import com.google.common.collect.Maps;

@XnatRestlet(value = "/services/verifyaccount",secure = false)
public final class HCPVerifyAccountResource extends Resource {
    static Logger logger = Logger.getLogger(HCPVerifyAccountResource.class);

	public static final String EMAIL_KEY = "email";
	public static final String ACCOUNT_KEY = "account";
	public static final String USERNAME_KEY = "username";
	public static final String GROUP_KEY = "group";

	public HCPVerifyAccountResource(final Context context, final Request request, final Response response) {
		super(context, request, response);
	}

	@Override
	public boolean allowGet() { return true; }

	@Override
	public boolean allowPost() { return false; }

	@Override
	public boolean allowPut() { return false; }

	@Override
	public boolean allowDelete() { return false; }

	@Override
	public void handleGet() {
		
		String email = this.getQueryVariable(EMAIL_KEY);
		String username = (this.getQueryVariable(ACCOUNT_KEY)!=null) ? this.getQueryVariable(ACCOUNT_KEY) : this.getQueryVariable(USERNAME_KEY); 
		String group = this.getQueryVariable(GROUP_KEY);
		
		try {
			
			String rtn = null;
			if (email!=null) {
				rtn = HcpLdapHelper.GroupsByEmail(email,group);
			} else {
				rtn = HcpLdapHelper.GroupsByUsername(username,group);
			}
			if (rtn !=null) {
				this.returnString(rtn,Status.SUCCESS_OK);
			} else {
				this.returnString("User not found.",Status.CLIENT_ERROR_NOT_FOUND);
			}
		
			
		} catch (Exception e) {
			logger.error(e);
			this.returnString("Due to a technical difficulty, we are unable to process your request.  Please contact our technical support.",Status.SERVER_ERROR_INTERNAL);
			return;
			
		}
		
	}

	private void returnString(String string, Status status) {
		getResponse().setEntity(new StringRepresentation(string));
		getResponse().setStatus(status);
	}
	
	private Form f= null;
	
	private Form getQueryVariableForm(){
		if(f==null){
			f= getRequest().getResourceRef().getQueryAsForm();
		}
		return f;
	}

	public String getQueryVariable(String key){
		Form f = getQueryVariableForm();
		if (f != null && f.getValuesMap().containsKey(key)) {
			return TurbineUtils.escapeParam(f.getFirstValue(key));
		}
		return null;
	}

}

