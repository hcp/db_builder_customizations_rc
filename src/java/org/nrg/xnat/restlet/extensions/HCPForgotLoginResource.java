/**
 * Copyright 2011 Washington University
 */
package org.nrg.xnat.restlet.extensions;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.hcp.security.HcpLdapHelper;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xdat.turbine.utils.PopulateItem;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFT;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.EventRequirementAbsent;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xdat.entities.AliasToken;
import org.nrg.xdat.entities.XDATUserDetails;
import org.nrg.xdat.entities.XdatUserAuth;
import org.nrg.xdat.services.AliasTokenService;
import org.nrg.xdat.services.XdatUserAuthService;

import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.util.RequestUtil;
import org.restlet.Context;
import org.restlet.resource.Representation;
import org.restlet.resource.Resource;
import org.restlet.resource.StringRepresentation;
import org.restlet.data.Form;
import org.restlet.data.Parameter;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.google.common.collect.Maps;

/**
 * Computes an averaged connectome for subjects in the provided stored search.
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
@XnatRestlet(value = "/services/forgotlogin",secure = false)
public final class HCPForgotLoginResource extends Resource {
    static Logger logger = Logger.getLogger(HCPForgotLoginResource.class);

	public static final String EMAIL_KEY = "email";
	public static final String ACCOUNT_KEY = "account";

	public HCPForgotLoginResource(final Context context, final Request request, final Response response) {
		super(context, request, response);
	}

	@Override
	public boolean allowGet() { return true; }

	@Override
	public boolean allowPost() { return true; }

	@Override
	public boolean allowPut() { return false; }

	@Override
	public boolean allowDelete() { return false; }

	@Override
	public void handleGet() {
		handlePost();
	}

	@Override
	public void handlePost() {
		
		String email = this.getQueryVariable(EMAIL_KEY);
		String username = this.getQueryVariable(ACCOUNT_KEY);
		String subject = TurbineUtils.GetSystemName() + " Login Request";
		String admin = AdminUtils.getAdminEmailId();
		Map<String, Object> ldapAttrs = null;
		
		try {
		
			if (!StringUtils.isBlank(username)) {
				//check user
				ItemSearch search = new ItemSearch();
				search.setAllowMultiples(false);
				search.setElement("xdat:user");
				search.addCriteria("xdat:user.login",username);
	
				ItemI temp = search.exec().getFirst();
				
				if (temp==null){
					
					ldapAttrs = HcpLdapHelper.AttrsByUser(username);
					if (ldapAttrs == null || ldapAttrs.get("mail")==null) {
						this.returnString("Unknown username.", Status.CLIENT_ERROR_BAD_REQUEST);
						return;
					}
					String to = ldapAttrs.get("mail").toString();
					String fn = ldapAttrs.get("givenName")!=null ? ldapAttrs.get("givenName").toString() : "";
					String sn = ldapAttrs.get("sn")!=null ? ldapAttrs.get("sn").toString() : "";
					String url=TurbineUtils.GetFullServerPath() + "/app/action/XDATActionRouter/xdataction/MyXNAT";
					XDATUser user = handleNewLdapUser("ldap1", username, to, fn, sn);
					
					// This step should be unnecessary, but the user returned in some instances causes the getToken process to throw exceptions.  This 
					// prevents these issues.
					temp = search.exec().getFirst();
					user = new XDATUser(temp, false);
					
					AliasToken token = XDAT.getContextService().getBean(AliasTokenService.class).issueTokenForUser(user,true,null);
					sendResetMail(admin,to,fn,sn,subject,token);
					return;
					
				}
				XDATUser user = new XDATUser(temp, false);
				try {
					String to = user.getEmail();
					String url=TurbineUtils.GetFullServerPath() + "/app/action/XDATActionRouter/xdataction/MyXNAT";
					AliasToken token = XDAT.getContextService().getBean(AliasTokenService.class).issueTokenForUser(user,true,null);
					sendResetMail(admin,to,user.getFirstname(),user.getLastname(),subject,token);
					return;
					//data.setScreenTemplate("Login.vm");
					//data.setScreen("/app/template/Login.vm");
				} catch (MessagingException e) {
					logger.error("Unable to send mail",e);
					System.out.println("Error sending Email");
	
					this.returnString("Due to a technical difficulty, we are unable to send you the email containing your information.  Please contact our technical support.",Status.SERVER_ERROR_INTERNAL);
					return;
				}
			
			} else if (!StringUtils.isBlank(email)) {
				//check email
				ItemSearch search = new ItemSearch();
				search.setAllowMultiples(false);
				search.setElement("xdat:user");
				search.addCriteria("xdat:user.email",email);
	
				ItemI temp = search.exec().getFirst();
				
				if (temp==null){
					
					ldapAttrs = HcpLdapHelper.AttrsByEmail(email);
					if (ldapAttrs == null || ldapAttrs.get("cn")==null) {
						this.returnString("Unknown email address.",Status.CLIENT_ERROR_BAD_REQUEST);
						return;
					}
					username = ldapAttrs.get("cn").toString();
					sendUsernameMail(admin,email,username,subject);
					
				}else{
					XDATUser user = new XDATUser(temp, false);
					sendUsernameMail(admin,email,user.getUsername(),subject);
				}
				
			}else{
				this.returnString("Unknown/Missing username/email address.",Status.CLIENT_ERROR_BAD_REQUEST);
				return;
			}
				
		} catch (Exception e) {
			logger.error(e);
			this.returnString("Due to a technical difficulty, we are unable to process your request.  Please contact our technical support.",Status.SERVER_ERROR_INTERNAL);
			return;
		}
		
	}

	private void sendUsernameMail(String admin, String email, String username, String subject) {
		try {
			String url=TurbineUtils.GetFullServerPath() + "/app/template/Index.vm";
			String message = String.format(USERNAME_REQUEST, username, url, TurbineUtils.GetSystemName());
			XDAT.getMailService().sendHtmlMessage(admin, email, subject, message);
			this.returnString("The corresponding username for this email address has been emailed to your account.",Status.SUCCESS_OK);
			return;
		} catch (MessagingException exception) {
			logger.error(exception);
			this.returnString("Due to a technical difficulty, we are unable to send you the email containing your information.  Please contact our technical support.",Status.SERVER_ERROR_INTERNAL);
			return;
		}
	}

	private void sendResetMail(String admin, String to, String fn, String sn, String subject, AliasToken token) throws MessagingException {
		// TODO Auto-generated method stub
		String text = "Dear " + fn + " " + sn + ",<br/>\r\n" + "Please click this link to reset your password: " + TurbineUtils.GetFullServerPath() + "/app/template/ChangePassword.vm?a=" + token.getAlias() + "&s=" + token.getSecret() + "<br/>\r\nThis link will expire in 24 hours.";
		XDAT.getMailService().sendHtmlMessage(admin, to, subject, text);
		this.returnString("You have been sent an email with a link to reset your password. Please check your email.",Status.SUCCESS_OK);
	}

	// TODO: This should be converted to use a Velocity template or property in
	// a resource bundle.
	private static final String USERNAME_REQUEST = "<html><body>\nYou requested your username, which is: %s\n<br><br><br>Please login to the site for additional user information <a href=\"%s\">%s</a>.\n</body></html>";
	private static final String PASSWORD_RESET = "<html><body>\nYour password has been reset to:<br>%s\n<br><br><br>Please login to the site and create a new password in the <a href=\"%s\">account settings</a>.\n</body></html>";

	private void returnString(String string, Status status) {
		getResponse().setEntity(new StringRepresentation(string));
		getResponse().setStatus(status);
	}
	
	private Form f= null;
	
	private Form getQueryVariableForm(){
		if(f==null){
			f= getRequest().getResourceRef().getQueryAsForm();
		}
		return f;
	}

	public String getQueryVariable(String key){
		Form f = getQueryVariableForm();
		if (f != null && f.getValuesMap().containsKey(key)) {
			return TurbineUtils.escapeParam(f.getFirstValue(key));
		}
		return null;
	}
	
	/*
	 * NOTE:  The following methods are slightly modified methods from HibernateXdatUserAuthService
	 */

    private XDATUserDetails handleNewLdapUser(final String id, String username, String email, String firstName, String lastName) {
        XDATUserDetails userDetails = null;

        try {
            logger.debug("Adding LDAP user '" + username + "' to database.");

            PersistentWorkflowI wrk = PersistentWorkflowUtils.buildAdminWorkflow(null, "xdat:user", username, EventUtils.newEventInstance(EventUtils.CATEGORY.SIDE_ADMIN, EventUtils.TYPE.WEB_FORM, "Created user from LDAP", null, null));

            try {
                XDATUser newUser = createXDATUser(username, email, firstName, lastName);

                SaveItemHelper.authorizedSave(newUser, XDAT.getUserDetails(), true, false, true, false, wrk.buildEvent());
                wrk.setId(newUser.getStringProperty("xdat_user_id"));

                XdatUserAuth newUserAuth = new XdatUserAuth(username, XdatUserAuthService.LDAP, id, username, true, 0);
                XDAT.getXdatUserAuthService().create(newUserAuth);

                // <HACK_ALERT>
                /*
                * We must save enabled flag to DB as true above, because the administrator code for enabling a user account does not flip this flag
                * (no time to mess with that now).
                * But for purposes of determining whether or not the user can log in right now after we've just created their account,
                * we use the system-wide auto-enable config setting.
                * Must clone a new object to return, rather than modifying the existing, so that Hibernate still saves the desired values to the DB.
                */
                newUserAuth = new XdatUserAuth(newUserAuth);
                newUserAuth.setEnabled(newUserAccountsAreAutoEnabled());
                // </HACK_ALERT>

                userDetails = new XDATUserDetails(newUser);
                userDetails.setAuthorization(newUserAuth);

                XDAT.setUserDetails(userDetails);

                PersistentWorkflowUtils.complete(wrk, wrk.buildEvent());
            } catch (Exception e) {
                logger.error(e);
                try {
                    PersistentWorkflowUtils.fail(wrk, wrk.buildEvent());
                } catch (Exception e1) {
                    logger.error(e);
                }
            }
        } catch (EventRequirementAbsent exception) {
            logger.error(exception);
            throw new UsernameNotFoundException(SpringSecurityMessageSource.getAccessor().getMessage("JdbcDaoImpl.notFound", new Object[]{username}, "Username {0} not found"));
        }

        return userDetails;
    }


    private XDATUser createXDATUser(final String username, final String email, final String firstName, final String lastName) throws Exception {
        Map<String, String> newUserProperties = new HashMap<String, String>();
        newUserProperties.put(XFT.PREFIX + ":user.login", username);
        newUserProperties.put(XFT.PREFIX + ":user.email", email);
        newUserProperties.put(XFT.PREFIX + ":user.primary_password", null);
        newUserProperties.put(XFT.PREFIX + ":user.firstname", firstName);
        newUserProperties.put(XFT.PREFIX + ":user.lastname", lastName);
        newUserProperties.put(XFT.PREFIX + ":user.primary_password.encrypt", "true");
        // TODO: Need to add ability to verify email address in cases where we may not completely trust LDAP repo.
        newUserProperties.put(XFT.PREFIX + ":user.verified", "true");
        newUserProperties.put(XFT.PREFIX + ":user.enabled", newUserAccountsAreAutoEnabled().toString());

        PopulateItem populater = new PopulateItem(newUserProperties, null, XFT.PREFIX + ":user", true);
        ItemI item = populater.getItem();

        item.setProperty("xdat:user.assigned_roles.assigned_role[0].role_name", "SiteUser");
        item.setProperty("xdat:user.assigned_roles.assigned_role[1].role_name", "DataManager");

        return new XDATUser(item);
    }

    public Boolean newUserAccountsAreAutoEnabled() {
        return XFT.GetUserRegistration();
    }
	

}

