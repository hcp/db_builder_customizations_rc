package org.nrg.xnat.restlet.extensions;

import org.nrg.xdat.XDAT;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.StringRepresentation;
import org.apache.log4j.Logger;
import java.util.Arrays;
import java.util.List;

@XnatRestlet({"/services/sessionAttribute"})
public class SessionAttributeResource extends SecureResource {
    static Logger logger = Logger.getLogger(SessionAttributeResource.class);
    
	private static final String configtool = "userSettableSessionAttributes";
	private static final String configpath = "attributes";
	private final List<String> configList;
	private final String requestedAttr;

    public SessionAttributeResource(Context context, Request request, Response response) throws Exception {
        super(context, request, response);
		final String attributeConfig = XDAT.getConfigService().getConfigContents(configtool,configpath);
		if (attributeConfig==null) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  No user settable attributes have been configured");
		}
		configList = Arrays.asList(attributeConfig.replaceAll("[\n\\\\].*$","").split(","));
		requestedAttr = getQueryVariable("attribute");
		if (requestedAttr == null || !configList.contains(requestedAttr)) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  Attribute not specified or is not one of the allowed modifiable session attributes");
		}
    }

    @Override public boolean allowDelete() { return false; }
    @Override public boolean allowPut()    { return true; }
    @Override public boolean allowGet()    { return true; }
    @Override public boolean allowPost()   { return false;  }

    @Override public void handleGet(){
        final Object attr = getHttpSession().getAttribute(requestedAttr);
        if (attr == null) {
        	this.getResponse().setEntity(new StringRepresentation(""));
        	return;
        } else if (!(attr instanceof String)) {
        	getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  Attribute stored value is not a string.  Only String values are permitted.");
        	return;
        }
        this.getResponse().setEntity(new StringRepresentation((String)attr));
    }
    
    @Override public void handlePut(){
		final String requestedValue = getQueryVariable("value");
		if (requestedValue == null || requestedValue.length()==0) {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"ERROR:  Attribute value must be specified for PUT requests");
			return;
		}
		getHttpSession().setAttribute(requestedAttr, requestedValue);
       	getResponse().setStatus(Status.SUCCESS_OK);
    }
    
}
