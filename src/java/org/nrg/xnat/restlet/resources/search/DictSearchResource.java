// Copyright 2010 Washington University School of Medicine All Rights Reserved
package org.nrg.xnat.restlet.resources.search;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;

import org.apache.commons.lang.ArrayUtils;
import org.nrg.hcp.security.HcpUserDatabaseEntryUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.*;
import org.nrg.xft.db.MaterializedViewI;
import org.nrg.xnat.restlet.presentation.CSVPresenter;
import org.nrg.xdat.search.DisplaySearch;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFTTable;
import org.nrg.xft.db.MaterializedView;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.entities.datadictionary.Attribute;
import org.nrg.xnat.restlet.presentation.RESTHTMLPresenter;
import org.nrg.xnat.restlet.representations.ItemXMLRepresentation;
import org.nrg.xnat.restlet.resources.ItemResource;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.services.datadictionary.DataDictionaryService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.Variant;

import com.noelios.restlet.ext.servlet.ServletCall;

import javax.servlet.http.HttpSession;

public class DictSearchResource extends ItemResource {
    private final DataDictionaryService _service;
    private HttpSession httpSession;

    private String categoryS;

    public DictSearchResource(Context context, Request request,
                              Response response) {
        super(context, request, response);

        //make sure we have access
        _service= XDAT.getContextService().getBean(DataDictionaryService.class);
        httpSession = getHttpSession();

        try {
            categoryS= URLDecoder.decode((String) getParameter(request, "CATEGORIES"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error("",e);
        }

        if(_service!=null && categoryS!=null){
            this.getVariants().add(new Variant(MediaType.TEXT_XML));
        }
    }

    @Override
    public Representation getRepresentation(Variant variant) {
        MediaType mt = overrideVariant(variant);

        String tiersStr = null;
        if (containsQueryVariable("restricted")) {
            tiersStr = getQueryVariable("restricted");
        }

        String subjectLabels = null;
        if (containsQueryVariable("subject")) {
            subjectLabels = getQueryVariable("subject");
        } else if (httpSession.getAttribute("downloadSubjects") != null && !httpSession.getAttribute("downloadSubjects").equals("")) {
            subjectLabels = (String) httpSession.getAttribute("downloadSubjects");
        }

        XdatStoredSearch xss = createSearch(_service,categoryS,this.getQueryVariable("project"),subjectLabels,tiersStr);

        if(xss!=null){
            if(filepath !=null && filepath.startsWith("results")){
                try {
                    DisplaySearch ds=xss.getDisplaySearch(user);
                    String sortBy = this.getQueryVariable("sortBy");
                    String sortOrder = this.getQueryVariable("sortOrder");
                    if (sortBy != null){
                        ds.setSortBy(sortBy);
                        if(sortOrder != null)
                        {
                            ds.setSortOrder(sortOrder);
                        }
                    }

                    MaterializedViewI mv=null;

                    if(xss.getId()!=null && !xss.getId().equals("")){
                        mv = MaterializedView.getViewBySearchID(xss.getId(), user, MaterializedView.DEFAULT_MATERIALIZED_VIEW_SERVICE_CODE);
                    }

                    if(mv!=null && (xss.getId().startsWith("@") || this.isQueryVariableTrue("refresh"))){
                        mv.delete();
                        mv=null;
                    }

                    LinkedHashMap<String,Map<String,String>> cp = SearchResource.setColumnProperties(ds,user,this);
                    XFTTable table = null;
                    // Get the last tier in the tiers param list
                    int highestTier = Integer.parseInt(tiersStr.substring(tiersStr.length()-1));

                    if(mv!=null){
                        if (mt.equals(SecureResource.APPLICATION_XLIST)){
                            table=(XFTTable)ds.execute(new RESTHTMLPresenter(TurbineUtils.GetRelativePath(ServletCall.getRequest(this.getRequest())),this.getCurrentURI(),user,sortBy),user.getLogin());
                        }else if(mt!=null && (mt.equals(MediaType.APPLICATION_EXCEL) || mt.equals(SecureResource.TEXT_CSV)) && !this.isQueryVariableTrue("includeHiddenFields")){
                            String prefix = "";

                            if(tiersStr == null){
                                table=(XFTTable)ds.execute(new CSVPresenter(),user.getLogin());
                            }
                            else if(highestTier > 0){
                                table=(XFTTable)ds.execute(new CSVPresenter("true"),user.getLogin());
                                prefix = "RESTRICTED_";
                            }
                            else{
                                table=(XFTTable)ds.execute(new CSVPresenter("false"),user.getLogin());
                                prefix = "unrestricted_";
                            }
                            removeDelimitersPerRequestOrDefault(table);
                            //copied from CSVScreen
                            Date today = Calendar.getInstance(TimeZone.getDefault()).getTime();
                            String fileName=prefix+user.getUsername() + "_" + (today.getMonth() + 1) + "_" + today.getDate() + "_" + (today.getYear() + 1900) + "_" + today.getHours() + "_" + today.getMinutes() + "_" + today.getSeconds() + ".csv";

                            this.setContentDisposition(fileName, false);
                        }
                        else{
                            table=mv.getData(null, null, null);
                        }
                    }else{
                        ds.setPagingOn(false);
                        if (mt.equals(SecureResource.APPLICATION_XLIST)){
                            table=(XFTTable)ds.execute(new RESTHTMLPresenter(TurbineUtils.GetRelativePath(ServletCall.getRequest(this.getRequest())),this.getCurrentURI(),user,sortBy),user.getLogin());
                        }else if(mt!=null && (mt.equals(MediaType.APPLICATION_EXCEL) || mt.equals(SecureResource.TEXT_CSV)) && !this.isQueryVariableTrue("includeHiddenFields")){
                            String prefix = "";

                            if(tiersStr == null){
                                table=(XFTTable)ds.execute(new CSVPresenter(),user.getLogin());
                            }
                            else if(highestTier > 0){
                                table=(XFTTable)ds.execute(new CSVPresenter("true"),user.getLogin());
                                prefix = "RESTRICTED_";
                            }
                            else{
                                table=(XFTTable)ds.execute(new CSVPresenter("false"),user.getLogin());
                                prefix = "unrestricted_";
                            }
                            removeDelimitersPerRequestOrDefault(table);
                            //copied from CSVScreen
                            Date today = Calendar.getInstance(TimeZone.getDefault()).getTime();
                            String fileName=prefix+user.getUsername() + "_" + (today.getMonth() + 1) + "_" + today.getDate() + "_" + (today.getYear() + 1900) + "_" + today.getHours() + "_" + today.getMinutes() + "_" + today.getSeconds() + ".csv";

                            this.setContentDisposition(fileName, false);
                        }else{
                            table=(XFTTable)ds.execute(null,user.getLogin());
                        }

                    }

                    Hashtable<String,Object> tableParams=new Hashtable<String,Object>();
                    tableParams.put("totalRecords", table.getNumRows());

                    return this.representTable(table, mt, tableParams,cp);
                } catch (Exception e) {
                    e.printStackTrace();
                    this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
                }
            }else{
                if (mt.equals(MediaType.TEXT_XML)){
                    ItemXMLRepresentation rep= new ItemXMLRepresentation(xss.getItem(),MediaType.TEXT_XML);
                    rep.setAllowDBAccess(false);

                    return rep;
                }
            }
//	        else if (mt.equals(MediaType.APPLICATION_JSON)){
//				return new JSONTableRepresentation(item,params,MediaType.APPLICATION_JSON);
//			}else{
//				return new HTMLTableRepresentation(item,params,MediaType.TEXT_HTML);
//			}
        }

        return null;

    }

    private void removeDelimitersPerRequestOrDefault(XFTTable table) {
         // Let's default to removing commas here because that's the currently desired Connectome behavior.
         if(!this.isQueryVariableFalse("removeDelimitersFromFieldValues")){
           	 while (table.hasMoreRows()) {
	             Object[] cols = table.nextRow();
	             for (int i=0;i<cols.length;i++) {
	                 if (cols[i] instanceof String && ((String)cols[i]).indexOf(',')>=0) {
	                     cols[i]=((String)cols[i]).replaceAll(", ?"," ");
	                 }
	             }
	         }
         }
	}

	@Override
    public boolean allowDelete() {
        return false;
    }

    @Override
    public boolean allowPut() {
        return false;
    }

    public XdatStoredSearch createSearch(DataDictionaryService _service, String categoryS, String project, String subjects, String tiersStr) {
        String[] categories = {};

        if (org.apache.commons.lang.StringUtils.isNotEmpty(categoryS)) {
            categories = categoryS.split(",");
        }

        //initialize search
        final XdatStoredSearch search = new XdatStoredSearch((UserI) user);
        search.setRootElementName("xnat:subjectData");
        search.setDescription(categoryS);
        search.setBriefDescription(categoryS);

        // Get a list of tiers we want to display into an array
        String[] tiersStrArr = tiersStr.split(",");
        int[] tiersArr = new int[tiersStrArr.length];

        for (int i = 0; i < tiersStrArr.length; i++) {
            // Only add the tier param if the user has access to it
            if (HcpUserDatabaseEntryUtils.hasAccessLevel(user, project, tiersArr[i])) {
                tiersArr[i] = Integer.parseInt(tiersStrArr[i]);
            }
        }
        // Highest level of access should be the last in the array
        int highestTier = tiersArr[tiersArr.length-1];

        int count = 0;

        if(containsQueryVariable("csvName")) {
            try{

                String csvName = getQueryVariable("csvName");
                for (final Attribute attribute : _service.getAttributesInCsv(csvName)) {
                    final XdatSearchField column = new XdatSearchField((UserI) user);
                    column.setElementName(attribute.getXsiType());
                    column.setFieldId(attribute.getFieldId());
                    column.setHeader(attribute.getColumnHeader());
                    column.setSequence(count++);
                    if (!ArrayUtils.contains(categories, attribute.getCategory()) && !attribute.getSystemId().equals("xnat:subjectData/subject_label")) {
                        column.setVisible("false");
                    }
                    try {
                        search.setSearchField(column);
                    } catch (Exception e) {
                        logger.error("" + e);
                    }
                }
            } catch (Exception e) {
                logger.error("" + e);
            }
            count=count;
        }
        else{
            for (final String category : _service.getCategories(project, highestTier)) {
                for (final String assessment : _service.getAssessments(category, project, highestTier)) {
                    for (final Attribute attribute : _service.getAttributes(category, assessment, project, highestTier)) {
                        boolean includeAttribute = false;

                        for (int tier : tiersArr) {
                            if (attribute.getTier() == tier || attribute.getFullDisplayName().equals("Subject")) {
                                includeAttribute = true;
                                break;
                            }
                        }

                        if (includeAttribute) {
                            final XdatSearchField column = new XdatSearchField((UserI) user);
                            column.setElementName(attribute.getXsiType());
                            column.setFieldId(attribute.getFieldId());
                            column.setHeader(attribute.getColumnHeader());
                            column.setSequence(count++);
                            if (!ArrayUtils.contains(categories, category) && !attribute.getSystemId().equals("xnat:subjectData/subject_label")) {
                                column.setVisible("false");
                            }
                            try {
                                search.setSearchField(column);
                            } catch (Exception e) {
                                logger.error("" + e);
                            }
                        }
                    }
                }
            }
        }

        try{
            /** COMMENTED OUT:  2016/11/30.  I don't think this is needed anymore and we need to handle MR Sessions differently now that we have 7T data
            //add MR Columns, if requested.  This is temporarily hard coded because the data dictionary doesn't currently support unfilterable columns yet.
            if(ArrayUtils.contains(categories, "MR Sessions")){
                String[][] mr_fields={
                        {"LABEL","Label"},
                        {"SCANNER","Scanner"},
                        {"MR_SCAN_COUNT_AGG","Scans"}
                };

                for(String[] field:mr_fields){
                    final XdatSearchField column=new XdatSearchField((UserI)user);
                    column.setElementName("xnat:mrSessionData");
                    column.setFieldId(field[0]);
                    column.setHeader(field[1]);
                    column.setSequence(count++);

                    search.setSearchField(column);
                }

                search.setRootElementName("xnat:mrSessionData");
            }
            */
            final XdatCriteriaSet pcs= new XdatCriteriaSet((UserI)user);
            pcs.setMethod("OR");

            final XdatCriteriaSet scs= new XdatCriteriaSet((UserI)user);
            scs.setMethod("OR");

            //add constraints for project if requested
            if(project!=null){

                for(final String p: org.springframework.util.StringUtils.commaDelimitedListToSet(project)){
                    XdatCriteria c=new XdatCriteria((UserI)user);
                    c.setSchemaField(search.getRootElementName()+"/project");
                    c.setComparisonType("=");
                    c.setValue(p);
                    pcs.setCriteria(c);

                    c=new XdatCriteria((UserI)user);
                    c.setSchemaField(search.getRootElementName()+"/sharing/share/project");
                    c.setComparisonType("=");
                    c.setValue(p);
                    pcs.setCriteria(c);
                }
                if(subjects ==null){
                    search.setSearchWhere(pcs);
                }
            }
            //add constraints for subjects if requested
            if(subjects !=null){
                for(final String s: org.springframework.util.StringUtils.commaDelimitedListToSet(subjects)){
                    XdatCriteria c=new XdatCriteria((UserI)user);
                    c.setSchemaField(search.getRootElementName()+"/LABEL");
                    c.setComparisonType("=");
                    c.setValue(s);
                    scs.setCriteria(c);
                }
                if(project==null){
                    search.setSearchWhere(scs);
                }
                else{//we need to combine the two criteria sets
                    final XdatCriteriaSet tcs= new XdatCriteriaSet((UserI)user);
                    tcs.setMethod("AND");
                    tcs.setChildSet(pcs);
                    tcs.setChildSet(scs);
                    search.setSearchWhere(tcs);
                }
            }

        }
        catch(Exception e){
            logger.error(""+e);
        }
        return search;
    }
}
