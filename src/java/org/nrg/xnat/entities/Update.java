package org.nrg.xnat.entities;

/**
 * Created by mmckay01 on 1/29/14.
 */

import org.apache.commons.lang.StringUtils;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"project","date","update","link"}))
public class Update extends AbstractHibernateEntity{
    private String project;
    private String date;
    private String update;
    private String link;

    public Update() {
    }

    public Update(String project, String date, String update, String link) {
        this.project = project;
        this.date = date;
        this.update = update;
        this.link = link;
    }

    public String getDate(){
        return date;
    }

    public String getUpdate(){
        return update;
    }

    public String getLink(){
        return link;
    }

    public String getProject(){
        return project;
    }

    public void setProject(String project){
        this.project = project;
    }

    public void setDate(String date){
        this.date = date;
    }

    public void setUpdate(String update){
        this.update = update;
    }
    public void setLink(String link){
        this.link = link;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null || !(object instanceof Update)) {
            return false;
        }
        Update other = (Update) object;
        return           (StringUtils.equals(getDate(),other.getDate()) &&
                StringUtils.equals(getUpdate(),other.getUpdate()) &&
                StringUtils.equals(getLink(),other.getLink()) &&
                StringUtils.equals(getProject(),other.getProject()));
    }


}
