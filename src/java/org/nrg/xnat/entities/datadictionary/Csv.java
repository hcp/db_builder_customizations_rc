/*
 * Category
 * Copyright (c) 2013. Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 */

package org.nrg.xnat.entities.datadictionary;

import java.util.List;

/**
 * Represents a category.
 *
 * @author rherri01
 * @since 2/18/13
 */
public class Csv extends Node {
    public Csv() {
        super();
    }

    public Csv(final int position, final String name, final String columnHeader, final String description, final int tier, final List<String> attributes, final List<String> projects) {
        super(position, name, columnHeader, description, tier, projects);
        setAttributes(attributes);
    }

    public List<String> getAttributes() {
        return _attributes;
    }

    public void setAttributes(final List<String> attributes) {
        _attributes = attributes;
    }

    private static final long serialVersionUID = -6450976298430095961L;

    private List<String> _attributes;
}
