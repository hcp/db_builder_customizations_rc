/*
 * Assessment
 * Copyright (c) 2013. Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 */

package org.nrg.xnat.entities.datadictionary;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Assessment
 *
 * @author rherri01
 * @since 2/18/13
 */
public class Assessment extends Node {
    public Assessment() {
        super();
    }

    public Assessment(final int position, final String name, final String category, final String columnHeader, final String description, final int tier, final List<String> projects) {
        super(position, name, columnHeader, description, tier, projects);
        setCategory(category);
    }

    public String getCategory() {
        return _category;
    }

    public void setCategory(final String category) {
        _category = category;
    }

    private static final long serialVersionUID = -6450976298430095961L;

    private String _category;
}
