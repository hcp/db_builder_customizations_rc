package org.nrg.xnat.entities;

public class PackageInfo {

	private final Long fileSize;
	private final Long fileCount;
	private final Long subjectCount;

	public PackageInfo(final Long fileSize,Long fileCount,Long subjectCount) {
		this.fileSize = fileSize;
		this.fileCount = fileCount;
		this.subjectCount = subjectCount;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public Long getFileCount() {
		return fileCount;
	}

	public Long getSubjectCount() {
		return subjectCount;
	}

}
