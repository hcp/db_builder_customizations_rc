package org.nrg.xnat.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang.StringUtils;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;
import org.hibernate.annotations.Index;

@Entity

@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"filePath"}))
public class PackageFile extends AbstractHibernateEntity{

	@Index(name="subject_idx")
    private String subject;
	@Index(name="package_name_idx")
    private String packageName;
    private String projectPath;
    private String filePath;
    private long fileSize;
    private int fileCount;
    private long lastModified;

    public PackageFile() {
    }

    public PackageFile(String newPath) {
        this.filePath = newPath;
    }

    public PackageFile(String newPath, long newSize, int newFileCount, long newLastModified) {
        this.filePath = newPath;
        this.fileSize = newSize;
        this.fileCount = newFileCount;
        this.lastModified = newLastModified;
    }

    public PackageFile(String newProjectPath, String newSubject, String newPackage, String newPath, long newSize, int newFileCount, long newLastModified) {
        this.projectPath = newProjectPath;
        this.subject = newSubject;
        this.packageName = newPackage;
        this.filePath = newPath;
        this.fileSize = newSize;
        this.fileCount = newFileCount;
        this.lastModified = newLastModified;
    }

    public String getSubject(){
        return subject;
    }

    public String getPackageName(){
        return packageName;
    }

    public String getProjectPath(){
        return projectPath;
    }

    public String getFilePath(){
        return filePath;
    }

    public long getFileSize(){
        return fileSize;
    }

    public int getFileCount(){
        return fileCount;
    }

    public long getLastModified(){
        return lastModified;
    }

    public void setSubject(String subject){
        this.subject=subject;
    }

    public void setPackageName(String packageName){
        this.packageName=packageName;
    }

    public void setProjectPath(String projectPath){
        this.projectPath=projectPath;
    }

    public void setFilePath(String filePath){
        this.filePath=filePath;
    }

    public void setFileSize(long fileSize){
        this.fileSize = fileSize;
    }

    public void setFileCount(int fileCount){
        this.fileCount = fileCount;
    }

    public void setLastModified(long lastModified){
        this.lastModified = lastModified;
    }


    @Override
    public boolean equals(Object object) {
        if (object == null || !(object instanceof PackageFile)) {
            return false;
        }
        PackageFile other = (PackageFile) object;
        return           (StringUtils.equals(getFilePath(), other.getFilePath()) &&
                getFileSize()==other.getFileSize() &&
                getFileCount()==other.getFileCount() &&
                getLastModified()==other.getLastModified());
    }


}
