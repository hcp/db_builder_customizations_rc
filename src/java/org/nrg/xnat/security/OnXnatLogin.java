package org.nrg.xnat.security;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nrg.xdat.XDAT;
import org.nrg.hcp.security.HcpLdapHelper;
import org.nrg.xdat.entities.XDATUserDetails;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.turbine.utils.AccessLogger;
import org.nrg.xft.XFTItem;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.entities.AmazonData;
import org.nrg.xnat.services.amazonData.AmazonDataService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.*;

public class OnXnatLogin extends SavedRequestAwareAuthenticationSuccessHandler {

    protected final Log logger = LogFactory.getLog(getClass());

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {

        if (logger.isDebugEnabled()) {
            logger.debug("Request is to process authentication");
        }

        XDATUserDetails user = null;

        try {
            SecurityContext securityContext = SecurityContextHolder.getContext();
            Object principal = securityContext.getAuthentication().getPrincipal();

            if (principal instanceof XDATUserDetails) {
                user = (XDATUserDetails) principal;
            } else if (principal instanceof String) {
                user = new XDATUserDetails((String) principal);
            }
        }
        catch (Exception e) {
            logger.error(e);
        }

        try { // Set user's access based on DUT acceptance
            HcpLdapHelper ldap = HcpLdapHelper.getInstance();
            HcpLdapHelper.DUTStatus dut = ldap.updateAllGroupPermissions(user);

            request.getSession().setAttribute("userTiers", ldap.getUserTiers(user));
            request.getSession().setAttribute("tier", 0);

            if(dut==HcpLdapHelper.DUTStatus.MOD){
                user = new XDATUserDetails(user.getUsername());
            }
        }
        catch (Exception e) {
            logger.error(e);
        }

        String defaultProject = null;
        try { // Setup other session variables
            defaultProject = StringUtils.trimToEmpty(XDAT.getConfigService().getConfigContents("dashboard", "project"));
            request.getSession().setAttribute("project", defaultProject);

            request.getSession().setAttribute("defaultProject", defaultProject);
            request.getSession().setAttribute("defaultRestrictedProject", defaultProject + "_RST");
            request.getSession().setAttribute("tier", 0);

            request.getSession().setAttribute("XNAT_CSRF", UUID.randomUUID().toString());
            request.getSession().setAttribute("user", user);
            java.util.Date today = java.util.Calendar.getInstance(java.util.TimeZone.getDefault()).getTime();
            XFTItem item = XFTItem.NewItem("xdat:user_login",user);
            item.setProperty("xdat:user_login.user_xdat_user_id", user.getID());
            item.setProperty("xdat:user_login.login_date",today);
            item.setProperty("xdat:user_login.ip_address", AccessLogger.GetRequestIp(request));
            item.setProperty("xdat:user_login.session_id", request.getSession().getId());
            SaveItemHelper.authorizedSave(item,null,true,false, EventUtils.DEFAULT_EVENT(user,null));

            List<AmazonData> dataInTable = XDAT.getContextService().getBean(AmazonDataService.class).getAmazonDataForUser(user.getUsername());
            if(dataInTable==null || dataInTable.size()<1){
                request.getSession().setAttribute("hasAws", false);
            }
            else{
                request.getSession().setAttribute("hasAws", true);
                request.getSession().setAttribute("awsAccessKey",((AmazonData)dataInTable.get(0)).getAccessKey());
            }
        }
        catch (Exception e) {
            logger.error(e);
        }

        try { // Get dashboard project and subject key parent lists from config
            String config = XDAT.getConfigService().getConfigContents("dashboard", "projects");
            ArrayList<String> projectsList = new ArrayList<String>();

            if (config == null) {
                logger.error("No dashboard projects list found. Upload a list using the XNAT config service. Setting to defaultProject.");
                projectsList.add(defaultProject);
            } else {
                config = config.replaceAll("\\s", "");
                for (String proj : config.split(",")) {
                    projectsList.add(proj);
                }
            }
            request.getSession().setAttribute("dashboardProjects", projectsList);

            // Subject Key Umbrella Project List
            config = XDAT.getConfigService().getConfigContents("subjectkeys", "umbrellas");
            ArrayList<String> subjectKeyUmbrellas = new ArrayList<String>();

            if (config == null) {
                logger.error("No Subject Keys umbrella list found. Upload a list using the XNAT config service.");
                subjectKeyUmbrellas.add(defaultProject);
            } else {
                config = config.replaceAll("\\s", "");
                for (String umbrella : config.split(",")) {
                    subjectKeyUmbrellas.add(umbrella);
                }
            }
            request.getSession().setAttribute("subjectKeyUmbrellas", subjectKeyUmbrellas);


            // Build project id->shortname map
            Map<String, String> projDisplayNames = new HashMap<String, String>();

            for (String projId : projectsList) {
                XnatProjectdata p = XnatProjectdata.getProjectByIDorAlias(projId, user, false);
                projDisplayNames.put(projId, p.getDisplayName());
            }
            for (String projId : subjectKeyUmbrellas) {
                XnatProjectdata p = XnatProjectdata.getProjectByIDorAlias(projId, user, false);
                projDisplayNames.put(projId, p.getDisplayName());
            }
            request.getSession().setAttribute("projDisplayNames", projDisplayNames);
        } catch (Exception e) {
            logger.error(e);
        }
        super.onAuthenticationSuccess(request, response, authentication);
    }

    @Override
    protected String determineTargetUrl(HttpServletRequest request, HttpServletResponse response){
        String loginLanding = "/app/template/Index.vm?login=true";
        String url = getDefaultTargetUrl();
        if("/".equals(url)){
            setDefaultTargetUrl(loginLanding);
            return loginLanding;
        } else {
            return super.determineTargetUrl(request, response);
        }
    }

}
