package org.nrg.xnat.services.unrelatedsubjects;

import org.nrg.framework.services.NrgService;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.XFTTable;
import java.util.ArrayList;
import java.util.HashMap;
import org.nrg.xft.db.PoolDBUtils;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * User: mmckay01
 * Date: 8/9/13
 * Time: 1:51 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class UnrelatedSubjectsService implements NrgService {
    public static String SERVICE_NAME = "UnrelatedSubjectsService";
    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(UnrelatedSubjectsService.class);

    private static final int maxFamilySize = 4;
    private ArrayList<String[]> subjectLists;//Maximally large unrelated subject lists. subjectLists[0] is first family
    private ArrayList<String> subjectsInSubjectsList;
    //private HashMap<String, ArrayList<String>> siblings;//A map of subject IDs to the lists of subject IDs of their siblings

    public UnrelatedSubjectsService(){
        logger.debug("Creating unrelated subject service from the database");

        try{
            subjectsInSubjectsList = new ArrayList<String>();
            subjectLists = new ArrayList<String[]>();
            ArrayList<ArrayList<String>> subjectIDsAndLabels = new ArrayList<ArrayList<String>>();

            subjectsInSubjectsList = new ArrayList<String>();
            String query = "SELECT id, label FROM xnat_subjectdata WHERE project='HCP_Subjects' ORDER BY label ASC";
            XFTTable table=XFTTable.Execute(query, PoolDBUtils.getDefaultDBName(), null);

            table.resetRowCursor();
            while (table.hasMoreRows())
            {
                Object[] row = table.nextRow();
                if (row[0]!=null && row[1]!=null)
                {
                    ArrayList<String> idAndLabel = new ArrayList<String>();
                    idAndLabel.add(row[0].toString());
                    idAndLabel.add(row[1].toString());

                    subjectIDsAndLabels.add(idAndLabel);
                }
            }

            for(ArrayList<String> idAndLabel : subjectIDsAndLabels) {

                //This currently does not get the right twin status. If we want to display twin status in the table, this query must be altered.
                String siblingQuery = "SELECT xnat_subjectdata.id, xnat_subjectdata.label,tier1.restricted_twinstatus FROM xnat_subjectdata LEFT JOIN hcp_restrictedtier1 AS tier1 ON tier1.id IN (SELECT id FROM xnat_subjectassessordata WHERE subject_id = 'ConnectomeDB_S00245') RIGHT JOIN (SELECT DISTINCT subject_id FROM xnat_subjectassessordata WHERE id in (SELECT id from hcp_restrictedtier1 WHERE restricted_motherid = (SELECT DISTINCT restricted_motherid FROM hcp_restrictedtier1 WHERE id in (SELECT id FROM xnat_subjectassessordata WHERE subject_id = '"+idAndLabel.get(0)+"')) AND restricted_fatherid = (SELECT DISTINCT restricted_fatherid FROM hcp_restrictedtier1 WHERE id in (SELECT id FROM xnat_subjectassessordata WHERE subject_id = '"+idAndLabel.get(0)+"'))) AND subject_id <> '"+idAndLabel.get(0)+"') AS siblings ON xnat_subjectdata.id = subject_id LEFT JOIN hcp_restrictedtier1 ON hcp_restrictedtier1.id=xnat_subjectdata.id ORDER BY xnat_subjectdata.label ASC";
                XFTTable siblingTable=XFTTable.Execute(siblingQuery, PoolDBUtils.getDefaultDBName(), null);
                siblingTable.resetRowCursor();
                String[] siblingArray = new String[maxFamilySize];
                ArrayList<String> siblingArrayList = new ArrayList<String>();
                siblingArray[0]="<a href=\"https://hcpx-dev-mckay1.nrg.mir/app/action/DisplayItemAction/search_value/"+idAndLabel.get(0).toString()+"/search_element/xnat:subjectData/search_field/xnat:subjectData.ID/project/HCP_SUBJECTS/\" target=\"_blank\">"+idAndLabel.get(1).toString()+"</a>";
                siblingArrayList.add(idAndLabel.get(1));
                int siblingCount = 1;
                while (siblingTable.hasMoreRows() && siblingCount<maxFamilySize)
                {
                    Object[] siblingRow = siblingTable.nextRow();
                    if (siblingRow[1]!=null)
                    {
                        siblingArray[siblingCount]="<a href=\"https://hcpx-dev-mckay1.nrg.mir/app/action/DisplayItemAction/search_value/"+siblingRow[0].toString()+"/search_element/xnat:subjectData/search_field/xnat:subjectData.ID/project/HCP_SUBJECTS/\" target=\"_blank\">"+siblingRow[1].toString()+"</a>";
                        //+" ("+siblingRow[2].toString()

                        siblingArrayList.add(siblingRow[1].toString());
                        siblingCount++;
                    }
                }
                if(!subjectsInSubjectsList.contains(idAndLabel.get(1))){
                    //If that family has not yet been added, add it
                    subjectLists.add(siblingArray);
                    subjectsInSubjectsList.addAll(siblingArrayList);
                }

            }
        }
        catch(Exception e){
            logger.error("Error creating unrelated subject service from the database");
        }
    }

    public ArrayList<String[]> getSubjectLists(){
        return subjectLists;
    }

    public int getMaxFamilySize(){
        return maxFamilySize;
    }


}
