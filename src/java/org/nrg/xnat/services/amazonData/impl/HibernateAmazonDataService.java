package org.nrg.xnat.services.amazonData.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xnat.daos.AmazonDataDAO;
import org.nrg.xnat.entities.AmazonData;
import org.nrg.xnat.services.amazonData.AmazonDataService;
import org.springframework.stereotype.Service;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by mmckay01 on 1/29/14.
 */
@Service
public class HibernateAmazonDataService extends AbstractHibernateEntityService<AmazonData, AmazonDataDAO> implements AmazonDataService {

    public HibernateAmazonDataService() {
    }

    @Override
    @Transactional
    public List<AmazonData> getAmazonDataForUser(String userLogin) {
        AmazonData example = new AmazonData();
        example.setUserLogin(userLogin);
        return _dao.findByExample(example, EXCLUSION_PROPERTIES);
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String getSecretKey() {
        return secretKey;
    }

    @Override
    public void setAwsGroup(String awsGroup) {
        this.awsGroup = awsGroup;
    }

    @Override
    public String getAwsGroup() {
        return awsGroup;
    }

    @Override
    public void setAllUsers(String allUsers) {
        this.allUsers = allUsers;
    }

    @Override
    public String getAllUsers() {
        return allUsers;
    }

    @Override
    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    @Override
    public String toJson(final List<AmazonData> amazonDataList) throws IOException{
        return _mapper.writeValueAsString(amazonDataList);
    }

    private static final String[] EXCLUSION_PROPERTIES = new String[] { "id", "created", "disabled", "enabled", "timestamp", "hasAws", "accessKey", "awsGroup" };

    private static final Log _log = LogFactory.getLog(HibernateAmazonDataService.class);

    @Inject
    private AmazonDataDAO _dao;

    @Inject
    private DataSource _datasource;

    private String key="";

    private String secretKey="";

    private String awsGroup="";

    private String allUsers="";

    private final ObjectMapper _mapper = new ObjectMapper() {{
        getDeserializationConfig().set(DeserializationConfig.Feature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
        getDeserializationConfig().set(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        getDeserializationConfig().set(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        getDeserializationConfig().set(DeserializationConfig.Feature.WRAP_EXCEPTIONS, true);
        getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        getSerializationConfig().set(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS, false);
    }};
}
