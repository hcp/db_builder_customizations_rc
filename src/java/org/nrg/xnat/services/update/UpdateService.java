package org.nrg.xnat.services.update;

import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.xnat.entities.Publication;
import org.nrg.xnat.entities.Update;

import java.io.IOException;
import java.util.List;

/**
 * Created by mmckay01 on 1/29/14.
 */
public interface UpdateService extends BaseHibernateService<Update>{
    public static String SERVICE_NAME = "UpdateService";

    public List<Update> getUpdatesForProject(String project);

    public String toJson(final List<Update> updates) throws IOException;

    public List<Update> getUpdates(String project, String date, String update, String link);

    public List<Update> getUpdates();

}
