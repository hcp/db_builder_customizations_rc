package org.nrg.xnat.services.codedid;

import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.xnat.entities.CodedId;

import java.util.List;

/**
 * Created by mmckay01 on 1/29/14.
 */
public interface CodedIdService extends BaseHibernateService<CodedId>{
    public static String SERVICE_NAME = "CodedIdService";

    public List<CodedId> getCodedIdSetForProject(String project);
}
