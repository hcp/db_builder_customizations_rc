package org.nrg.xnat.turbine.modules.actions;

//Copyright 2006 Harvard University / Washington University School of Medicine All Rights Reserved
/*
 * Created on Oct 18, 2006
 *
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ParameterParser;
import org.apache.velocity.context.Context;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.turbine.modules.actions.SecureAction;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFTTable;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.utils.FileUtils;
import org.nrg.xnat.entities.CodedId;
import org.nrg.xnat.services.codedid.CodedIdService;
import org.apache.commons.lang.StringUtils;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.om.XnatProjectdata;

public class SubjectKeyUpload extends SecureAction {

    @Override
    public void doPerform(RunData data, Context context) throws Exception {
        preserveVariables(data,context);
    }

    public void doUpload(RunData data, Context context) throws Exception {
        preserveVariables(data,context);
        ParameterParser params = data.getParameters();

        //grab the FileItems available in ParameterParser
        FileItem fi = params.getFileItem("csv_to_store");
        String proj = (String)TurbineUtils.GetPassedParameter("subjectKeyProjectId", data);
        context.put("projectName", (XnatProjectdata.getXnatProjectdatasById(proj, TurbineUtils.getUser(data), false)).getName());
        if (fi != null)
        {
            ArrayList<String> problems= new ArrayList<String>();
            String tempName = "";
            File temp = null;
            try{
                File folder = TurbineUtils.getUser(data).getCachedFile("codedIds");
                if(!folder.exists()){
                    folder.mkdirs();
                }
                tempName = "Subjects"+((folder.listFiles()!=null)?folder.listFiles().length:0)+".csv";
                temp = new File(folder,tempName);
                fi.write(temp);
            }catch(Exception e){
                problems.add("Error getting coded ID cache.");
            }
            List<List<String>> rows = null;
            try{
                rows = FileUtils.CSVFileToArrayList(temp);
            }
            catch(FileNotFoundException e){
                problems.add("Could not find the coded ID file you specified.");
            }
            catch(IOException e){
                problems.add("Error opening the specified coded ID file.");
            }

            fi.delete();

            ArrayList<String> subjectList = new ArrayList<String>();
            ArrayList<String> codedIdList = new ArrayList<String>();
            ArrayList<Integer> problematicRows = new ArrayList<Integer>();

            try{
                String currentProject = (String) data.getSession().getAttribute("project");
                String tooFewColumnsError = "At least one of your rows has fewer than two columns of data.";
                String subjectIdCharactersError = "Your subject IDs can contain only letters, numbers, underscores, periods, and dashes. This must be the first column and its column header must be 'SubjectId'.";
                String codedIdCharactersError = "Your coded IDs can contain only letters, numbers, underscores, periods, and dashes. This must be the second column and its column header must be 'CodedId'.";
                String invalidSubjectsError = "Your subject IDs must match the IDs of subjects in the "+currentProject+" project.";
                String duplicateSubjectsError = "You have at least one duplicate subject. All subjects must be unique.";
                String duplicateCodedIdError = "You have at least one duplicate coded ID. All coded IDs must be unique.";
                String publishedProjectError = "The subject key "+proj+" has already been published and subject information cannot be changed. Please contact a site admin if you need to modify this key.";
                String invalidHeadersError = "Your column headers are invalid. The first column header must be 'SubjectId' and the second column header must be 'CodedId'.";
                String noSubjectsError = "You must include at least one SubjectId/CodedId pairing in your csv.";
                if( (XnatProjectdata.getProjectByIDorAlias(proj,TurbineUtils.getUser(data),false)).getPublicAccessibility().equals("protected")){
                    problems.add(publishedProjectError);
                }
                else{
                    if (!(rows.size()>0 && (rows.get(0).get(0).equals("SubjectId")||rows.get(0).get(0).equals("'SubjectId'")||rows.get(0).get(0).equals("\"SubjectId\"")) && (rows.get(0).get(1).equals("CodedId")||rows.get(0).get(1).equals("'CodedId'")||rows.get(0).get(1).equals("\"CodedId\"")))){
                        problems.add(invalidHeadersError);
                    }
                    if(rows.size()>0){
                        rows.remove(0);
                    }
                    for(int i=0; i<rows.size();i++){
                        List<String> row = rows.get(i);
                        if(!(row.get(0).equals("")&&row.get(1).equals(""))){
                            if(row.size()<2 || row.get(1).equals("")){
                                if(!problems.contains(tooFewColumnsError)){
                                    problems.add(tooFewColumnsError);
                                }
                                problematicRows.add(i+1);
                            }
                            else{
                                row.set(0,StringUtils.trimToEmpty(row.get(0)));
                                row.set(1,StringUtils.trimToEmpty(row.get(1)));
                                if(((row.get(0).startsWith("'") && row.get(0).endsWith("'")) || (row.get(0).startsWith("\"") && row.get(0).startsWith("\"")))){
                                    row.set(0,row.get(0).substring(1, row.get(0).length()-1));
                                }
                                if(((row.get(1).startsWith("'") && row.get(1).endsWith("'")) || (row.get(1).startsWith("\"") && row.get(1).startsWith("\"")))){
                                    row.set(1,row.get(1).substring(1, row.get(1).length()-1));
                                }
                                if(i!=0 || !row.get(0).equals("SubjectId") || !row.get(1).equals("CodedId")){
                                    //Not header row
                                    if(subjectList.contains(row.get(0))){
                                        if(!problems.contains(duplicateSubjectsError)){
                                            problems.add(duplicateSubjectsError);
                                        }
                                        problematicRows.add(i+1);
                                    }
                                    else{
                                        subjectList.add(row.get(0));
                                    }
                                    if(codedIdList.contains(row.get(1))){
                                        if(!problems.contains(duplicateCodedIdError)){
                                            problems.add(duplicateCodedIdError);
                                        }
                                        problematicRows.add(i+1);
                                    }
                                    else{
                                        codedIdList.add(row.get(1));
                                    }
                                    if( !(row.get(0).matches("^[a-zA-Z0-9_.-]*$")) ){
                                        if(!problems.contains(subjectIdCharactersError)){
                                            problems.add(subjectIdCharactersError);
                                        }
                                        problematicRows.add(i+1);
                                    }
                                    if( !(row.get(1).matches("^[a-zA-Z0-9_.-]*$"))){
                                        if(!problems.contains(codedIdCharactersError)){
                                            problems.add(codedIdCharactersError);
                                        }
                                        problematicRows.add(i+1);
                                    }

                                    final XFTTable t= XFTTable.Execute("SELECT subject_id,label FROM xnat_projectParticipant WHERE project='"+ currentProject + "'", TurbineUtils.getUser(data).getDBName(), TurbineUtils.getUser(data).getUsername());
                                    final Hashtable lbls=t.toHashtable("label","subject_id");
                                    if(!lbls.containsKey(row.get(0))){
                                        if(!problems.contains(invalidSubjectsError)){
                                            problems.add(invalidSubjectsError);
                                        }
                                        problematicRows.add(i+1);
                                    }
                                }
                            }
                        }
                    }
                    if(subjectList.size()<=0){
                        problems.add(noSubjectsError);
                    }
                }
            }catch(Exception e){
                problems.add("Error processing provided file.");
            }
            context.put("rows",rows);
            context.put("tempName",tempName);
            context.put("problems",problems);
            context.put("problematicRows",problematicRows);
            context.put("subjectKeyProjectId",(String)TurbineUtils.GetPassedParameter("subjectKeyProjectId",data));
            data.setScreenTemplate("SubjectKey2.vm");
        }else{
            data.setScreenTemplate("SubjectKey2.vm");
        }
    }

    public void doStore(RunData data,Context context) throws Exception{
        preserveVariables(data,context);

        File folder = TurbineUtils.getUser(data).getCachedFile("codedIds");
        if(!folder.exists()){
            folder.mkdirs();
        }
        String tempName = (String)TurbineUtils.GetPassedParameter("tempName",data);
        File temp = new File(folder,tempName);
        if(!temp.exists()){
            logger.error("Temp file error.");
        }
        List<List<String>> rows = FileUtils.CSVFileToArrayList(temp);
        ArrayList<String> subjectList = new ArrayList<String>();
        ArrayList<String> codedIdList = new ArrayList<String>();
        String proj = (String)TurbineUtils.GetPassedParameter("subjectKeyProjectId", data);
        ArrayList<String> problems= new ArrayList<String>();
        ArrayList<Integer> problematicRows = new ArrayList<Integer>();

        try{
            String currentProject = (String) data.getSession().getAttribute("project");
            String tooFewColumnsError = "At least one of your rows has fewer than two columns of data.";
            String subjectIdCharactersError = "Your Subject IDs can contain only letters, numbers, underscores, periods, and dashes. This must be the first column and its column header must be 'SubjectId'.";
            String codedIdCharactersError = "Your Coded Ids can contain only letters, numbers, underscores, periods, and dashes. This must be the second column and its column header must be 'CodedId'.";
            String invalidSubjectsError = "Your Subject IDs must match the IDs of subjects in the "+currentProject+" project.";
            String duplicateSubjectsError = "You have at least one duplicate subject. All subjects must be unique.";
            String duplicateCodedIdError = "You have at least one duplicate coded ID. All coded IDs must be unique.";
            String publishedProjectError = "The subject key "+proj+" has already been published and subject information cannot be changed. Please contact a site admin if you need to modify this key.";
            String invalidHeadersError = "Your column headers are invalid. The first column header must be 'SubjectId' and the second column header must be 'CodedId'.";
            String noSubjectsError = "You must include at least one SubjectId/CodedId pairing in your csv.";
            if( (XnatProjectdata.getProjectByIDorAlias(proj,TurbineUtils.getUser(data),false)).getPublicAccessibility().equals("protected")){
                problems.add(publishedProjectError);
            }
            else{
                if (!(rows.size()>0 && (rows.get(0).get(0).equals("SubjectId")||rows.get(0).get(0).equals("'SubjectId'")) && (rows.get(0).get(1).equals("CodedId")||rows.get(0).get(1).equals("'CodedId'")))){
                    problems.add(invalidHeadersError);
                }
                for(int i=0; i<rows.size();i++){
                    List<String> row = rows.get(i);
                    if(!(row.get(0).equals("")&&row.get(1).equals(""))){
                        if(row.size()<2 || row.get(1).equals("")){
                            if(!problems.contains(tooFewColumnsError)){
                                problems.add(tooFewColumnsError);
                            }
                            problematicRows.add(i+1);
                        }
                        else{
                            row.set(0, StringUtils.trimToEmpty(row.get(0)));
                            row.set(1,StringUtils.trimToEmpty(row.get(1)));
                            if(((row.get(0).startsWith("'") && row.get(0).endsWith("'")) || (row.get(0).startsWith("\"") && row.get(0).startsWith("\"")))){
                                row.set(0,row.get(0).substring(1, row.get(0).length()-1));
                            }
                            if(((row.get(1).startsWith("'") && row.get(1).endsWith("'")) || (row.get(1).startsWith("\"") && row.get(1).startsWith("\"")))){
                                row.set(1,row.get(1).substring(1, row.get(1).length()-1));
                            }
                            if(i!=0 || !row.get(0).equals("SubjectId") || !row.get(1).equals("CodedId")){
                                //Not header row
                                if(subjectList.contains(row.get(0))){
                                    if(!problems.contains(duplicateSubjectsError)){
                                        problems.add(duplicateSubjectsError);
                                    }
                                    problematicRows.add(i+1);
                                }
                                else{
                                    subjectList.add(row.get(0));
                                }
                                if(codedIdList.contains(row.get(1))){
                                    if(!problems.contains(duplicateCodedIdError)){
                                        problems.add(duplicateCodedIdError);
                                    }
                                    problematicRows.add(i+1);
                                }
                                else{
                                    codedIdList.add(row.get(1));
                                }

                                if( !(row.get(0).matches("^[a-zA-Z0-9_.-]*$")) ){
                                    if(!problems.contains(subjectIdCharactersError)){
                                        problems.add(subjectIdCharactersError);
                                    }
                                    problematicRows.add(i+1);
                                }
                                if( !(row.get(1).matches("^[a-zA-Z0-9_.-]*$"))){
                                    if(!problems.contains(codedIdCharactersError)){
                                        problems.add(codedIdCharactersError);
                                    }
                                    problematicRows.add(i+1);
                                }

                                final XFTTable t= XFTTable.Execute("SELECT subject_id,label FROM xnat_projectParticipant WHERE project='"+ currentProject + "'", TurbineUtils.getUser(data).getDBName(), TurbineUtils.getUser(data).getUsername());
                                final Hashtable lbls=t.toHashtable("label","subject_id");
                                if(!lbls.containsKey(row.get(0))){
                                    if(!problems.contains(invalidSubjectsError)){
                                        problems.add(invalidSubjectsError);
                                    }
                                    problematicRows.add(i+1);
                                }
                            }
                        }
                    }
                }
                if(subjectList.size()<=0){
                    problems.add(noSubjectsError);
                }
            }
        }catch(Exception e){
            problems.add("Error processing provided file.");
        }
        if (rows.size()>0 && (rows.get(0).get(0).equals("SubjectId")||rows.get(0).get(0).equals("'SubjectId'")||rows.get(0).get(0).equals("\"SubjectId\"")) && (rows.get(0).get(1).equals("CodedId")||rows.get(0).get(1).equals("'CodedId'")||rows.get(0).get(0).equals("\"CodedId\""))){
            rows.remove(0);
        }
        if(problems.size()>0){
            context.put("rows",rows);
            context.put("tempName",tempName);
            context.put("problems",problems);
            context.put("problematicRows",problematicRows);
            XnatProjectdata project = XnatProjectdata.getXnatProjectdatasById(proj, TurbineUtils.getUser(data), false);
            context.put("projectName", project.getName());
            data.setScreenTemplate("SubjectKey2.vm");
        }
        else{
            String subjectKeyProjectId = (String)TurbineUtils.GetPassedParameter("subjectKeyProjectId",data);
            List<CodedId> oldCodedIds = XDAT.getContextService().getBean(CodedIdService.class).getCodedIdSetForProject(subjectKeyProjectId);
            for(CodedId oldCodedId : oldCodedIds){
                XDAT.getContextService().getBean(CodedIdService.class).delete(oldCodedId);
            }
            for(List<String> row : rows){
                String subjectId = row.get(0);
                String codedId = row.get(1);
                CodedId newSubjectKey = new CodedId(proj, StringUtils.trimToEmpty(subjectId), StringUtils.trimToEmpty(codedId));
                try{
                    XDAT.getContextService().getBean(CodedIdService.class).create(newSubjectKey);
                }catch(Throwable e){

                }
            }
            data.setRedirectURI(TurbineUtils.GetRelativeServerPath(data)+ "/REST/projects/"+subjectKeyProjectId);
        }
    }

    static org.apache.log4j.Logger logger = Logger.getLogger(SubjectKeyUpload.class);
}
