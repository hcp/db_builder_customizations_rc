//Copyright 2006 Harvard University / Washington University School of Medicine All Rights Reserved
/*
 * Created on Oct 17, 2006
 *
 */
package org.nrg.xnat.turbine.modules.actions;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.turbine.modules.actions.SecureAction;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.nrg.xdat.om.ArcArchivespecification;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class DownloadMultipleResources extends SecureAction {

    private final Map<String, String> _parameters;
    private String _filesToDownload;

    public DownloadMultipleResources() {
	_parameters = new HashMap<String, String>();
    _filesToDownload = "";
    }

    @Override
    public void doPerform(final RunData data, final Context context) {
        initializeParameters(data);

        String project = getParameter("project");
        if (StringUtils.isBlank(project)) {
            project = StringUtils.trimToEmpty(XDAT.getConfigService().getConfigContents("dashboard", "project"));
        }
        context.put("project", StringEscapeUtils.escapeJava(project));
        context.put("resource", StringEscapeUtils.escapeJava(getParameter("resource")));
        context.put("files", getFilesToDownload());

        try{
            final ArcArchivespecification arc = ArcSpecManager.GetInstance();
            final URL url = new URL(arc.getSiteUrl());
            context.put("webapp", url);   // local host, to start
        }catch(Exception e){
        }
        data.setScreenTemplate("DownloadMultipleResources.vm");
    }

    private void initializeParameters(final RunData data) {
        final Map<String, String> parameters = TurbineUtils.GetDataParameterHash(data);
        for (final String parameter : parameters.keySet()) {
            String pValue = StringEscapeUtils.unescapeXml(StringEscapeUtils.unescapeHtml(parameters.get(parameter)));
            _parameters.put(parameter, pValue);
            if(parameter.startsWith("file-") && !parameter.startsWith("file-all")){
                _filesToDownload=_filesToDownload+pValue+",";
            }
        }
    }

    private String getFilesToDownload(){
        return _filesToDownload.substring(0,_filesToDownload.length()-1);
    }

    private String getParameter(final String parameter) {
	return _parameters.get(parameter);
    }
}