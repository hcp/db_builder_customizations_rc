package org.nrg.xnat.turbine.modules.screens;

import org.apache.commons.lang.StringUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;

public class XDATScreen_download_packages extends SecureScreen {

    @Override
    protected void doBuildTemplate(final RunData data, final Context context) throws Exception {
        String projId = getProjectId(data);
        context.put("projectId", projId);
        XnatProjectdata p= XnatProjectdata.getProjectByIDorAlias(projId, TurbineUtils.getUser(data), false);
        if(p!=null){
            context.put("projectName", p.getName());
        }
        final String query = data.getParameters().get("query");
        if (!StringUtils.isBlank(query)) {
            context.put("query", query);
        }
        else{
            final String subjects = data.getParameters().get("subjects");
            if (!StringUtils.isBlank(subjects)) {
                context.put("query", "Subject = (="+subjects+")");
            }
        }
        // the action that calls us will have put the subjectIds in the context for us

        if(data.getParameters().get("subjects")!=null && data.getParameters().get("MEGSubjects")!=null && data.getParameters().get("subjects").equals(data.getParameters().get("MEGSubjects"))){
            context.put("selected","MEG");
        }
    }

    private String getProjectId(final RunData data) {
        String project = StringUtils.trimToEmpty(XDAT.getConfigService().getConfigContents("dashboard", "project"));//If no project is specified, set it to the default project
        String sessionProject = (String)data.getSession().getAttribute("project");
        if(sessionProject!=null && sessionProject!=""){
            project = sessionProject;
        }
        final String curr_proj = data.getParameters().get("project");
        if (!StringUtils.isBlank(curr_proj)) {
            project=curr_proj;
        }
        return project;
    }
}
