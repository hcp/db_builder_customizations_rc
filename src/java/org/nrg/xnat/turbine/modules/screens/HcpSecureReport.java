package org.nrg.xnat.turbine.modules.screens;

import org.apache.commons.lang.StringUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.hcp.security.HcpUserDatabaseEntryUtils;

import java.util.ArrayList;

/**
 * Created by Michael Hileman on 2015/02/13.
 */
public class HcpSecureReport extends SecureReport {
    public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(HcpSecureReport.class);

    public void finalProcessing(RunData data, Context context) {
        try {
            /* Handle switching between project */

            // Get our current project context
            String curProjContext = (String) data.getSession().getAttribute("project");
            String pid = null;

            // Use the query string if there is one or the velocity context if not
            if (TurbineUtils.HasPassedParameter("project", data))
                pid = (String) TurbineUtils.GetPassedParameter("project", data);
            else if (context.get("project") != null)
                pid = (String) context.get("project"); // might want to use this when available, call this.finalProc at end of Screen classes
            else // set to curProjContext to avoid null pointer exceptions
                pid = curProjContext;
            // should do this in function and break on else statement

            // Get the current project's root project
            XDATUser user = (XDATUser) data.getSession().getAttribute("user");
            XnatProjectdata p = XnatProjectdata.getProjectByIDorAlias(pid, user, false);
            String newProjContext = p.getRootProject().getId();

            // Check if there's been any change in project context
            if (!curProjContext.equals(newProjContext)) {
                // The project has changed
                data.getSession().setAttribute("project", newProjContext);
                context.put("project", newProjContext);
                data.getSession().setAttribute("tier", 0);
            }

            /* Handle access tiers */

            // Set tier to whatever is in the query string or the session variable
            // Tier changes only if user has access to that tier for the project
            if (TurbineUtils.HasPassedParameter("tierSelect", data)) {
                int requestedTier = TurbineUtils.GetPassedInteger("tierSelect", data);

                if (HcpUserDatabaseEntryUtils.hasAccessLevel(user, newProjContext, requestedTier)) {
                    data.getSession().setAttribute("tier", requestedTier);
                }
            } // Otherwise, leave the tier alone

            // As a safeguard, check that the user has access to the currently set tier value
            int tier = (Integer) data.getSession().getAttribute("tier");

            if (!HcpUserDatabaseEntryUtils.hasAccessLevel(user, newProjContext, tier))
                data.getSession().setAttribute("tier", 0);

            // See if user has any restricted access within the current project context
            if (HcpUserDatabaseEntryUtils.getAccessLevel(user, newProjContext) > 0 || user.isSiteAdmin() || user.isOwner(newProjContext))
                data.getSession().setAttribute("hasRestrictedAccess", true);
            else
                data.getSession().setAttribute("hasRestrictedAccess", false);

        } catch (Exception e) {
            logger.error(e);
        }
    }
}