/*
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 *
 * 	@author Will Horton (Email: hortonw@mir.wustl.edu)

*/

package org.apache.turbine.app.hcpexternal.modules.screens;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xnat.services.datadictionary.DataDictionaryService;
import org.nrg.xdat.security.XDATUser;

public class MegaTrawl_launcher extends SecureScreen {

    @Override
    protected void doBuildTemplate(final RunData data, final Context context) throws Exception {

        XDATUser user = TurbineUtils.getUser(data);
        context.put("user", user);
    }

}