package org.nrg.xnat.turbine.modules.screens;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xnat.services.datadictionary.DataDictionaryService;

/**
 * @author tim@deck5consulting.com
 *         <p/>
 * Prep class for the interactive dashboard (Quick View).  Doesn't do anything but basic initialization (security)
 */
public class SubjectKey3 extends SecureScreen {

	@Override
	protected void doBuildTemplate(final RunData data, final Context context) throws Exception {

	}

}
