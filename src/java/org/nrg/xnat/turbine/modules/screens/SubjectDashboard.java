package org.nrg.xnat.turbine.modules.screens;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.exceptions.*;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.UserGroup;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.db.PoolDBUtils;
import org.nrg.xnat.services.datadictionary.DataDictionaryService;
import org.nrg.xdat.security.XDATUser;
import org.nrg.hcp.security.HcpUserDatabaseEntryUtils;

/**
 * @author tim@deck5consulting.com
 *         <p/>
 * Prep class for the interactive dashboard (Quick View).  Doesn't do anything but basic initialization (security)
 */
public class SubjectDashboard extends SecureScreen {

	@Override
	protected void doBuildTemplate(final RunData data, final Context context) throws Exception {

        final String groupId = StringEscapeUtils.unescapeXml(StringEscapeUtils.unescapeHtml(((String) TurbineUtils
            .GetPassedParameter("subjectGroupName", data))));
        final String subject = StringEscapeUtils.unescapeXml(StringEscapeUtils.unescapeHtml(((String) TurbineUtils
            .GetPassedParameter("subject", data))));

        context.put("autoOpenGroupName", StringEscapeUtils.escapeJava(groupId)); // this variable is going directly into
                                         // a JS string, need to escape double
										 // quotes
        context.put("autoOpenSubject", subject);

        XDATUser user = (XDATUser)data.getSession().getAttribute("user");
        if(TurbineUtils.HasPassedParameter("tierSelect", data)){
            String proj = (String)data.getSession().getAttribute("project");
            int requestedTier = TurbineUtils.GetPassedInteger("tierSelect", data);

            if(HcpUserDatabaseEntryUtils.hasAccessLevel(user, proj, requestedTier)) {
                data.getSession().setAttribute("tier", requestedTier);
            }
        }
		
        String project=(String)data.getSession().getAttribute("project");
        if(TurbineUtils.HasPassedParameter("project", data)){
            data.getSession().setAttribute("project",(String)TurbineUtils.GetPassedParameter("project", data));
            project = (String)TurbineUtils.GetPassedParameter("project", data);
        }

        // Check that the user has any project access and return null if not
        UserGroup projectGroup = user.getGroupByTag(project);
        if (projectGroup == null) {
            data.setStatusCode(403);
            data.setMessage("You do not have access to " + project + ". You must accept data use terms before accessing.");
            return;
        }

        if(TurbineUtils.HasPassedParameter("subjectID", data)){
            String[] ids = StringEscapeUtils.unescapeXml(StringEscapeUtils.unescapeHtml(((String) TurbineUtils
                    .GetPassedParameter("subjectID", data)))).split(",");
            String labels = "";
            for(String id:ids){
                String label = (String) PoolDBUtils.ReturnStatisticQuery("SELECT id, label, project FROM xnat_subjectdata WHERE LOWER(project) = 'hcp_subjects' AND LOWER(id) = '" + id.toLowerCase() + "';", "label", null, null);
                labels = labels+label+"|";

            }
            labels = labels.substring(0,labels.length()-1);
            context.put("autoOpenSubject", labels);

        }

        int tier = (Integer)(data.getSession().getAttribute("tier"));
        DataDictionaryService dictService = XDAT.getContextService().getBean(DataDictionaryService.class);
        context.put("categories", dictService.getCategoriesWithTiers(project,tier));
        context.put("assessments", dictService.getAssessmentsWithTiers(project,tier));
        context.put("attributeMap",dictService.getAttributeMap(project,tier));

        if (project.contains(",")) {
        	context.put("project", project);
        }else{
            XnatProjectdata p= XnatProjectdata.getProjectByIDorAlias(project, TurbineUtils.getUser(data), false);
            if(p!=null){
            	context.put("project", p.getId());
            	context.put("om", p);
            }
        }
	}

}
