package org.nrg.xnat.turbine.modules.screens;

import org.apache.log4j.Logger;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xdat.entities.XDATUserDetails;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.XFTItem;
import org.nrg.xft.XFTTable;
import org.nrg.xft.search.TableSearch;
import org.apache.velocity.VelocityContext;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xnat.services.datadictionary.DataDictionaryService;

import java.lang.Override;
import java.util.Hashtable;
import org.nrg.xdat.security.XDATUser;

/**
 * Created with IntelliJ IDEA.
 * User: mmckay01
 * Date: 7/30/13
 * Time: 3:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class XDATScreen_report_xnat_subjectData extends HcpSecureReport {
    public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XDATScreen_report_xnat_subjectData.class);

    @Override
    public void finalProcessing(RunData data, Context context) {
        // Handle tiers in super class
        super.finalProcessing(data, context);

        try {
            XnatSubjectdata sub = new XnatSubjectdata(item);
            context.put("subject",sub);

            String dbname = ((XDATUserDetails)((VelocityContext) context).internalGet("user")).getDBName();
            String login = ((XDATUserDetails)((VelocityContext) context).internalGet("user")).getLogin();
            String query = "SELECT xnat_subjectdata.id, xnat_subjectdata.label FROM (xnat_subjectdata RIGHT JOIN (SELECT DISTINCT subject_id FROM xnat_subjectassessordata WHERE id in (SELECT id from hcp_restrictedtier1 WHERE restricted_motherid = (SELECT DISTINCT restricted_motherid FROM hcp_restrictedtier1 WHERE id in (SELECT id FROM xnat_subjectassessordata WHERE subject_id = '"+sub.getId()+"')) AND restricted_fatherid = (SELECT DISTINCT restricted_fatherid FROM hcp_restrictedtier1 WHERE id in (SELECT id FROM xnat_subjectassessordata WHERE subject_id = '"+sub.getId()+"'))) AND subject_id <> '"+sub.getId()+"') AS siblings ON xnat_subjectdata.id = subject_id JOIN xnat_projectparticipant ON xnat_subjectdata.id = xnat_projectparticipant.subject_id) WHERE xnat_projectparticipant.project='"+data.getSession().getAttribute("defaultProject")+"';";

            XFTTable table = TableSearch.Execute(query, dbname, login);
            Object siblingsList = null;
            if (table.size()>0)
            {
                table.resetRowCursor();
                int rowCount = 0;
                while(table.hasMoreRows())
                {
                    rowCount++;
                    Hashtable row = table.nextRowHash();
                    if(rowCount<=3){
                        context.put("sibling"+rowCount,row.get("id"));
                        context.put("siblinglabel"+rowCount,row.get("label"));
                    }
                }
            }

//            String project = "";
//            if(TurbineUtils.HasPassedParameter("project", data)){
//                project = (String)TurbineUtils.GetPassedParameter("project", data);
//            }
//            else{
//                project = sub.getProject();
//            }
//            context.put("project",project);

        } catch (Exception e) {
            logger.error("",e);
        }
    }
}
