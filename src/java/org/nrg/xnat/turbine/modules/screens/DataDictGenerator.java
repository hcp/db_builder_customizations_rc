package org.nrg.xnat.turbine.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;

/**
 * @author tim@deck5consulting.com
 *         <p/>
 * Prep class for the interactive dashboard (Quick View).  Doesn't do anything but basic initialization (security)
 */
public class DataDictGenerator extends SecureScreen {

	@Override
	protected void doBuildTemplate(final RunData data, final Context context) throws Exception {

	}

}
