/*
 * org.nrg.xnat.turbine.modules.screens.XDATScreen_report_xnat_projectData
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 5/21/15 9:47 AM
 */
package org.nrg.xnat.turbine.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.hcp.security.HcpLdapHelper;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.exceptions.IllegalAccessException;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.db.ItemAccessHistory;
import org.nrg.xft.db.PoolDBUtils;
import org.nrg.xft.exception.DBPoolException;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xnat.entities.CodedId;
import org.nrg.xnat.entities.Publication;
import org.nrg.xnat.entities.Update;
import org.nrg.xnat.services.codedid.CodedIdService;
import org.nrg.xnat.services.datadictionary.DataDictionaryService;
import org.nrg.xnat.services.publication.PublicationService;
import org.nrg.xnat.services.update.UpdateService;
import org.nrg.xnat.turbine.utils.ProjectAccessRequest;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author XDAT
 */
public class XDATScreen_report_xnat_projectData extends HcpSecureReport {
    public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XDATScreen_report_xnat_projectData.class);
    /* (non-Javadoc)
     * @see org.nrg.xdat.turbine.modules.screens.SecureReport#finalProcessing(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
     */
    @Override
    public void finalProcessing(RunData data, Context context) {

        XnatProjectdata project = (XnatProjectdata)om;
        XDATUser user = TurbineUtils.getUser(data);
        String projectId = project.getId();
        context.put("project", projectId);
        // data.getSession().setAttribute("project", projectId);
        super.finalProcessing(data, context);

        try{
            if(TurbineUtils.HasPassedParameter("view", data)){
                context.put("view",TurbineUtils.GetPassedParameter("view", data));
            }

            int tier = (Integer)(data.getSession().getAttribute("tier"));
            // Temporarily set to regular non-restricted project until dictService usage further investigated
            String restrictedProject = (String)data.getSession().getAttribute("project");

            DataDictionaryService dictService = XDAT.getContextService().getBean(DataDictionaryService.class);

            context.put("categories", dictService.getCategoriesWithTiers(restrictedProject,tier));
            context.put("assessments", dictService.getAssessmentsWithTiers(restrictedProject, tier));
            context.put("attributeMap",dictService.getAttributeMap(restrictedProject,tier));


            List<CodedId> codedIds = XDAT.getContextService().getBean(CodedIdService.class).getCodedIdSetForProject(projectId);
            String subjectsWithCodedIds = "";
            for(CodedId codedId : codedIds){
                subjectsWithCodedIds = subjectsWithCodedIds + codedId.getSubjectId()+",";
            }
            if(subjectsWithCodedIds.length()>0){
                subjectsWithCodedIds=subjectsWithCodedIds.substring(0,subjectsWithCodedIds.length()-1);
            }
            context.put("subjectsWithCodedIds",subjectsWithCodedIds);
            context.put("subjectKeyProjectId",projectId);
            context.put("codedIdCount", codedIds.size());
            context.put("connectomeType", project.getConnectomeType());

            List<Update> updates = XDAT.getContextService().getBean(UpdateService.class).getUpdatesForProject(projectId);
            context.put("updatesJSON",XDAT.getContextService().getBean(UpdateService.class).toJson(updates));

            List<Publication> publications = XDAT.getContextService().getBean(PublicationService.class).getPublicationsForProject(projectId);
            context.put("publicationsJSON",XDAT.getContextService().getBean(PublicationService.class).toJson(publications));
            context.put("publicationsCount", publications.size());
        }
        catch(Exception e){
            logger.error("Could not find default restricted project name.",e);
        }

        // See if the project should display Subject Key section or not
        boolean hasSubjectKeys = false;

        try {
            ArrayList<String> subjectKeyUmbrellas = (ArrayList) data.getSession().getAttribute("subjectKeyUmbrellas");

            // Get the root project of each umbrella and see if it's the current project ID
            for (String skUmbrella : subjectKeyUmbrellas) {
                XnatProjectdata p = XnatProjectdata.getProjectByIDorAlias(skUmbrella, user, false);
                String rootProjId = p.getRootProject().getId();

                if (rootProjId.equals(projectId)) {
                    hasSubjectKeys = true;
                    break;
                }
            }
        }
        catch (Exception e) {
            logger.error("Failed to parse through subject key umbrellas.");
        }
        context.put("hasSubjectKeys", hasSubjectKeys);

        try {
            if(user.canRead("xnat:subjectData/project",projectId)){
                ItemAccessHistory.LogAccess(user, item, "report");
            }

            if(ProjectAccessRequest.CREATED_PAR_TABLE){
                Integer parcount=(Integer)PoolDBUtils.ReturnStatisticQuery("SELECT COUNT(par_id)::int4 AS count FROM xs_par_table WHERE proj_id='"+ projectId + "'", "count", user.getDBName(), user.getLogin());
                context.put("par_count", parcount);
            }

            if(((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter("topTab",data))!=null){
                context.put("topTab", ((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter("topTab",data)));
            }

            if(((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter("bottomTab",data))!=null){
                context.put("bottomTab", ((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter("bottomTab",data)));
            }

            setDefaultTabs("xnat_projectData_summary_details", "xnat_projectData_summary_management", "xnat_projectData_summary_manage", "xnat_projectData_summary_pipeline", "xnat_projectData_summary_history");
            cacheTabs(context, "xnat_projectData/tabs");
        } catch (XFTInitException e) {
            logger.error("",e);
        } catch (ElementNotFoundException e) {
            logger.error("",e);
        } catch (DBPoolException e) {
            logger.error("",e);
        } catch (SQLException e) {
            logger.error("",e);
        } catch (IllegalAccessException e) {
            logger.error("",e);
        } catch (Exception e) {
            logger.error("",e);
        }
    }

    @Override
    public void doBuildTemplate(RunData data, Context context)
    {
        super.doBuildTemplate(data, context);
        // Recreate the user's tier map in the http session
        // TODO: this should probably happen in HcpLdapHelper if possible
        try {
            XDATUser user = TurbineUtils.getUser(data);
            HcpLdapHelper ldap = HcpLdapHelper.getInstance();
            data.getSession().setAttribute("userTiers", ldap.getUserTiers(user));
        } catch (Exception e) {
            logger.error("userTiers map could not be updated in the http session", e);
        }
    }
}
