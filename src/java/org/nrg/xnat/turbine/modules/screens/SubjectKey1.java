/*
 * org.nrg.xnat.turbine.modules.screens.XDATScreen_add_xnat_projectData
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 8:40 PM
 */
package org.nrg.xnat.turbine.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.hcp.security.HcpUserDatabaseEntryUtils;
import org.nrg.xdat.display.DisplayManager;
import org.nrg.xdat.om.ArcProject;
import org.nrg.xdat.security.ElementSecurity;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.modules.screens.EditScreenA;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFTItem;
import org.nrg.xft.schema.Wrappers.GenericWrapper.GenericWrapperElement;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.nrg.xnat.turbine.utils.XNATUtils;
import org.nrg.xdat.om.XnatProjectdata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;

public class SubjectKey1 extends EditScreenA {
    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SubjectKey1.class);

    public String getElementName() {
        return "xnat:projectData";
    }

    public ItemI getEmptyItem(RunData data) throws Exception
    {
        String s = getElementName();
        ItemI temp =  XFTItem.NewItem(s,TurbineUtils.getUser(data));
        return temp;
    }
    /* (non-Javadoc)
     * @see org.nrg.xdat.turbine.modules.screens.SecureReport#finalProcessing(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
     */
    public void finalProcessing(RunData data, Context context) {
        Hashtable hash = XNATUtils.getInvestigatorsForCreate(getElementName(),data);
        context.put("investigators",hash);
        context.put("arc",ArcSpecManager.GetInstance());
        context.put("projType", "subjectKey");

        if (TurbineUtils.HasPassedParameter("destination", data)){
            context.put("destination", TurbineUtils.GetPassedParameter("destination", data));
        }
        if (TurbineUtils.HasPassedParameter("projectID", data)){
            String passedProject = (String)TurbineUtils.GetPassedParameter("projectID", data);
            context.put("projectID", passedProject);
            context.put("subjectKeyProjectId", passedProject);
            XnatProjectdata proj = XnatProjectdata.getXnatProjectdatasById(passedProject, TurbineUtils.getUser(data), false);
            context.put("currProj", proj);
            context.put("getName", proj.getName());
            context.put("getSecondaryId", proj.getSecondaryId());
            context.put("getDescription", proj.getDescription());
            context.put("project", proj);
            context.put("getParentproject", proj.getParentproject());
            item = proj.getItem();
        }
        try {
            ArrayList<ElementSecurity> root = new ArrayList<ElementSecurity>();
            ArrayList<ElementSecurity> subjectAssessors = new ArrayList<ElementSecurity>();
            ArrayList<ElementSecurity> mrAssessors = new ArrayList<ElementSecurity>();
            ArrayList<ElementSecurity> petAssessors = new ArrayList<ElementSecurity>();

            Collection<ElementSecurity> all =ElementSecurity.GetElementSecurities().values();
            for (ElementSecurity es: all){
                try {
                    if (es.getAccessible() || (item.getStringProperty("ID")!=null && es.matchesUsageEntry(item.getStringProperty("ID")))){
                        GenericWrapperElement g= es.getSchemaElement().getGenericXFTElement();

                        if(g.instanceOf("xnat:mrAssessorData")){
                            mrAssessors.add(es);
                        }else if(g.instanceOf("xnat:petAssessorData")){
                            petAssessors.add(es);
                        }else if(g.instanceOf("xnat:subjectAssessorData")){
                            subjectAssessors.add(es);
                        }else if (g.instanceOf("xnat:subjectData") || g.instanceOf("xnat:experimentData")){
                            root.add(es);
                        }
                    }
                } catch (Throwable e) {
                    logger.error("",e);
                }
            }

            context.put("root", root);
            context.put("subjectAssessors", subjectAssessors);
            context.put("mrAssessors", mrAssessors);
            context.put("petAssessors", petAssessors);
            context.put("page_title", "New " + DisplayManager.GetInstance().getSingularDisplayNameForProject());
            context.put("item", item);
            if (item.getProperty("ID")!=null)
            {
                ArcProject p = ArcSpecManager.GetInstance().getProjectArc(item.getStringProperty("ID"));
                if (p!=null){
                    context.put("arcP", p);
                }
            }
        } catch (Exception e) {
            logger.error("",e);
        }

        // Set the parent project to the appropriate Coded ID project umbrella
        String curProjContext = (String) data.getSession().getAttribute("project");
        ArrayList<String> codedProjects  = (ArrayList) data.getSession().getAttribute("subjectKeyUmbrellas");
        boolean match = false;

        for (String codedProj : codedProjects) {
            XnatProjectdata projObject = XnatProjectdata.getXnatProjectdatasById(codedProj, TurbineUtils.getUser(data), false);
            String rootProj = projObject.getRootProject().getId();
            if (rootProj.equals(curProjContext)) {
                context.put("umbrella", codedProj);
                match = true;
                break;
            }
        }
        if (!match) {
            // Use the default dashboard project
            String dashProj = (String) data.getSession().getAttribute("dashboardProject");
            context.put("umbrella", dashProj);
            logger.info("No Coded ID parent project found for " + curProjContext + ". Using default dashboard project " + dashProj);
        }

        try {
            // Set tier to whatever is in the query string or the session variable
            // Tier changes only if user has access to that tier for the project
            XDATUser user = (XDATUser) data.getSession().getAttribute("user");
            String pid = (String) data.getSession().getAttribute("project");
            XnatProjectdata p = XnatProjectdata.getProjectByIDorAlias(pid, user, false);
            String newProjContext = p.getRootProject().getId();

            if (TurbineUtils.HasPassedParameter("tierSelect", data)) {
                int requestedTier = TurbineUtils.GetPassedInteger("tierSelect", data);

                if (HcpUserDatabaseEntryUtils.hasAccessLevel(user, newProjContext, requestedTier)) {
                    data.getSession().setAttribute("tier", requestedTier);
                }
            } // Otherwise, leave the tier alone

            // As a safeguard, check that the user has access to the currently set tier value
            int tier = (Integer) data.getSession().getAttribute("tier");

            if (!HcpUserDatabaseEntryUtils.hasAccessLevel(user, newProjContext, tier))
                data.getSession().setAttribute("tier", 0);

            // See if user has any restricted access within the current project context
            if (HcpUserDatabaseEntryUtils.getAccessLevel(user, newProjContext) > 0 || user.isSiteAdmin() || user.isOwner(newProjContext))
                data.getSession().setAttribute("hasRestrictedAccess", true);
        } catch (Exception e) {
            logger.error("Failed to handle 'tierSelect' parameter", e);
        }
    }
}