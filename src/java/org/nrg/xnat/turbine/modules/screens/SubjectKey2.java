package org.nrg.xnat.turbine.modules.screens;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xnat.services.datadictionary.DataDictionaryService;
import org.nrg.xdat.security.XDATUser;

/**
 * @author tim@deck5consulting.com
 *         <p/>
 * Prep class for the interactive dashboard (Quick View).  Doesn't do anything but basic initialization (security)
 */
public class SubjectKey2 extends SecureScreen {

	@Override
	protected void doBuildTemplate(final RunData data, final Context context) throws Exception {
        if (TurbineUtils.HasPassedParameter("projectID", data)){
            context.put("projectID", TurbineUtils.GetPassedParameter("projectID", data));
            context.put("subjectKeyProjectId", TurbineUtils.GetPassedParameter("projectID", data));
            XnatProjectdata proj = XnatProjectdata.getXnatProjectdatasById((String)TurbineUtils.GetPassedParameter("projectID", data), TurbineUtils.getUser(data), false);
            context.put("projectName", proj.getName());
        }
        else{
            context.put("subjectKeyProjectId", ((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter("subjectKeyProjectId",data)));
        }
	}

}
