package org.nrg.xnat.turbine.modules.screens;
//Copyright 2007 Washington University School of Medicine All Rights Reserved
/*
 * Created on Nov 1, 2007
 *
 */

import org.nrg.xdat.turbine.utils.TurbineUtils;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.turbine.modules.screens.RawScreen;
import org.apache.turbine.util.RunData;

public class DownloadSubjectKey extends RawScreen {

    /* (non-Javadoc)
     * @see org.apache.turbine.modules.screens.VelocitySecureScreen#doBuildTemplate(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
     */
    @Override
    protected void doOutput(RunData data) throws Exception {

        HttpServletResponse response = data.getResponse();
        //We have to set the size to workaround a bug in IE (see com.lowagie iText FAQ)
        //data.getResponse().setContentLength(baos.size());
        TurbineUtils.setContentDisposition(data.getResponse(), "template.csv", false);
        ServletOutputStream out = response.getOutputStream();

        StringBuffer sb = new StringBuffer();
        sb.append("'SubjectId','CodedId'");
        out.print(sb.toString());
        out.close();
    }
    public String getContentType(RunData data)
    {
        return "application/msexcel";
    }

}

