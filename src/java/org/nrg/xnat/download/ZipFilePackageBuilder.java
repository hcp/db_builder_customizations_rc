package org.nrg.xnat.download;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.Map;
import org.nrg.xdat.XDAT;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;

public abstract class ZipFilePackageBuilder implements PackageBuilder {
    private final Logger logger = LoggerFactory.getLogger(ZipFilePackageBuilder.class);

    protected abstract class ZipFilePackage implements Package {
        private final String id;
        private int count = 0;
        private long size = 0L;
        ImmutableList<Path> paths = null;
        ImmutableMap<URI,IOException> failures = null;

        protected ZipFilePackage(final String id) {
            this.id = id;
        }

        protected abstract void addPathsFor(final String id,
                                            final ImmutableList.Builder<Path> paths,
                                            final ImmutableMap.Builder<URI,IOException> failures);

        protected void relativizeAndAdd(final ImmutableList.Builder<Path> paths,
                                        final Path path,
                                        final int count,
                                        final long size) {
            paths.add(root.relativize(path));
            this.count += count;
            this.size += size;
        }

        protected void relativizeAndAdd(final ImmutableList.Builder<Path> paths,
                                        final Path path) {
            long size = 0;
            try {
                size = (Long)Files.getAttribute(path, "basic:size");
            } catch (IOException e) {
                logger.error("unable to get size for " + path, e);
            }
            relativizeAndAdd(paths, path, 0, size);
        }


        private void updatePaths() {
            if (null == paths) {
                final ImmutableList.Builder<Path> paths = ImmutableList.builder();
                final ImmutableMap.Builder<URI,IOException> failures = ImmutableMap.builder();

                addPathsFor(id, paths, failures);
                this.paths = paths.build();
                this.failures = failures.build();
            }
        }
        
        public Iterator<URI> iterator() {
            updatePaths();
            return Iterables.transform(paths, new Function<Path,URI>() {
                public URI apply(final Path path) {
                    return path.toUri();
                }
            }).iterator();
        }

        public int getFileCount() {
            updatePaths();
            return count;
        }

        public long getSize() {
            updatePaths();
            return size;
        }

        public Iterator<Path> getPathIterator() {
            updatePaths();
            return paths.iterator();
        }

        public long getMD5(URI uri) throws IOException {
            // TODO: implement
            throw new UnsupportedOperationException();
        }

        public Iterator<File> getFileIterator() {
            updatePaths();
            return Iterables.transform(paths, new Function<Path,File>() {
                public File apply(final Path path) {
                    return path.toFile();
                }
            }).iterator();
        }

        public Map<URI,?> getFailures() {
            updatePaths();
            return failures;
        }
    }

    private final Path root;

    public ZipFilePackageBuilder(final Path root) {
        this.root = root;
    }

    protected Path getRoot() {
        return root;
    }
}
