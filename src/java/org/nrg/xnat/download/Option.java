package org.nrg.xnat.download;

import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by mmckay01 on 5/12/14.
 */
@XmlRootElement
public class Option {

    private final Logger logger = LoggerFactory.getLogger(DownloadPackageService.class);
    private boolean _isDefault = false;
    private String _name = null;
    private String _label = null;
    private ArrayList<String> _related = null;
    private ArrayList<Filter> _children = null;

    public boolean getIsDefault() {
        return _isDefault;
    }

    public void setIsDefault(boolean isDefault) {
        this._isDefault = isDefault;
    }

    @XmlID
    public String getName() {
        return _name;
    }

    public void setName(String name) {
        this._name = name;
    }

    public String getLabel() {
        return _label;
    }

    public void setLabel(String label) {
        this._label = label;
    }

    public ArrayList<String> getRelated() {
        return _related;
    }

    public void setRelated(ArrayList<String> related) {
        this._related = related;
    }

    public ArrayList<Filter> getChildren() {
        return _children;
    }

    public void setChildren(ArrayList<Filter> children) {
        this._children = children;
    }

    public JSONObject toJSON() {
        final JSONObject option = new JSONObject();
        try{
            if(_name!=null){
                option.put("name", _name);
            }
            if(_label!=null){
                option.put("label", _label);
            }
            if(_isDefault!=false){
                option.put("isDefault", _isDefault);
            }

            JSONArray option_related = new JSONArray();
            if(_related!=null){
                for(String relate:_related){
                    option_related.put(relate);
                }
            }
            if(option_related.length()>0){
                option.put("related", option_related);
            }

            JSONArray option_children = new JSONArray();
            if(_children!=null){
                for(Filter child:_children){
                    option_children.put(child.toJSON());
                }
            }
            if(option_children.length()>0){
                option.put("children", option_children);
            }
        }
        catch(JSONException e){
            logger.error("Exception creating option JSON for "+_name+".", e);
        }
        return option;
    }

}
