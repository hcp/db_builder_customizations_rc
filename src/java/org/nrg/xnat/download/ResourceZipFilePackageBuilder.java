package org.nrg.xnat.download;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FilenameUtils;
import org.nrg.xdat.bean.CatCatalogBean;
import org.nrg.xdat.model.CatEntryI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.security.XDATUser;
import org.springframework.security.access.AccessDeniedException;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

public class ResourceZipFilePackageBuilder extends ZipFilePackageBuilder {
    private static final String SECURING_ELEMENT = "xnat:mrSessionData/project";

    protected class ResourceZipFilePackage extends ZipFilePackage {
        public ResourceZipFilePackage(final String filePath) {
            super(filePath);
            if (filePath == null || !filePath.endsWith(".zip")) {
                throw new IllegalArgumentException("This package builder only supports ZIP files");
            }
        }

        @Override
        protected void addPathsFor(final String id,
                                   final ImmutableList.Builder<Path> paths,
                                   final ImmutableMap.Builder<URI,IOException> failures) {
            final Path path = Paths.get(getFile(project, resource, id).getAbsolutePath());
            if (Files.exists(path)) {
                relativizeAndAdd(paths, path);
            } else {
                failures.put(path.toUri(),
                             new FileNotFoundException(path.toString()));
            }
        }
    }

    private final XnatProjectdata project;
    private final XnatResourcecatalog resource;

    public ResourceZipFilePackageBuilder(final Path root, final XnatProjectdata project, final String resourceId) {
        super(root);
        this.project = project;
        resource = getResource(project, resourceId);
    }


    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.download.PackageBuilder#apply(org.nrg.xdat.security.XDATUser, java.lang.String)
     */
    public Package apply(final String projectId, final XDATUser user, String id) {
        Throwable cause = null;
        try {
            if (user.canRead(SECURING_ELEMENT, projectId)) {
                return new ResourceZipFilePackage(id);
            }
        } catch (Throwable t) {
            cause = t;
        }
        throw new AccessDeniedException("user " + user.getUsername() + " cannot access project " + projectId, cause);
    }


    private XnatResourcecatalog getResource(final XnatProjectdata project, final String resourceId)
            throws ResourceNotFoundException {
        if (null == project) {
            throw new ResourceNotFoundException();
        }
        for (XnatAbstractresourceI resource : project.getResources_resource()) {
            if (resource.getLabel().equals(resourceId) && resource instanceof XnatResourcecatalog) {
                return (XnatResourcecatalog) resource;
            }
        }
        throw new ResourceNotFoundException();
    }

    private File getFile(XnatProjectdata project, XnatResourcecatalog resource, String filePath)
            throws ResourceNotFoundException {
        if (null == project) {
            throw new ResourceNotFoundException();
        }
        final CatCatalogBean catalog = resource.getCatalog(project.getArchiveRootPath());
        for (CatEntryI catEntry : catalog.getEntries_entry()) {
            if (catEntry.getUri().equals(filePath)) {
                String absoluteFilePath = resource.getCatalogFile(project.getArchiveRootPath()).getParent()
                        + File.separator + FilenameUtils.separatorsToSystem(filePath);
                return new File(absoluteFilePath);
            }
        }
        throw new ResourceNotFoundException();
    }

    public static class ResourceNotFoundException extends RuntimeException {
        private static final long serialVersionUID = 8917895048621323060L;
    }
}
