/**
 * Copyright (c) 2013 Washington University
 */
package org.nrg.xnat.download;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public interface Package extends Iterable<URI> {
    long getMD5(URI uri) throws IOException;

    Map<URI,?> getFailures();

    Iterator<File> getFileIterator();
    Iterator<Path> getPathIterator();
    
    int getFileCount();
    long getSize();
}
