/**
 * Copyright (c) 2013 Washington University School of Medicine
 */
package org.nrg.xnat.download;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class PackageRoot {

    public LinkedHashMap<String, File> getRoots() {
        return _roots;
    }

    public void setRoots(LinkedHashMap<String, File> roots) {
        _roots = roots;
    }

    public File getRoot(String project) {
        return _roots.get(project);
    }

    private LinkedHashMap<String, File> _roots = new LinkedHashMap<String, File>();

 }
