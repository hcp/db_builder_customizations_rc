package org.nrg.xft.db.views.service.custom;

import org.apache.log4j.Logger;
import org.nrg.xft.db.MaterializedViewI;
import org.nrg.xft.db.views.H2MetaTable;
import org.nrg.xft.db.views.LegacyMaterializedViewImpl;
import org.nrg.xft.db.views.MaterializedViewForFilterImpl;
import org.nrg.xft.db.views.service.CustomMaterializedViewService;
import org.nrg.xft.db.views.service.MaterializedViewManager;
import org.nrg.xft.db.views.service.MaterializedViewServiceI;
import org.nrg.xft.security.UserI;

import java.util.Hashtable;

/**
 * Created by mmckay01 on 1/20/2015.
 */

@CustomMaterializedViewService(value = "filter")
public class MaterializedViewForFilterService implements MaterializedViewServiceI {
    static org.apache.log4j.Logger logger = Logger.getLogger(MaterializedViewForFilterService.class);

    @Override
    public void deleteViewsByUser(UserI user) throws Exception {
        MaterializedViewManager manager=MaterializedViewManager.getMaterializedViewManager();
        for(MaterializedViewI view: manager.getViewsByUser(user,this)){
            view.delete();
        }

    }

    @Override
    public MaterializedViewI getViewByTablename(String tablename, UserI user) throws Exception{
        return H2MetaTable.findByTablename(tablename, user);
    }

    @Override
    public MaterializedViewI getViewBySearchID(String search_id, UserI user) throws Exception{
        return H2MetaTable.findBySearchID(search_id, user);
    }

    @Override
    public MaterializedViewI createView(UserI user) {
        try {
            return new MaterializedViewForFilterImpl(user);
        }
        catch(Exception e){
            return null;
        }
    }

    @Override
    public MaterializedViewI populateView(Hashtable t, UserI u) {
        try{
            return new MaterializedViewForFilterImpl(t, u);
        }
        catch(Exception e){
            return null;
        }
    }

    @Override
    public void save(MaterializedViewI i) throws Exception {

        H2MetaTable.registerTable(i);
    }

    @Override
    public void delete(MaterializedViewI i) throws Exception {
        H2MetaTable.deleteTable(i);
    }
}
