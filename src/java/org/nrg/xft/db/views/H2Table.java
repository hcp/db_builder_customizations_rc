package org.nrg.xft.db.views;

import com.twmacinta.util.MD5;
import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.display.DisplayField;
import org.nrg.xdat.om.XdatCriteria;
import org.nrg.xdat.om.XdatSearchField;
import org.nrg.xdat.schema.SchemaElement;
import org.nrg.xdat.search.DisplaySearch;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.security.XdatCriteriaSet;
import org.nrg.xdat.security.XdatStoredSearch;
import org.nrg.xft.XFTItem;
import org.nrg.xft.XFTTable;
import org.nrg.xft.db.MaterializedViewI;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.FieldNotFoundException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.entities.datadictionary.Attribute;
import org.nrg.xnat.services.datadictionary.DataDictionaryService;
import org.restlet.data.MediaType;
import org.xml.sax.SAXException;
import org.h2.jdbcx.JdbcConnectionPool;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by mmckay01 on 1/26/2015.
 */
public class H2Table {
    private static org.apache.log4j.Logger logger = Logger.getLogger(H2Table.class);
    private static final String tableURL = "jdbc:h2:mem:h2Tables;DB_CLOSE_DELAY=-1;IGNORECASE=TRUE";
    private static final JdbcConnectionPool cp = JdbcConnectionPool.create(tableURL,"","");
    private static final String h2Driver = "org.h2.Driver";


    private String tableName;
    private String joinSQL;
    private String fromSQL;
    private String createSQL;


    public H2Table(MaterializedViewI view) throws Exception {

        //TODO: break up into smaller methods
        XFTItem item = XFTItem.PopulateItemFromFlatString(view.getSearch_xml(), ((XDATUser) view.getUser()), true);
        XdatStoredSearch storedSearch = new XdatStoredSearch(item);
        String project = getProjectFromStoredSearch(storedSearch);

        ArrayList<XdatSearchField> fields = storedSearch.getSearchFields();
        ArrayList<String> elmNames = new ArrayList<String>();
        for(XdatSearchField field: fields){
            String elmName = field.getElementName();
            if(!elmNames.contains(elmName)){
                elmNames.add(elmName);
            }
        }
        String[] cols;
//        DeleteDbFiles.execute(".", db, false);//should change to true later to prevent unnecessary logging

        try{
            Class.forName(h2Driver);
        }catch(ClassNotFoundException e){
            logger.error("",e);
        }
        try(Connection conn = cp.getConnection();
            Statement existsStatement = conn.createStatement();
            Statement joinStatement = conn.createStatement();
        ){
            tableName = project + "_" + getMd5OfXml(view.getSearch_xml().toString());
            if (view.getTable_name() != null && !view.getTable_name().isEmpty()) {
                tableName = view.getTable_name();
            }
            ResultSet rs = existsStatement.executeQuery("select count(*) from information_schema.tables where table_name = '" + tableName.toUpperCase() + "'");
            rs.next();
            long exists = rs.getLong(1);

            ArrayList<String> tablesToDrop = new ArrayList<String>();
            if(exists<1) {//if table does not yet exist
                ArrayList<String> columnsInJoinedTable = new ArrayList<String>();
                joinSQL = "create table if not exists " + tableName + " AS ( SELECT ";
                fromSQL = " FROM ";
                String firstTable = "";
                int counter = 0;

                ArrayList<DisplaySearch> searches = new ArrayList<DisplaySearch>();
                if(elmNames.size()>0 && !elmNames.get(0).equals("xnat:subjectData")){//XNAT_SUBJECTDATA needs to be first
                    int index = elmNames.indexOf("xnat:subjectData");
                    if(index>=0) {
                        elmNames.remove(index);
                    }
                    elmNames.add(0, "xnat:subjectData");
                }
                for (String elmName : elmNames) {
                    try(Statement dropStatementTemp = conn.createStatement();
                        Statement stat = conn.createStatement();
                    ){
                        DisplaySearch ds = getDisplaySearch(view.getUser(), storedSearch, elmName);
                        searches.add(ds);

                        XFTTable table = (XFTTable) ds.execute(null);
                        Object[] row = table.nextRow();

                        String[] columns = table.getColumns();
                        String tempTableName = tableName + "_" + elmName.replaceAll(" ", "_").replaceAll("-", "_").replaceAll(":", "_") + "_tmp";
                        if (counter == 0) {
                            firstTable = tempTableName;
                            fromSQL = fromSQL + tempTableName;
                        } else {
                            //if (tempTableName.equalsIgnoreCase(defaultProject + "_MR_Sessions_tmp")) {
                            //    fromSQL = fromSQL + " LEFT JOIN " + tempTableName + " ON " + tempTableName + ".XNAT_SUBJECTDATA_SUBJECTID = " + firstTable + ".subjectid";
                            //} else {
                            fromSQL = fromSQL + " LEFT JOIN " + tempTableName + " ON " + firstTable + ".subjectid = " + tempTableName + ".subjectid";
                            //}
                        }
                        //joinSQL = joinSQL + tempTableName + ".*, ";


                        String dropSQLTemp = "DROP TABLE IF EXISTS "+tempTableName;
                        dropStatementTemp.execute(dropSQLTemp);

                        createSQL = "create table " + tempTableName + "(";
                        boolean hasCells = false;
                        int colCounter = 0;
                        Map<Integer,String> colTypes = new HashMap<Integer,String>();
                        for (String column : columns) {
                            if (column != null /*&& !column.equalsIgnoreCase("SUB_PROJECT_CODED_ID_HCP_500")*/) {
                                String colType = "varchar(255)";
                                if (column.equals("subjectid") || column.equals("xnat_subjectdata_subjectid")) {
                                    createSQL = createSQL + "subjectid" + " varchar(255) primary key, ";
                                    if (elmName.equals("xnat:subjectData") && !columnsInJoinedTable.contains("subjectid")) {
                                        joinSQL = joinSQL + tempTableName + ".subjectid AS subjectid, ";
                                        columnsInJoinedTable.add("subjectid");
                                    }
                                    colTypes.put(colCounter,colType);

                                } else{ //if((!column.endsWith("insert_date")) && (!column.endsWith("insert_user"))) {
                                    try{
                                        String colClass = row[colCounter].getClass().getSimpleName();
                                        if(!colClass.equals("String")){//Most non-string types should be the same in Java and H2
                                            if(colClass.equals("BigDecimal")) {//BigDecimals need special handling
                                                try{
                                                    BigDecimal decimal = (BigDecimal)row[colCounter];
                                                    int scale = decimal.scale();
                                                    colType = "decimal(30, "+scale+")";
                                                }
                                                catch(Exception e){
                                                    logger.error("",e);
                                                    colType = "decimal(30)";
                                                }
                                            }
                                            else{
                                                colType = colClass.toLowerCase();
                                            }
                                        }
                                    }
                                    catch(Exception e){
                                        logger.error("",e);
                                    }
                                    createSQL = createSQL + column + " "+colType+", ";
                                    colTypes.put(colCounter,colType);
                                    if (!columnsInJoinedTable.contains(column)) {
                                        joinSQL = joinSQL + tempTableName + "." + column + " AS " + column + ", ";
                                        columnsInJoinedTable.add(column);
                                    }
                                }
                                hasCells = true;
                            }
                            colCounter++;
                        }




                        if (hasCells) {
                            createSQL = createSQL.substring(0, createSQL.length() - 2) + ")";
                            stat.execute(createSQL);
                            tablesToDrop.add(tempTableName);
                        }
                        table.resetRowCursor();
                        while (table.hasMoreRows()) {
                            row = table.nextRow();
                            String insertRowSQL = "merge into " + tempTableName + " values(";
                            boolean hasCellsI = false;
                            int cellCount = 0;
                            for (Object cell : row) {
                                String cellType = colTypes.get(cellCount);
                                if (cell != null) {
                                    String cellString = cell.toString();
                                    if(cellType.equals("varchar(255)")||cellType.equals("timestamp")||cellType.equals("time")
                                            ||cellType.equals("datetime")||cellType.equals("smalldatetime")||cellType.equals("date")){
                                        cellString = "'" + cellString + "'";
                                    }
                                    insertRowSQL = insertRowSQL + cellString + ", ";
                                    hasCellsI = true;
                                } else {
                                    if(cellType.equals("varchar(255)")){
                                        insertRowSQL = insertRowSQL + "'', ";
                                    }
                                    else {
                                        insertRowSQL = insertRowSQL + "NULL, ";
                                    }
                                }
                                cellCount++;
                            }
                            if (hasCellsI) {
                                try {
                                    insertRowSQL = insertRowSQL.substring(0, insertRowSQL.length() - 2) + ")";
                                    stat.execute(insertRowSQL);
                                }catch(Exception e){
                                    logger.error("",e);
                                }
                            }
                        }
                        counter++;


                    }
                    catch(Exception e){
                        logger.error("",e);
                    }


                }
                joinSQL = joinSQL.substring(0, joinSQL.length() - 2) + fromSQL + ")";
                joinStatement.execute(joinSQL);

                for(String tabName : tablesToDrop){
                    try(Statement dropStatement = conn.createStatement();
                    ){
                        String dropSQL = "DROP TABLE "+tabName;
                        dropStatement.execute(dropSQL);
                    }
                    catch(Exception e){
                        logger.error("",e);
                    }
                }
            }
        }
        catch(Exception e){
            logger.error("",e);
        }
    }

    public XdatStoredSearch getSearchForCategory(String project, String category) throws Exception {
        final MediaType mt = MediaType.TEXT_XML;
        final DataDictionaryService service = XDAT.getContextService().getBean(DataDictionaryService.class);
        int tier = 0;

        //initialize search
        final XdatStoredSearch search = new XdatStoredSearch((UserI)null);
        search.setRootElementName("xnat:subjectData");
        search.setDescription(category);
        search.setBriefDescription(category);

        int count=0;

        final XdatSearchField column1 = new XdatSearchField();
        column1.setElementName("xnat:subjectData");
        column1.setFieldId("SUBJECT_LABEL");
        column1.setHeader("Subject");
        SchemaElement se1 = SchemaElement.GetElement("xnat:subjectData");
        DisplayField df1 = se1.getDisplayField("SUBJECT_LABEL");
        column1.setType(df1.getDataType());
        search.setSearchField(column1);

        for(final String assessment:service.getAssessments(category, project, tier)){
            for(final Attribute attribute:service.getAttributes(category, assessment, project, tier)){
                try {
                    if (!attribute.getColumnHeader().equals("Subject")){
                        final XdatSearchField column = new XdatSearchField();
                        column.setElementName(attribute.getXsiType());
                        column.setFieldId(attribute.getFieldId());
                        column.setHeader(attribute.getColumnHeader());
                        column.setSequence(count++);
                        SchemaElement se = SchemaElement.GetElement(attribute.getXsiType());
                        DisplayField df = se.getDisplayField(attribute.getFieldId());
                        column.setType(df.getDataType());
                        search.setSearchField(column);
                    }
                }
                catch(Exception e){
                    e.getMessage();
                }
            }
        }


        /** COMMENTED OUT:  2016/11/30.  I don't think this is needed anymore and we need to handle MR Sessions differently now that we have 7T data
        //add MR Columns, if requested.  This is temporarily hard coded because the data dictionary doesn't currently support unfilterable columns yet.
        if(category.equals("MR Sessions")){
            String[][] mr_fields={
                    {"LABEL","Label"},
                    {"SCANNER","Scanner"},
                    {"MR_SCAN_COUNT_AGG","Scans"}
            };

            for(String[] field:mr_fields){
                final XdatSearchField column2=new XdatSearchField();
                column2.setElementName("xnat:mrSessionData");
                column2.setFieldId(field[0]);
                column2.setHeader(field[1]);
                column2.setSequence(count++);
                column2.setType("string");
                search.setSearchField(column2);
            }

            search.setRootElementName("xnat:mrSessionData");
        }
        */

        final XdatCriteriaSet cs= new XdatCriteriaSet((UserI)null);
        cs.setMethod("OR");

        XdatCriteria c=new XdatCriteria();
        c.setSchemaField(search.getRootElementName() + "/project");
        c.setComparisonType("=");
        c.setValue(project);
        cs.setCriteria(c);

        c=new XdatCriteria();
        c.setSchemaField(search.getRootElementName()+"/sharing/share/project");
        c.setComparisonType("=");
        c.setValue(project);
        cs.setCriteria(c);

        search.setSearchWhere(cs);
        return search;
    }

    public void execute(String query){
        try{
            Class.forName(h2Driver);
        }catch(ClassNotFoundException e){
            logger.error("",e);
        }
        try(Connection conn = cp.getConnection();
            Statement statement = conn.createStatement();
        ){
            statement.execute(query);
        }
        catch(Exception e){
            logger.error("",e);
        }
        return;
    }

    public Long getSize(String query){
        Long size = new Long(0);
        try{
            Class.forName(h2Driver);
        }catch(ClassNotFoundException e){
            logger.error("",e);
        }
        try(Connection conn = cp.getConnection();
            Statement statement = conn.createStatement();
        ){
            ResultSet rs = statement.executeQuery(query);
            rs.next();
            size = rs.getLong(1);
        }
        catch(Exception e){
            logger.error("",e);
        }
        return size;
    }

    public static DisplaySearch getDisplaySearch(UserI user, XdatStoredSearch search, String elementName) throws XFTInitException,ElementNotFoundException,FieldNotFoundException,Exception
    {
        DisplaySearch ds = new DisplaySearch();
        ds.setUser(user);
        ds.setTitle(search.getDescription());
        ds.setDescription(search.getDescription());
        ds.setRootElement(search.getRootElementName());
        ds.setStoredSearch(search);
        Boolean b = search.getAllowDiffColumns();
        if (b!=null)
        {
            ds.setAllowDiffs(b.booleanValue());
        }
        try {
            Iterator sfs = search.getSearchFields().iterator();
            while (sfs.hasNext())
            {
                XdatSearchField sf = (XdatSearchField)sfs.next();
                String e = sf.getElementName();
                if(e.equals(elementName)){
                    String f = sf.getFieldId();
                    String h = sf.getHeader();
                    if(sf.getValue()!=null){
                        //remove value if it is appended to the field ID (deprecated value method)
                        if(f.indexOf("="+sf.getValue())>-1){
                            int i =f.indexOf("="+sf.getValue());
                            if (i>0){
                                f = f.substring(0,i);
                            }
                        }else if(f.indexOf("."+sf.getValue())>-1){
                            int i =f.indexOf("."+sf.getValue());
                            if (i>0){
                                f = f.substring(0,i);
                            }
                        }

                    }else{
                        //handle value passed by deprecated method
                        if(f.indexOf("=")>-1){
                            sf.setValue(f.substring(f.indexOf("=")+1));
                            f=f.substring(0,f.indexOf("="));
                        }
                        if(f.indexOf('.')>-1){
                            sf.setValue(f.substring(f.indexOf('.')+1));
                            f=f.substring(0,f.indexOf('.'));
                        }
                    }

                    if (h!=null && !h.trim().equals("")){
                        ds.addDisplayField(e,f,h,sf.getValue(),sf.getVisible());
                    }else{
                        ds.addDisplayField(e,f,null,(Object)sf.getValue(),sf.getVisible());
                    }
                }

            }
            if(!elementName.equals("xnat:subjectData")) {
                ds.addDisplayField("xnat:subjectData","SUBJECTID","Subject",null,true);
            }

        } catch (FieldNotFoundException e) {
            e.printStackTrace();
            logger.error("",e);
        }

        XdatCriteriaSet set=search.getInClause();
        if(set!=null){
            for(XdatCriteria crit:set.getCriteria()){
                ds.getInClauses().put(crit.getSchemaField(), crit.getValue());
            }
        }else{
            //WHERE CLAUSE
            Iterator wheres = search.getChildItems("xdat:stored_search.search_where").iterator();
            while (wheres.hasNext())
            {
                set = new XdatCriteriaSet((XFTItem)wheres.next());
                ds.addCriteria(set.getDisplaySearchCriteria());
            }
        }

        //SORT BY
        if (search.getSortBy_elementName()!=null &&  search.getSortBy_fieldId()!=null)
            ds.setSortBy(search.getSortBy_elementName() + "." + search.getSortBy_fieldId());


        return ds;
    }

    public static String getProjectFromStoredSearch(XdatStoredSearch storedSearch) throws IOException, SAXException {
        ArrayList<org.nrg.xdat.om.XdatCriteriaSet> sets = storedSearch.getSearchWhere();
        String project = "";
        for(org.nrg.xdat.om.XdatCriteriaSet set:sets){
            ArrayList<XdatCriteria>  criteria = set.getCriteria();
            for(XdatCriteria crit:criteria){
                if((storedSearch.getRootElementName()+"/project").equals(crit.getSchemaField())){
                    project=crit.getValue();
                }
            }
        }
        return project;
    }

    public static String getMd5OfXml(String xml) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        byte[] bytesOfMessage = xml.getBytes("UTF-8");

        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] xmlDigest = md.digest(bytesOfMessage);
        return MD5.asHex(xmlDigest);
    }


    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public static JdbcConnectionPool getConnectionPool(){
        return cp;
    }

}
