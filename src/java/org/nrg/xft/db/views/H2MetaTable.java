package org.nrg.xft.db.views;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;
import org.nrg.hcp.security.HcpUserDatabaseEntryUtils;
import org.nrg.xdat.om.XdatUserGroupid;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.UserGroup;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.security.XdatStoredSearch;
import org.nrg.xft.XFTItem;
import org.nrg.xft.db.MaterializedViewI;
import org.nrg.xft.security.UserI;
import org.springframework.jdbc.core.JdbcTemplate;
import org.h2.jdbcx.JdbcConnectionPool;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;

/**
 * Created by mmckay01 on 1/26/2015.
 */
public class H2MetaTable{
    private static org.apache.log4j.Logger logger = Logger.getLogger(H2MetaTable.class);
    private static final String tableURL = "jdbc:h2:~/h2Tables;IGNORECASE=TRUE";
    private static final String db = "h2Tables";
    private static final String h2Driver = "org.h2.Driver";
    private static final String restrictedGroupName = "Restricted";
    private static boolean tableCreated = false;
    private static BasicDataSource ds = new BasicDataSource();
    private static JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);

    public static void deleteTable(MaterializedViewI view, String tablename) throws Exception {
        try{
            Class.forName(h2Driver);
        }catch(ClassNotFoundException e){
            logger.error("",e);
        }
        try(Connection conn = H2Table.getConnectionPool().getConnection();
            Statement statement = conn.createStatement();
            Statement statement2 = conn.createStatement();
        ){
            String deleteSQL = "DELETE FROM materialized_views WHERE table_name = '" + tablename + "'";
            statement.execute(deleteSQL);

            String dropSQL = "DROP TABLE " + tablename;
            statement2.execute(dropSQL);
        }
        catch(Exception e){
            logger.error("",e);
        }
    }

    public static void deleteTable(MaterializedViewI view) throws Exception {
        deleteTable(view,view.getTable_name());
    }

    public static MaterializedViewI findTable(MaterializedViewI view) throws Exception {
        createMaterializedViewTable();
        String searchXml = view.getSearch_xml();
        String md5 = H2Table.getMd5OfXml(searchXml);
        XFTItem item = XFTItem.PopulateItemFromFlatString(view.getSearch_xml(), ((XDATUser) view.getUser()), true);
        XdatStoredSearch storedSearch = new XdatStoredSearch(item);
        String projectId = H2Table.getProjectFromStoredSearch(storedSearch);
        XDATUser user = (XDATUser) view.getUser();
        XnatProjectdata p = XnatProjectdata.getProjectByIDorAlias(projectId, user, false);
        XnatProjectdata umbrellaProject = p.getRootProject();

        Hashtable<String, UserGroup> groups = ((XDATUser)view.getUser()).getGroups();
        boolean hasAccessToRestricted = false;
        if (HcpUserDatabaseEntryUtils.getAccessLevel(user, umbrellaProject.getId()) > 0) {
            hasAccessToRestricted = true;
        }
        try{
            Class.forName(h2Driver);
        }catch(ClassNotFoundException e){
            logger.error("",e);
        }
        MaterializedViewI viewToReturn = null;
        try(Connection conn = H2Table.getConnectionPool().getConnection();
            Statement statement = conn.createStatement();
        ){
            String selectSQL = "SELECT * FROM materialized_views WHERE md5_of_xml = '"+md5+"'";
            if(hasAccessToRestricted){
                selectSQL += " AND group_name IS '"+restrictedGroupName+"';";
            }
            else{
                selectSQL += " AND group_name IS NOT '"+restrictedGroupName+"';";
            }

            statement.execute(selectSQL);
            viewToReturn = getViewFromResultSet(statement.getResultSet(), (XDATUser)view.getUser());
        }
        catch(Exception e){
            logger.error("",e);
        }
        return viewToReturn;
    }

    public static void registerTable(MaterializedViewI view, H2Table table) throws SQLException {
        String projectId="";
        try{
            XFTItem item = XFTItem.PopulateItemFromFlatString(view.getSearch_xml(), ((XDATUser) view.getUser()), true);
            XdatStoredSearch storedSearch = new XdatStoredSearch(item);
            projectId = H2Table.getProjectFromStoredSearch(storedSearch);
            XDATUser user = (XDATUser) view.getUser();
            XnatProjectdata p = XnatProjectdata.getProjectByIDorAlias(projectId, user, false);
            XnatProjectdata umbrellaProject = p.getRootProject();
            Hashtable<String, UserGroup> groups = (user.getGroups());
            boolean hasAccessToRestricted = false;
            if (HcpUserDatabaseEntryUtils.getAccessLevel(user, umbrellaProject.getId()) > 0) {
                hasAccessToRestricted = true;
            }
            if(view instanceof MaterializedViewForFilterImpl){
                ((MaterializedViewForFilterImpl)view).setMd5_of_xml(H2Table.getMd5OfXml(item.writeToFlatString(0)));
                if(hasAccessToRestricted){
                    ((MaterializedViewForFilterImpl)view).setGroup_name(restrictedGroupName);
                }
            }

            createMaterializedViewTable();
        }
        catch(Exception e){
            logger.error("",e);
        }

        try{
            Class.forName(h2Driver);
        }catch(ClassNotFoundException e){
            logger.error("",e);
        }
        try(Connection conn = H2Table.getConnectionPool().getConnection();
            Statement statement = conn.createStatement();
        ){
            Calendar calendar = Calendar.getInstance();
            java.util.Date created = calendar.getTime();
            if (view.getCreated() != null) {
                created = view.getCreated();
            }
            view.setCreated(created);
            view.setLast_access(created);

            String table_name = table.getTableName();
            if (table_name == null || table_name.isEmpty()) {
                table_name = projectId + "_";
                if(view.getSearch_xml()!=null){
                    table_name = table_name + H2Table.getMd5OfXml(view.getSearch_xml().toString());
                }
            }
            view.setTable_name(table_name);
            String username = view.getUser_name();
            String search_id = "";
            if (view.getSearch_id() != null) {
                search_id = view.getSearch_id().toString();
            }
            String tag = "";
            if (view.getTag() != null) {
                tag = view.getTag().toString();
            }
            String md5_of_xml = "";
            if (view.getSearch_xml() != null) {
                try {
                    md5_of_xml = H2Table.getMd5OfXml(view.getSearch_xml().toString());
                } catch (Exception e) {
                    logger.error("",e);
                }
            }
            String group_name = "";
            if ((view instanceof MaterializedViewForFilterImpl) && ((MaterializedViewForFilterImpl) view).getGroup_name() != null) {
                group_name = ((MaterializedViewForFilterImpl) view).getGroup_name().toString();
            }
            String search_sql = view.getSearch_sql();
            String search_xml = view.getSearch_xml();
            String createdString = new SimpleDateFormat("yyyy-MM-dd").format(created);
            String insertSQL = "INSERT INTO materialized_views (table_name, created, last_access, username, search_id, tag, md5_of_xml, group_name, search_sql, search_xml) VALUES ('" + table_name + "', '" + createdString + "', '" + createdString + "', '" + username + "', '" + search_id + "', '" + tag + "', '" + md5_of_xml + "', '" + group_name + "', '" + search_sql + "', '" + search_xml + "');";
            createMaterializedViewTable();
            statement.execute(insertSQL);
        }
        catch(Exception e){
            logger.error("",e);
        }

        try {
            H2Table temp = new H2Table(view);
        } catch (Exception e) {
            logger.error("",e);
        }
    }

    public static void registerTable(MaterializedViewI view1) throws Exception {
        MaterializedViewI view2 = H2MetaTable.findTable(view1);//initialize this and get table name if created    find from mat view table
        if(view2==null/*hasn't been created yet*/){
            if(view1.getCreated()==null){
                Calendar calendar = Calendar.getInstance();
                java.util.Date created = calendar.getTime();
                view1.setCreated(created);
                view1.setLast_access(created);
            }
            H2Table temp = new H2Table(view1);

            H2MetaTable.registerTable(view1,temp);
        }
        else{
            view1.setCreated(view2.getCreated());
            view1.setSearch_id(view2.getSearch_id());
            view1.setSearch_sql(view2.getSearch_sql());
            view1.setTable_name(view2.getTable_name());
            view1.setTag(view2.getTag());
            view1.setUser(view2.getUser());
            view1.setUser_name(view2.getUser_name());
            try {
                ((MaterializedViewForFilterImpl) view1).setMd5_of_xml(((MaterializedViewForFilterImpl) view2).getMd5_of_xml());
                ((MaterializedViewForFilterImpl) view1).setGroup_name(((MaterializedViewForFilterImpl) view2).getGroup_name());
            }
            catch(Exception e){
                logger.error("",e);
            }
        }
    }

    public static MaterializedViewI findBySearchID(String search_id, UserI user) throws SQLException {
        try{
            Class.forName(h2Driver);
        }catch(ClassNotFoundException e){
            logger.error("",e);
        }
        MaterializedViewI viewToReturn = null;
        try(Connection conn = H2Table.getConnectionPool().getConnection();
            Statement statement = conn.createStatement();
        ){
            createMaterializedViewTable();
            String selectSQL = "SELECT * FROM materialized_views WHERE search_id = '"+search_id+"'";

            statement.execute(selectSQL);
            viewToReturn = getViewFromResultSet(statement.getResultSet(), user);
        }
        catch(Exception e){
            logger.error("",e);
        }
        return viewToReturn;
    }

    public static MaterializedViewI findByTablename(String tablename, UserI user) throws SQLException {
        try{
            Class.forName(h2Driver);
        }catch(ClassNotFoundException e){
            logger.error("",e);
        }
        MaterializedViewI viewToReturn = null;
        try(Connection conn = H2Table.getConnectionPool().getConnection();
            Statement statement = conn.createStatement();
        ){
            createMaterializedViewTable();
            String selectSQL = "SELECT * FROM materialized_views WHERE table_name = '"+tablename+"'";

            statement.execute(selectSQL);
            viewToReturn = getViewFromResultSet(statement.getResultSet(), user);
        }
        catch(Exception e){
            logger.error("",e);
        }
        return viewToReturn;
    }

    private static void createMaterializedViewTable() throws ClassNotFoundException, SQLException {
        try{
            Class.forName(h2Driver);
        }catch(ClassNotFoundException e){
            logger.error("",e);
        }
        try(Connection conn = H2Table.getConnectionPool().getConnection();
            Statement statement = conn.createStatement();
        ){
            String createTableSQL = "CREATE TABLE IF NOT EXISTS materialized_views(table_name VARCHAR(255), created TIMESTAMP, last_access TIMESTAMP, username VARCHAR(255), search_id VARCHAR(255), tag VARCHAR(255), md5_of_xml VARCHAR(255), group_name VARCHAR(255), search_sql CLOB, search_xml CLOB);";
            statement.execute(createTableSQL);
            tableCreated = true;
        }
        catch(Exception e){
            logger.error("",e);
        }
        return;
    }

    private static MaterializedViewI getViewFromResultSet(ResultSet rs, UserI user){
        MaterializedViewI viewToReturn = null;
        try {
            while(rs.next()) {//if has a result

                Hashtable t = new Hashtable();
                t.put("created", rs.getDate("created"));
                t.put("last_access", rs.getDate("last_access"));
                t.put("search_id", rs.getString("search_id"));
                t.put("search_sql", rs.getString("search_sql"));
                t.put("search_xml", rs.getString("search_xml"));
                t.put("table_name", rs.getString("table_name"));
                t.put("tag", rs.getString("tag"));
                t.put("username", rs.getString("username"));
                t.put("md5_of_xml", rs.getString("md5_of_xml"));
                t.put("group_name", rs.getString("group_name"));

                viewToReturn = new MaterializedViewForFilterImpl(t, user);
                String tableName = rs.getString("table_name");
                Hashtable<String, UserGroup> groups = ((XDATUser)user).getGroups();

                XFTItem item = XFTItem.PopulateItemFromFlatString(rs.getString("search_xml"), user, true);
                XdatStoredSearch storedSearch = new XdatStoredSearch(item);
                String projectId = H2Table.getProjectFromStoredSearch(storedSearch);
                XnatProjectdata p = XnatProjectdata.getProjectByIDorAlias(projectId, (XDATUser) user, false);
                XnatProjectdata umbrellaProject = p.getRootProject();

                // Check that the user has any project access and return null if not
                UserGroup projectGroup = ((XDATUser) user).getGroupByTag(umbrellaProject.getId());
                if (projectGroup == null) {
                    return null;
                }

                boolean hasAccessToRestricted = false;
                if (HcpUserDatabaseEntryUtils.getAccessLevel((XDATUser) user, umbrellaProject.getId()) > 0) {
                    hasAccessToRestricted = true;
                }

                if(rs.getString("group_name").equals(restrictedGroupName) && hasAccessToRestricted){
                    return viewToReturn;
                }
                else if((!(rs.getString("group_name").equals(restrictedGroupName))) && (!hasAccessToRestricted)){
                    return viewToReturn;
                }
                else{
                    viewToReturn = null;
                }
            }
        } catch (Exception e) {
            logger.error("",e);
        }
        return viewToReturn;
    }
}