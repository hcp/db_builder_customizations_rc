/*
 * GENERATED FILE
 * Created on Tue May 21 20:21:57 CDT 2013
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class HcpRestrictedasr extends BaseHcpRestrictedasr {

	public HcpRestrictedasr(ItemI item)
	{
		super(item);
	}

	public HcpRestrictedasr(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseHcpRestrictedasr(UserI user)
	 **/
	public HcpRestrictedasr()
	{}

	public HcpRestrictedasr(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
